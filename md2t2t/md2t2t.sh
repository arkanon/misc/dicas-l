#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2024/12/31 (Tue) 01:45:53 -03
# 2024/10/05 (Sat) 05:13:54 -03
# 2024/06/13 (Thu) 09:28:48 -03
# 2024/06/10 (Mon) 09:58:31 -03
# 2024/06/07 (Fri) 15:20:52 -03
# 2023/06/13 (Tue) 02:44:34 -03
# 2023/04/23 (Sun) 04:54:02 -03
# 2023/04/19 (Wed) 04:56:00 -03
# 2022/12/06 (Tue) 01:59:19 -03
# 2021/10/07 (Thu) 23:59:54 -03

# Based on <http://github.com/txt2tags/tools/blob/master/markdown2txt2tags.sh>
#
# http://txt2tags.org
# http://blog.txt2tags.org
# http://github.com/txt2tags/txt2tags
# http://github.com/txt2tags/tools/tree/master/HTML-WikiConverter-Txt2tags
#
# [t2t] convert from html to txt2tags <http://sf.net/p/txt2tags/mailman/message/29588583>
#
# Converts from markdown to the txt2tags format. It requires:
# - pandoc    (to convert from markdown to html: http://johnmacfarlane.net/pandoc)
# - html2wiki (http://search.cpan.org/~diberri/HTML-WikiConverter-0.68/bin/html2wiki)
# - txt2tags  (to export for html2wiki: http://wiki.txt2tags.org/index.php/Main/Html2wiki)
#
# [TODO]
# -- estilizar última linha de commandos multilinha
# -- tratar TOC
# -- tratar imagens com o tamanho indicado



  install()
  {

    sudo apt install -y pandoc                       # pandoc      2.9.2.1
    sudo apt install -y libhtml-wikiconverter-perl   # html2wiki   0.68-3.1
    sudo apt install -y cpanminus
    sudo apt install -y perl-doc
    sudo apt install -y python3-full

    export PIPX_HOME=/export/app-data/python
    sudo mkdir -p $PIPX_HOME
    sudo chown -R $USER: $PIPX_HOME

    pipx install grip     # 4.6.2
    pipx install txt2tags # 3.9

    sudo cpanm --force HTML::WikiConverter::Txt2tags # Txt2tags.pm 0.04 (/usr/local/share/perl/5.34.0/HTML/WikiConverter/Txt2tags.pm)
  # perldoc HTML::WikiConverter::Txt2tags

    ln -fs $PWD/md2t2t.sh /fs/bin/md2t2t

    grip ./file.md
    firefox http://localhost:6419

    md2t2t README.md dica
    firefox dica.html

  }



  [[ -f $1 ]] || { echo -e "\nUsage: ${0##*/} <file.md> [output-prefix]\n\nOutput: output-prefix.t2t and output-prefix.html\n" >&2; exit 1; }



  declare -A sed=(

     [markdown]='
                  s@(```).+@\1@
                  s@!\[[^]]*]@![]@
                  /^# /{ s@ [_`]|[_`] @ @g ; s@[_`]$@@g }
                  /☐/d
                '

       [pandoc]='
                  s@(<pre>)<code>@\1@g
                  s@</code>(</pre>)@\1@g
                  s@<P>@<p>@g
                  s@</P>@</p><br/><br/>\n\n\nLINEBREAK@g
                '

    [html2wiki]='
                  s@&lt;@<@g
                  s@&gt;@>@g
                  s@LINEBREAK@\n\n@g
                  s@^=@\n=@g
                '

     [txt2tags]='
                  /<pre>/,/<\/pre>/{
                    /(^[$<]|[\|\\] *$)/!s@.+@<span class="saida">&</span>@
                    /[\|\\/] *$/{n;s@<span class="saida">@@}
                    /[\|\\/] *$/{n;s@</span>@@}
                  }
                  s@<pre>@<pre class="comandos">@g
                '

  )

  declare -a css=(
    "https://fonts.googleapis.com/css?family=Open+Sans&display=swap"
    "https://fonts.googleapis.com/css?family=Oswald:400,700"
    "https://fonts.googleapis.com/css?family=Montserrat"
    "https://www.dicas-l.com.br/css/responsee.css"
  # "../style/style.css"
    "../style/extra.css"
  )



   input=$1
  output=${2:-${1%.*}}

  sed -r "${sed[markdown]}" $input |
  pandoc -f markdown -t html       |
  sed -r "${sed[pandoc]}"          |
  html2wiki --dialect Txt2tags     |
  sed -r "${sed[html2wiki]}"       > $output.t2t

  printf -v style -- '--style=%s ' "${css[@]}"

  txt2tags -t html $style -i $output.t2t -o - |
  sed -r "${sed[txt2tags]}"                   > $output.html



# -----------------------------------
# pandoc
# -----------------------------------
# input             output
# -----------------------------------
# commonmark        commonmark
# creole            -
# csv               -
# docbook           docbook
# docx              docx
# dokuwiki          dokuwiki
# epub              epub
# fb2               fb2
# gfm               gfm
# haddock           haddock
# html              html
# ipynb             ipynb
# jats              jats
# jira              jira
# json              json
# latex             latex
# man               man
# markdown          markdown
# markdown_github   markdown_github
# markdown_mmd      markdown_mmd
# markdown_phpextra markdown_phpextra
# markdown_strict   markdown_strict
# mediawiki         mediawiki
# muse              muse
# native            native
# odt               odt
# opml              opml
# org               org
# rst               rst
# t2t               -
# textile           textile
# tikiwiki          -
# twiki             -
# vimwiki           -
# -----------------------------------
# -                 asciidoc
# -                 asciidoctor
# -                 beamer
# -                 context
# -                 docbook4
# -                 docbook5
# -                 dzslides
# -                 epub2
# -                 epub3
# -                 html4
# -                 html5
# -                 icml
# -                 jats_archiving
# -                 jats_articleauthoring
# -                 jats_publishing
# -                 ms
# -                 opendocument
# -                 pdf
# -                 plain
# -                 pptx
# -                 revealjs
# -                 rtf
# -                 s5
# -                 slideous
# -                 slidy
# -                 tei
# -                 texinfo
# -                 xwiki
# -                 zimwiki
# -----------------------------------



# EOF
