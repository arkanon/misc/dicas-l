# Sintaxe `t2t`

```
primero, as convenções
  INI = início da linha
  FIM = fim da linha
  TAB = o caractere TAB, o \t


---------------------------------------------------------------------
cabeçalho          linha1 \n linha2 \n linha3

 - as primeiras 3 linhas do arquivo fonte
 - essas linhas serão as 3 primeiras linhas do documento final,
   bem diferenciadas do texto normal, ou serão colocadas sozinhas
   na primeira página (se for paginável)
 - o conteúdo dos cabeçalhos é livre, mas o seguinte formato é
   recomendado para a maioria dos documentos:
     linha 1: título do documento
     linha 2: nome do autor e/ou e-mail
     linha 3: data e/ou versão do documento (lembre do %%date)
 - o cabeçalho tem apenas texto, as marcas não são interpretadas
 - deixe a primeira linha em branco para definir um cabeçalho vazio
   (bom para testes na linha de comando)
 - deixe a segunda e a terceira linha em branco para omitir partes do
   cabeçalho

---------------------------------------------------------------------
título             <igual>palavras<igual>

 - sinais de igual BALANCEADOS ao redor, = assim =
 - o número de sinais de igual indica se é título, subtítulo, subsub...
 - ==== isso é um subsubsubtítulo ====
 -    ==    espaços antes ou depois das marcas são opcionais       ==
 - o máximo é de 4 subtítulos, =====assim=====
 - outras marcas como negrito e itálico não são interpretadas nos títulos
 - === sinais não balanceados não fazem um título =
 - o título NÃO é pode ser multilinha ==como
                            esse==

---------------------------------------------------------------------
título numerado    <mais>palavras<mais>

 - sinais de mais BALANCEADOS ao redor, + assim +
 - o número de sinais de mais indica se é título, subtítulo, subsub...
 - ++++ isso é um subsubsubtítulo ++++
 -    ++    espaços antes ou depois das marcas são opcionais       ++
 - o máximo é de 4 subtítulos, +++++assim+++++
 - outras marcas como negrito e itálico não são interpretadas nos títulos
 - +++ sinais não balanceados não fazem um título +
 - o título numerado NÃO é pode ser multilinha ==como
                            esse==

---------------------------------------------------------------------
parágrafo

 - os parágrafos são demilitados por linhas em branco
 - outras estruturas como lista, citação ou tabela fecham um parágrafo

---------------------------------------------------------------------
comentário         <INI><porcentagem>comentários

 - uma linha que começa com o sinal de porcentagem, % assim
 - o % deve estar no início da linha, sem espaços antes
 - como comentários, eles não são mostrados no texto final
 - NÃO são multilinha, então cada linha comentada deve começar com %
 - útil para lembretes e avisos

---------------------------------------------------------------------
negrito         <asterisco><asterisco>palavras<asterisco><asterisco>

 - dois asteriscos ao redor, **assim**
 - NÃO é multilinha **como
                    esse**
 - NÃO colocar espaços nas marcas, ** assim **

---------------------------------------------------------------------
itálico            <barra><barra>palavras<barra><barra>

 - duas barras ao redor, //assim//
 - como o negrito, NÃO é multilinha e NÃO pode ter espaços nas marcas

---------------------------------------------------------------------
sublinhado         <sublinha><sublinha>palavras<sublinha><sublinha>

 - dois sublinhas ao redor, __assim__
 - como o negrito, NÃO é multilinha e NÃO pode ter espaços nas marcas

---------------------------------------------------------------------
monoespaçado       <crase><crase>palavras<crase><crase>

 - crases (acento grave) ao redor, ``assim``
 - outras marcas NÃO são interpretadas dentro do monoespaçado
 - como o negrito, NÃO é multilinha e NÃO pode ter espaços nas marcas

---------------------------------------------------------------------
linha pré-formatada   <INI><crase><crase><crase><espaço>palavras

 - uma linha iniciada por 3 crases e um espaço, ``` assim
 - as crases devem estar no início da linha, sem espaços antes
 - coloque um espaço após as crases para separá-las do texto
 - outras marcas NÃO são interpretadas dentro da linha pré-formatada
 - a linha pré-formatada NÃO é multilinha, é claro

---------------------------------------------------------------------
área pré-formatada <INI><crase><crase><crase><FIM>
                   linhas
                   <INI><crase><crase><crase><FIM>

 - uma linha com exatamente 3 crases
 - seguida das linhas já formatadas
 - seguidas por outra linha com exatamente 3 crases
 - NÃO é permitido colocar espaços antes ou depois das crases
 - marcas NÃO são interpretadas dentro da área pré-formatada

---------------------------------------------------------------------
linha horizontal   <hífen><hífen><hífen><hífen><hífen><hífen>...
                   <sublinha><sublinha><sublinha><sublinha>...
                   <igual><igual><igual><igual><igual><igual>...

 - uma linha com pelos menos 20 hífens, sublinhas ou sinais de igual
 - espaços opcionais podem ser colocados no início ou fim da linha
 - qualquer outro caractere invalida a marca
 - se o primeiro caractere for um sinal de igual, a linha vai ser
   grossa, ou indicará uma pausa em formatos temporais como o mgp

---------------------------------------------------------------------
links              url ou email
links (explicito)  <abre-colchete>nome url<fecha-colchete>

 - um endereço de internet válido: URL, ftp, news ou email
 - estas entidades são detectadas automaticamente, sem marcação
 - o protocolo (http, https, ftp) é opcional, www.assim.com
 - também é possível dar nomes a um link, [clique aqui www.assim.com]
 - se o documento final não usa links, eles são somente sublinhados

---------------------------------------------------------------------
citação            <INI><TAB>palavras

 - uma linha que começa com um TAB
 - mais TABs no início indicam uma citação mais profunda (se permitido)
 - não há profundidade máxima (em alguns documentos sim)
 - embelezadores como negrito e itálico são permitidos dentro de citações,
   mas outras estruturas como listas, título ou comentários não.

---------------------------------------------------------------------
lista              <INI><hífen><espaço>palavras

 - uma linha que começa com um hífem seguido de exatamente um espaço,
   - assim
 - o primeiro caractere da lista NÃO pode ser um espaço, -  assim
 - espaços opcionais (espaços, não TABs) no início da linha indicam
   sublistas
 - não há profundidade máxima (em alguns documentos sim)
 - as sublistas terminam quando é encontrado um item da lista mãe
 - a lista termina com duas linhas em branco consecutivas

---------------------------------------------------------------------
lista numerada     <INI><mais><espaço>palavras

 - uma linha que começa com um sinal de mais seguido de exatamente um
   espaço, + assim
 - o primeiro caractere da lista NÃO pode ser um espaço, +  assim
 - as mesmas regras da lista normal se aplicam à numerada

---------------------------------------------------------------------
lista de termos    <INI><dois-pontos><espaço>palavras

 - uma linha que começa com dois-pontos seguido de exatamente um
   um espaço, seguido de palavras (o termo):
   : assim
 - o primeiro caractere do termo pode ser um espaço, :   assim
 - as mesmas regras da lista normal se aplicam à de termos

---------------------------------------------------------------------
imagem             <abre-colchete>arquivo.XXX<fecha-colchete>

 - um nome de arquivo colocado entre colchetes, [assim.jpg]
 - o nome do arquivo de termninar com .PNG, .jpg, .GIF, ...
 - símbolos são permitidos no nome do arquivo, [assim!~1.jpg]
 - NÃO é permitido espaços no nome do arquivo, [assim nao.gif]
 - NÃO é permitido espaços nos colchetes, [ assim.gif ]
 - o documento final deve ter suporte a imagens
 - a posição da marca na linha define o alinhamento da imagem:
   [esquerda.GIF] blablabla [centro.GIF] blablabla [direita.GIF]

---------------------------------------------------------------------
data (iso)         <porcentagem><porcentagem>date
data (c/formato)   <porcentagem><porcentagem>date(formato)

 - dois sinais de porcentagem seguidos pela palavra "date"
 - macro rápida para a data atual no formato ISO aaaammdd
 - a data aceita um formato especial, com %Y, %m, %d e amigos
 - útil para o cabeçalho e versionamento
 - sinal de que o txt2tags está ficando BLOAT...

---------------------------------------------------------------------
tabela             <pipe><espaço>campo1<space><pipe><espaço>campo2...

 - uma barra vertical (pipe) no início, identifica uma linha de tabela
 - dois pipes no início, identificam uma linha de título da tabela
 - espaços antes do primeiro pipe indicam que a tabela é centralizada
 - os campos são separados pela string ' | '
 - um | no final da primeira linha da tabela indica que ela terá borda
 - um | no final das outras linhas são ignorados (estéticos apenas)
 - o espaçamento interno define o alinhamento de cada campo
 - documentos que alinham colunas e não campos (como o sgml e o
   LaTeX), baseiam-se no alinhamento da primeira linha.
 - embelezadores são interpretados dentro de tabelas
 - fora comentários, qualquer linha que não comece com | fecha a tabela

---------------------------------------------------------------------
protegido           <aspas><aspas>palavras<aspas><aspas>

 - duas aspas duplas ao redor, ""assim""
 - usado para "proteger" um texto da conversão (passa como está)
 - marcas não são interpretadas dentro do protegido
 - NÃO é multilinha

---------------------------------------------------------------------
linha protegida     <INI><aspas><aspas><aspas><espaço>palavras

 - uma linha iniciada por 3 aspas e um espaço, """ assim
 - as aspas devem estar no início da linha, sem espaços antes
 - coloque um espaço após as aspas para separá-las do texto
 - outras marcas NÃO são interpretadas dentro da linha protegida
 - a linha protegida NÃO é multilinha, é claro

---------------------------------------------------------------------
área protegida     <INI><aspas><aspas><aspas><FIM>
                   linhas
                   <INI><aspas><aspas><aspas><FIM>

 - uma linha com exatamente 3 aspas
 - seguida das linhas já formatadas
 - seguidas por outra linha com exatamente 3 aspas
 - NÃO é permitido colocar espaços antes ou depois das aspas
 - marcas NÃO são interpretadas dentro da área protegida

---------------------------------------------------------------------
configuração        <INI><porcentagem><exclamação>nome: valor

- um comentário especial no início da linha, como %!nome: valor
- é possível atrelar a configuração a um destino específico, no
  formato %!chave(destino): valor
- alguns nomes válidos: options, encoding, style, preproc, postproc
- uma configuração com um nome inválido é considerada um comentário

---------------------------------------------------------------------
comando de inserção <INI><porcentagem><exclamação>include: arquivo

- um comentário especial no início da linha, com a palavra 'include'
- é possível atrelar um destino ao comando, como em
  %!include(destino): arquivo
- há três tipos de inserção:
  - %!include:   arquivo.t2t   -- insere o corpo de um .t2t
  - %!include: ``arquivo.txt`` -- insere um texto já formatado
  - %!include: ''arquivo.xxx'' -- insere um texto já convertido
```



☐
