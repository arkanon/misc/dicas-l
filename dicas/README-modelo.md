<!--
  2024/08/28 (Wed) 08:01:53 -03
-->



[[_TOC_]]



# Main Title

***Colaboração: Paulo Roberto Bagatini***

***Data de Publicação:***



## Subtitle

Text
same line text  
new line text:
```
Text↵
same line text  ↵
new line text:
```

Unordered list (indent to subitem)
- *italic*
- **bold**
- ***italic bold***
- `preformatted`
- http://url.com
- [anchor](http://url.com)
- ![alt](image.png "title"){width=400px}

Ordered list:
1. something
1. other thing
1. and this
```
1. something
1. other thing
1. and this
```

Code (prefer "bash" over "shell-session" type, for `t2t` translation):
```bash
$ echo command
# comment
```



## Referências
- [Wikipedia](http://wikipedia.org/wiki/bash)



## Comandos usados nesse artigo

- [bash](    http://man.archlinux.org/man/bash.1):
    [echo](  http://man.archlinux.org/man/bash.1#echo%7e2),
    [printf](http://man.archlinux.org/man/bash.1#printf)
- [echo](    http://man.archlinux.org/man/echo.1)
- [printf](  http://man.archlinux.org/man/printf.1)



☐
