<!--
  2024/06/24 (Mon) 21:53:42 -03
  2024/06/23 (Sun) 10:46:30 -03
  2020/09/25 (Fri) 23:38:31 -03
  2020/08/07 (Fri) 23:43:49 -03
-->



[[_TOC_]]



# psDoom-NG

Mais de 24 anos atrás, em maio de 2000, alguém criou o [psDoom](http://psdoom.sourceforge.net), uma modificação do **Doom** para fechar aplicativos matando monstros durante
o jogo 💛. Uma aplicação bastante justificada, se me permitem a opinião. Infelizmente o tempo se encarregou de tornar o *patch* incompatível com as versões atuais das
bibliotecas do sistema operacional.

Quatro anos atrás tornou-se possível [remover pods kubernets jogando Doom](http://github.com/storax/kubedoom), no mesmo estilo do **psDoom**. Bateu a saudade e acabei
descobrindo que em 2014 uma boa alma revitalizou o código original e o adaptou ao do [Chocolate Doom](http://www.chocolate-doom.org). Agora novamente é possível substituir o
clique do *mouse* "no X" por um satisfatório tiro da sua arma preferida. E não apenas jogando [Doom](http://wikipedia.org/wiki/Doom_%281993_video_game%29), mas
[Doom 2](http://wikipedia.org/wiki/Doom_II) e pelo menos [Freedoom Phase 2](http://github.com/freedoom/freedoom) 👍

O projeto chama-se **psDoom-NG** (*New Generation*) e [está disponível no GitHub](http://github.com/keymon/psdoom-ng).

O vídeo a seguir demonstra toda a utilidade e praticidade envolvidas no processo 😎

[![psDoom-NG](play.jpg){width=400px}](http://youtube.com/watch?v=JSpHc945G38 "psDoom-NG")



# Instalação em Ubuntu

## Compilação do código fonte em um _container_

Uma boa abordagem para compilar o código fonte é fazê-lo em um _container_. Isso garante tanto que o procedimento não sofrerá interferência de customizações aplicadas à
instalação do _host_, quanto que não "poluirá" com pacotes de desenvolvimento uma instalação que provavelmente não tem esse objetivo. O uso de um _container_ em detrimento
ao de uma VM também traz vantagens, principalmente na simplicidade de integração do resultado da compilação com o _host_. Além disso, com a mesma facilidade, tem-se acesso
não só à mesma versão "limpa" de SO instalado no _host_, como à qualquer versão de qualquer distribuição, simplificando a possibilidade de uma eventual criação de pacote
com o binário resultante.

A opção pelo uso do [`podman`](https://podman.io) em vez do popular [`docker`](https://docker.io) é devida a algumas características interessantes do `podman` para o uso
proposto acima. Em especial, ele é executado sem a necessidade de um _daemon_ e de privilégios de _root_ o que o torna mais leve e mais seguro. Também, por ser executado
como usuário, um diretório do _host_ compartilhado como volume para o _container_ terá seu conteúdo gravado pelo root do _container_ automaticamente pertencente ao usuário
que o executou, o que torna transparente o intercâmbio entre o que é feito do lado do _host_ e do que é feito do lado do _container_.

Instalação do `podman` e execução do _container_:
```bash
sudo apt install -y podman
sudo usermod --add-subuids 100000-165535 --add-subgids 100000-165535 $USER
cat /etc/sub?id
podman system migrate
ls -la ~/.local/share/containers

dir=dica
mkdir -p $dir
cd $dir

args=(
  --rm
  --tty
  --interactive
  --volume   $PWD:/$dir
  --workdir /$dir
  --hostname $dir
  --name     $dir
)

. /etc/os-release

podman run "${args[@]}" ubuntu:$VERSION_ID bash
```

## Compilação do código fonte

Seja em _container_, seja no próprio _host_, o procedimento descrito a partir desse ponto é basicamente o mesmo.

---

Como o processo de download e compilação do código fonte muito provavelmente exigirá a instalação de alguns pacotes, vamos preparar o `apt` para agir não interativamente
usando seus _defaults_ e sincronizar suas listagens locais de pacotes com as dos repositórios instalados. Também popularemos a variável `$sudo` com a _string_ `sudo`
caso o comando esteja instalado e o usuário ativo não seja _root_. Se o comando `sudo` não estiver instalado possivelmente estaremos trabalhando em um _container_. Se o
usuário for _root_, o comando `sudo` é desnecessário. Em qualquer uma dessas duas situações, a variável será mantida vazia e dessa forma podemos copiar e colar as linhas
inteiras abaixo, sem preocupação em incluir ou excluir a palavra **sudo** no processo.
```bash
{ hash sudo 2>&- && ((UID>0)); } && sudo=sudo || sudo=
export DEBIAN_FRONTEND=noninteractive
$sudo apt update
```

Baixamos o código fonte. A forma tradicional de fazer isso para projetos versionados com `git` é clonando o repositório.
```bash
$sudo apt install -y git
git clone http://github.com/keymon/psdoom-ng
cd psdoom-ng/trunk
```

Para compilar o código é necessário um conjunto mínimo de pacotes voltados a esse objetivo. O meta-pacote `build-essential` instala esses pacotes.
```bash
$sudo apt install -y build-essential
```

Configuramos o código para compilação.
```bash
time ./configure
```

Eventualmente ainda podem faltar comandos ou bibliotecas. O comando `apt-file` permite identificar o nome dos pacotes que possuem um arquivo, dado seu caminho ou nome.
A escolha do pacote correto dentre os listados é de responsabilidade do usuário 🙂.
```bash
$sudo apt install -y apt-file
$sudo apt-file update
apt-file search -x sdl-config$
apt-file search -x SDL_mixer.h$
apt-file search -x SDL_net.h$

$sudo apt install -y automake1.11 libsdl{,-mixer,-net}1.2-dev

time ./configure
```

Finalmente compilamos.
```bash
time make
```

Do Ubuntu pelo menos 18.04 até o 23.04, o `gcc` permite uma compilação fluída:
```bash
gcc --version | head -n1
# gcc (Ubuntu 7.5.0-3ubuntu1~18.04) 7.5.0
# gcc (Ubuntu 12.3.0-1ubuntu1~23.04) 12.3.0
```

A partir do Ubuntu 23.10 (`gcc` 13.2.0) nos deparamos com o erro:
```
gcc -DHAVE_CONFIG_H -I. -I.. -I../opl -I../textscreen -I../pcsound -O2 -g -Wall -I/usr/include/SDL -D_GNU_SOURCE=1 -D_REENTRANT -MT i_sdlsound.o -MD -MP -MF .deps/i_sdlsound.Tpo -c -o i_sdlsound.o i_sdlsound.c
i_sdlsound.c: In function 'I_PrecacheSounds_SRC':
i_sdlsound.c:514:44: error: assignment of read-only location '*(src_data.data_in + (sizetype)((long unsigned int)sample_i * 4))'
  514 |                 src_data.data_in[sample_i] = data[sample_i] / 127.5 - 1;
      |                                            ^
```

Consultando [O Oráculo](http://google.com/search?q=i_sdlsound.c+fix+compilation), chegamos à uma [issue no **Chocolate Doom** de 2016](http://github.com/chocolate-doom/chocolate-doom/issues/788),
curiosamente fazendo referência a um erro muito parecido. Apesar de não ser exatamente o mesmo erro , a [solução adotada](http://github.com/chocolate-doom/chocolate-doom/commit/15b8e6e1e47e4f733f862c16a5c18a3485bd22d4)
serve perfeitamente para criar um patch aplicável ao caso de um Ubuntu >= 23.10.

Para criar o _patch_, começamos fazendo duas cópias do arquivo original: uma apenas para podermos voltar atrás se tudo der errado (`i_sdlsound.c-ori`) e outra onde faremos
manualmente as alterações necessárias (`i_sdlsound.c-new`). No fim, comparamos o inicial (`i_sdlsound.c`) com o novo usando o comando `diff`.
```bash
cd src
cp -a i_sdlsound.c i_sdlsound.c-ori
cp -a i_sdlsound.c i_sdlsound.c-new
vi i_sdlsound.c-new # alterações manuais
diff -Naru i_sdlsound.c{,-new} > i_sdlsound.c.patch
cat i_sdlsound.c.patch
```

O conteúdo do _patch_ será algo como:
```diff
--- i_sdlsound.c        2024-06-24 03:25:45.638761441 +0000
+++ i_sdlsound.c-new    2024-06-24 03:15:52.917510437 +0000

@@ -486,13 +486,15 @@
             float *rsound;
             uint32_t rlen;
            SRC_DATA src_data;
+           float *data_in;
 
            if (!LoadSoundLump(sound_i, &lumpnum, &samplerate, &length, &data))
                continue;
 
             assert(length <= LONG_MAX);
            src_data.input_frames = length;
-           src_data.data_in = malloc(length * sizeof(float));
+           data_in = malloc(length * sizeof(float));
+           src_data.data_in = data_in;
            src_data.src_ratio = (double)mixer_freq / samplerate;
 
            // mixer_freq / 4 adds a quarter-second safety margin.

@@ -511,7 +513,7 @@
                // whether a symmetrical range should be assumed.  The
                // following assumes a symmetrical range.
 
-               src_data.data_in[sample_i] = data[sample_i] / 127.5 - 1;
+               data_in[sample_i] = data[sample_i] / 127.5 - 1;
            }
 
             // don't need the original lump any more

@@ -525,7 +527,7 @@
             assert(src_data.output_frames_gen > 0);
             resampled_sound[sound_i] = src_data.data_out;
             resampled_sound_length[sound_i] = src_data.output_frames_gen;
-            free(src_data.data_in);
+            free(data_in);
             good_sound[sound_i] = true;
 
             // Track maximum amplitude for later normalization
```

Idealmente, o conteúdo do _patch_ deve ser absolutamente idêntico à saída do comando `diff`, por isso ele costuma ser redirecionado para um arquivo ao invés de enviado para
a tela para ser copiado e colado no arquivo do _patch_. Quando o conteúdo é copiado e colado, eventuais caracteres `tab` presentes nas linhas serão trocados por espaços e o
comando `patch` não reconhecerá as linhas que deveriam ser alteradas. Existem opções no comando `patch` para driblar essas situações, mas o melhor é tentar evitá-las. Para
não ser necessário copiar e colar em um arquivo o _patch_ mostrado acima, ele está disponível para download
[nesse endereço](https://gitlab.com/arkanon/misc/dicas-l/-/raw/master/dicas/3_em_1-compilando_em_container_criando_patches_e_matando_com_escopeta/i_sdlsound.c.patch). Basta
salvar o arquivo no diretório do clone do repositório do psDoom-NG e aplicar o comando `patch` indicando na opção `-p` quantos níveis de diretório devem ser ignorados na
identificação dos arquivos que serão alterados. Adicionalmente, a opção `--dry-run` permite que o comando `patch` apenas mostre o resultado do que seria executado, permitindo
analisar se as opções usadas foram adequadas e suficientes, ou não.  
```bash
cd src
$sudo apt install -y wget
wget -c gitlab.com/arkanon/misc/dicas-l/-/raw/master/dicas/3_em_1-compilando_em_container_criando_patches_e_matando_com_escopeta/i_sdlsound.c.patch
patch -p0 --dry-run < i_sdlsound.c.patch
# checking file i_sdlsound.c
```
Se o comando acima não indicou nenhum erro, ou pelo menos indicou erros aceitáveis:
```bash
patch -p0 < i_sdlsound.c.patch
# patching file i_sdlsound.c
```
Então, se a primeira tentativa de compilação falhou apenas devido ao erro que o _patch_ corrige, agora ela deve chegar ao fim sem tropeços:
```bash
cd ..
time make
```

Não sei vocês, mas eu sou um mero usuário 😢. Não saberia o que fazer com as informações de *debug* mantidas em um código compilado. Por isso, vou mandá-las para o
(*cyber*)espaço. Uma vantagenzinha obtida com isso é a possivelmente drástica redução do tamanho dos executáveis.
```bash
$sudo apt install -y file
lsb=$(find . -type f -exec file {} \; | grep -E 'LSB shared|LSB pie')
grep --color -E '[^/:]+:' <<< $lsb
exec=$(cut -d: -f1 <<< $lsb)
ls -la $exec
du -ch $exec
strip  $exec
```

Aqui podemos deixar de agir no _container_, se até agora estávamos. Se o código fonte foi compilado em uma distro-versão compatível com o _host_, os próximos passos
podem ser executados nela.

Agora o código compilado pode ser instalado. Ou esse passo pode ser ignorado, se estivermos apenas curiosos e sem planos de manter o binário. Podemos simplesmente executar o
**psDoom** diretamente do diretório em que foi compilado.
```bash
make install
```



# Configuração

Os parâmetros do aplicativo `src/psdoom` estão documentados no arquivo `CMDLINE`
```bash
cat CMDLINE
```

Os arquivos _default_ de configuração ficarão em `~/.psdoom`

O aplicativo `setup/psd-setup` pode ser usado para configurar algumas características do **psDoom**.  
Algumas podem ser configuradas pela interface do próprio **psDoom**, outras, apenas pelo `psd-setup` mesmo.

Configurando o **psDoom** pelo `psd-setup`, alguns parâmetros são armazenados no arquivo `default.cfg`, outros no arquivo `psd-doom.cfg`. Não sei se é um bug ou uma
característica, mas o jogo em si, usa o aquivo `psdoom.cfg`, e não `psd-doom.cfg`. Então, para poder usar o `psd-setup` e não ter que copiar ou mover o arquivo `psd-doom.cfg`
para `psdoom.cfg`, podemos resolver essa inconsistência com um *link* simbólico.
```bash
./setup/psd-setup

ln -fs psd-doom.cfg $HOME/.psdoom/psdoom.cfg
ls -la $HOME/.psdoom
```

Também podemos identificar os parâmetros que nos interessam e criar os arquivos de configuração já com eles no valor desejado, deixando de lado o `psd-setup`. Os comentários
acabarão removidos quando os outros parâmetros forem adicionados pelo **psDoom**.
```bash
mkdir -p $HOME/.psdoom
ln -fs psd-doom.cfg $HOME/.psdoom/psdoom.cfg

cat << EOT >| $HOME/.psdoom/psd-doom.cfg
  fullscreen      0    # Display  > Full screen  OFF
  screen_width    1024 # Display  > Window size  WIDTH
  screen_height   800  # Display  > Window size  HEIGHT
  show_endoom     0    # Display  > Advanced > Show ENDOOM screen on exit  OFF
  novert          1    # Mouse    > Allow vertical mouse movement  OFF
  grabmouse       1    # Mouse    > Grab mouse in windowed mode    OFF
  dclick_use      0    # Mouse    > Double click acts as "use"     OFF
  mouseb_use      1    # Mouse    > More buttons... > Use          RIGHT
EOT

cat << EOT >| $HOME/.psdoom/default.cfg
# joyb_speed      0    # Keyboard > Always run      OFF
  joyb_speed      29   # Keyboard > Always run      ON
  mouseb_forward  -1   # Mouse    > Move forward    NONE
  mouseb_strafe   2    # Mouse    > Strafe on       MIDLE
  mouseb_fire     0    # Mouse    > Fire weapon     LEFT
  sfx_volume      3    # Sound    > SFX volume      3
  snd_musicdevice 8    # Sound    > Music playback  Native MIDI
# snd_musicdevice 3    # Sound    > Music playback  OPL (Adlib/SB)
  music_volume    1    # Sound    > Music volume    1
  screenblocks    11   # tamanho da tela
EOT
```



# Execução

Parece que o **psDoom** utiliza apenas os níveis [E1M1 Hangar](http://doom.fandom.com/wiki/E1M1) no **Doom 1** e [MAP01 Entryway](http://doom.fandom.com/wiki/MAP01) no
**Doom 2** para associar aos processos.  
Além disso, por *default*, associa a monstros **todos** os processos disponíveis. É um comportamento um tanto "perigoso", encarado apenas pelos que não têm medo de morrer 😛  
Como sou franguinho, executarei apenas alguns aplicativos específicos e indicarei ao **psDoom** uma alternativa ao comando `ps`, de forma que ele veja apenas os aplicativos
"matáveis".

Assim, vamos lançar 12 instâncias do `xclock` e do `xeye` distribuídos em quantidades aleatórias, configurar o **psDoom** para "ver" apenas esses processos, e heróicamente
fechá-los a tiros de escopeta 😏  
São programinhas normalmente instalados por _default_. Se não estiverem, pertencem ao pacote `x11-apps`.
```bash
sudo apt install x11-apps
apps=(xclock xeyes) # c1

s=40; for i in {0..12}; { ${apps[((RANDOM%2))]} -geometry 100x100+$((s+(s+100)*i))+$((s+24)) & disown $!; } &> /dev/null # c2
```

Para atrelar processos a monstros, o **psDoom** usa linhas de dados no formato `USER PID CMD DEMON` onde `DEMON` é um dígito booleano indicando se o processo será associado
ao [ShotGun Guy](http://doom.fandom.com/wiki/Shotgun_Guy) (0) ou ao [Demon](http://doom.fandom.com/wiki/Demon) (1).  
Vamos usar um comando que substitua a saída padrão do `ps`. Vai mostrar no formato acima apenas os processos dos aplicativos desejados associando-os ao *ShotGun Guy*
(*boolean* 0). Também vai incluir o processo do próprio **psDooom**, associado-o ao *Demon* (*boolen* 1) só pra ver o que acontece quando este for morto... 😉

Abreviamos o `ps`, armazenando em uma variável o que for comum a todas as chamadas do comando.
```bash
ps="ps ho user,pid,comm -C" # c3
```

Processamos a saída do comando `ps` relativa aos aplicativos escolhidos e ao **psDoom**.
```bash
for i in ${apps[*]}; { $ps $i | sed -r 's/$/ 0/'; }; $ps psdoom | sed -r 's/$/ 1/' # c4
```

Utilizamos o modelo acima para indicar ao **psDoom** que **esse** é o comando `ps` que deve ser utilizado.
```bash
export PSDOOMPSCMD="for i in ${apps[*]}; { $ps \$i | sed -r 's/$/ 0/'; }; $ps psdoom | sed -r 's/$/ 1/'" # c5
```

Na execução do `ps` para buscar os processos e associá-los aos monstros é usada uma chamada ao `sh`. Como normalmente `/bin/sh` é um *link* apontando para algum *shell*
propriamente dito, e esse _shell_ possivelmnete é mais simplificado (como o `dash` no Ubuntu), uma forma de usar o `bash` sem modificar o código fonte é refazendo o *link*
`sh`.
```bash
sudo ln -fs bash /bin/sh
```

Indicamos também um diretório _default_ onde serão procurados os arquivos `wad` (dessa forma não será necessário apontar o caminho na chamado do jogo)
```bash
mkdir -p wad
cd wad

wget -qO- doomworld.com/3ddownloads/ports/shareware_doom_iwad.zip | zcat > doom.wad

wget -q   github.com/freedoom/freedoom/releases/download/v0.13.0/freedoom-0.13.0.zip
unzip -j freedoom-0.13.0.zip freedoom-0.13.0/freedoom2.wad
rm freedoom-0.13.0.zip

export DOOMWADPATH=$PWD # c6
```
[**Doom 2** Não teve versão shareware](http://doom.fandom.com/wiki/Shareware) então, para usar seu `IWAD`, é preciso uma cópia comercial do jogo.

E pronto. Executamos o **psDoom** indicando um `IWAD` 🎉
```bash
src/psdoom -iwad doom.wad -warp 1 # c7
```



# Solução de problemas

**Mensagem na execução relacionada ao `fluidsynth`** tentar sem sucesso promover sua prioridade de *thread* e bloquear o *soundfont* que está usando na memória não trocável
(*non-swappable memory*) para melhorar o desempenho.
```
fluidsynth: warning: Failed to pin the sample data to RAM; swapping is possible.
```

Temos duas causas possíveis: falta de recursos para o grupo `audio` **ou** (inclusivo) falta de acesso do usuário a esses recursos devido ao fato de não pertencer ao grupo
`audio`.  
Sendo apenas um *warning*, não há maiores consequências, mas desejando resolver, duas ações podem ser necessárias.

1. destinar os recursos necessários ao grupo `audio` aumentando os limites *default*.  
   Editamos como `root` o arquivo `/etc/security/limits.conf` e incluímos as linhas
```
@audio - rtprio  90
@audio - memlock unlimited
```

2. incluir o usuário no grupo `audio`:
```bash
sudo usermod -aG audio $USER
```

Tanto o aumento de recurso quanto a inclusão no grupo não serão visíveis pelo ambiente atual, mesmo reabrindo o terminal.
```bash
id | grep --color audio
```
Tradicionalmente, o próximo passo seria o doloroso processo de *logout*/*login*. Mesmo o comando `newgrp` não será suficiente nesse caso.
Mas ainda podemos evitar a reentrada na sessão gráfica fazendo um simples `ssh` na máquina local garantindo que o `X11Forwarding` seja utilizado.
```bash
ssh -X localhost
```
Isso exige que o pacote do `sshd` esteja instalado e que ele esteja configurado para aceitar `X11Forwarding`.
```bash
sudo apt install openssh-server
grep X11Forwarding /etc/ssh/sshd_config
```
O comando `grep` acima deve retornar a linha `X11Forwarding yes` não comentada com #. Se não retornar nada, ou retornar comentada ou descomentada mas com *no* ao invés de
*yes*, edite como `root` o arquivo `/etc/ssh/sshd_config` e ajuste nesse sentido. Depois reinicie o servidor `ssh`.
```bash
sudo systemctl restart sshd
```



**Mensagens na execução relacionadas ao `fluidsynth`** não encontrar o *soundfont* `TimGM6mb.sf2`
```
fluidsynth: error: Unable to open file "/usr/share/sounds/sf2/TimGM6mb.sf2"
fluidsynth: error: Couldn't load soundfont file
fluidsynth: error: Failed to load SoundFont "/usr/share/sounds/sf2/TimGM6mb.sf2"
```

Apesar de ser um *erro* e não apenas um *warning*, também não apresenta maiores consequências, uma vez que tanto a trilha quanto os efeitos sonoros soam (literalmente)
normais, pelo menos com o dispositivo de áudio `Native MIDI` configurado no arquivo `default.cfg`. Para resolver, basta instalar o pacote `timgm6mb-soundfont`:
```bash
sudo apt install timgm6mb-soundfont
```



## Referências

- http://imaginarycloud.com/blog/podman-vs-docker
- http://doomworld.com/classicdoom/info/shareware.php



☐
