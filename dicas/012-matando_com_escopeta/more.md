


Instalação do repositório oficial do `docker-ce`:
```bash
spath=/etc/apt/sources.list.d/docker.list
  tmp=/tmp/kr

sfile=${spath##*/}
kpath=/etc/apt/keyrings/${sfile%.*}.asc

. /etc/os-release
echo deb [arch=amd64,i386 signed-by=$kpath] https://download.docker.com/linux/ubuntu $VERSION_CODENAME stable | sudo tee $spath

apt_args=( -oDir::Etc::source{list=sources.list.d/$sfile,parts=-} -oAPT::Get::List-Cleanup=0 )
gpg_args=( --no-default-keyring --keyring $tmp --keyserver keyserver.ubuntu.com --recv-keys )
asc_args=( --no-default-keyring --keyring $tmp --output $kpath --armor --export --yes )
fprint=$(time sudo LC_ALL=C apt update "${apt_args[@]}" |& grep -oPm1 'NO_PUBKEY \K.*') &&
{
  time sudo LC_ALL=C gpg "${gpg_args[@]}" $fprint
  time sudo LC_ALL=C gpg "${asc_args[@]}"
}

rm $tmp
```

Instalação do `docker-ce` com isolamento do serviço em um _namespace_ Linux:
```bash
for file in /etc/sub{u,g}id; { grep -q $USER $file || echo $USER:100000:1 | sudo tee -a $file; }

file=/etc/docker/daemon.json
printf -v now '%(%Y%m%d-%H%M%S)T'
sudo mkdir -p /etc/docker
[[ -e $file ]] && sudo mv $file{,-$now}
echo -e "{\n  \"userns-remap\": \"$USER\"\n}" | sudo tee /etc/docker/daemon.json

sudo apt install -y docker-ce
```

Remoção do `docker-ce`
```bash
sudo apt remove -y docker-ce
sudo apt autoremove -y
```

Execução do _container_:
```bash
dir=dica
mkdir -p $dir
cd $dir

docker run --rm -ti -v $PWD:/$dir -w /$dir -h $dir --name $dir ubuntu bash
```



```bash
$sudo apt install -y g{cc,++}-12
g=/usr/bin/g
for i in 12 13; { $sudo update-alternatives --install ${g}cc gcc ${g}cc-$i $i --slave ${g}++ g++ ${g}++-$i; }
update-alternatives --get-selections
update-alternatives --query gcc
update-alternatives --list gcc
update-alternatives --set gcc /usr/bin/gcc-12
```



```bash
alias firefox='{ firefox -P dev & disown $!; } &> /dev/null'
```

```bash
app=xclock
s=40; for i in {0..12}; { $app -geometry 100x100+$((s+(s+100)*i))+$((s+24)) & }
export PSDOOMPSCMD="ps ho user,pid,comm -C $app | sed -r 's/ +/ /g; s/$/ 0/'"
```

Para abrir a janela numa posição pré-definida, uma solução é usar o `wmctrl`.  
Nesse caso vamos abri-la no canto inferior direito da tela.

Com um único monitor:
```bash
xwininfo -root | grep -E 'Width|Height'
```

Com mais de um monitor pode ficar complicado interpretar as dimensões apresentadas pelo `xwininfo`, dependendo da composição. Nesse caso talvez o comando `xrandr` seja mais prático.
```bash
xrandr
```

```bash
sw=1920 # screen width
sh=1080 # screen height

gw=1024 # game width  - conforme definido acima no arquivo de configuração psd-doom.cfg
gh=800  # game height - conforme definido acima no arquivo de configuração psd-doom.cfg

sudo apt install wmctrl

src/psdoom -nograbmouse -iwad doom2.wad -warp 1 & { disown $!; sleep 1; wmctrl -r hell -e 0,$((sw-2-gw)),$((sh-28-gh)),-1,-1; } # c6
```

O processo do **psDoom** foi iniciado em *background*, então uma forma de finalizá-lo adequadamente por linha de comando é enviando um sinal `QUIT` para ele
```bash
killall -QUIT psdoom # c8
```

Finalmente, se sobrou algum aplicativo de teste aberto, também podemos fechá-lo pelo terminal
```bash
killall ${apps[*]} # c9
```

```bash
src/psdoom -iwad doomu.wad -file ../../psdoom-ng1-orsonteodoro/psdoom-data/psdoom1.wad -warp 11 &
src/psdoom -iwad doom2.wad -file ../../psdoom-ng1-orsonteodoro/psdoom-data/psdoom2.wad -warp 1  &
```

A sessão gráfica atual não vai saber que o usuário está no novo grupo, mesmo reabrindo o terminal.
```bash
id | grep --color audio
```

Para evitar o doloroso processo de *logout*/*login*:
```bash
export PGRP=$(id -gn) # grupo primário
newgrp audio
id | grep --color audio # agora o usuário é visto no grupo "audio", mas seu grupo primário também foi modificado
newgrp $PGRP
id | grep --color audio # agora o usuário continua sendo visto no grupo "audio" e seu grupo primário voltou a ser o correto
```
Lamentavelmente vai continuar não "mostrando" o novo grupo para a sessão gráfica ou novos terminais, apenas para o *shell* que executou o `newgrp`. Mas já é alguma coisa...



Existe outra versão do projeto mas, pelo menos no **Ubuntu 18.04**, apesar de compilado com sucesso, não executou...

```bash
git clone https://github.com/keymon/psdoom-ng1 psdoom-ng1-orsonteodoro
cd psdoom-ng1-orsonteodoro
tar zxf contrib/psdoom-2000.05.03-data.tar.gz
l psdoom-data

time ./autogen.sh

vi src/Makefile
# comentar linhas:
# 767 | app_DATA = \
# 768 |            psdoom-ng.desktop

vi Makefile
# remover linha:
# 453 | CHANGELOG.psdoom-ng \

time make
l src/psd*
strip src/psdoom-ng{,-doom,-setup}
cat psd
l $HOME/.psdoom-ng
export DOOMWADPATH=/wad/internal
src/psdoom-ng-setup
src/psdoom-ng -window -iwad doomu.wad -file psdoom-data/psdoom1.wad
src/psdoom-ng -window -iwad doom2.wasudo usermod -aG audio $USERd -file psdoom-data/psdoom2.wad

# segmentation fault # :-/
```

