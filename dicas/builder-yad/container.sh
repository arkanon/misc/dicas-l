#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2024/07/08 (Mon) 10:25:54 -03
# 2024/07/07 (Sun) 16:28:24 -03



. /etc/os-release

user=v1cont
name=yad
repo=https://github.com/$user/$name
imgv=latest



args=(

# --rm
  --detach

  --interactive

  --env user
  --env name
  --env repo

  --volume   $PWD/$name-build:/$name-build
  --hostname      $name-build
  --name          $name-build
  --workdir      /$name-build

)



export user name repo

mkdir -p $name-build
cp -a build.sh $name-build

time podman run "${args[@]}" ubuntu:$imgv # ./build.sh |& tee $name-build/build.log
###  1.26GB  3m51,985s  217M  [24.04]



# EOF
