<!--
# Arkanon <arkanon@lsd.org.br>
# 2025/02/20 (Thu) 05:22:48 -03
# 2024/07/08 (Mon) 09:42:59 -03
# 2024/07/07 (Sun) 16:25:10 -03
# 2024/07/04 (Thu) 04:44:09 -03
# 2023/04/29 (Sat) 02:46:44 -03
# 2022/11/23 (Wed) 13:44:55 -03
# 2022/09/14 (Wed) 20:32:27 -03
# 2022/09/13 (Tue) 01:52:32 -03
# 2022/09/12 (Mon) 07:00:49 -03
# 2022/03/03 (Thu) 03:32:27 -03
# 2022/03/02 (Wed) 23:33:41 -03
# 2021/07/27 (Tue) 11:51:41 -03
# 2021/07/27 (Tue) 01:21:12 -03
# 2021/07/25 (Sun) 20:47:34 -03
# 2021/07/21 (Wed) 17:45:54 -03
# 2021/05/05 (Wed) 04:10:01 -03
# 2021/03/17 (Wed) 04:04:56 -03
# 2021/02/18 (Thu) 11:25:55 -03
-->

Este documento (url's de referência em [português](https://ishortn.ink/yad-build-pt) e [english](https://ishortn.ink/yad-build-en))
é um roteiro de compilação do `yad` a partir de seu repositório no GitHub. O método foi testado em containers `podman` rodando **Ubuntu 24.04 e 22.04** e a execução via
`chroot` do binário foi testada em um _container_ **Ubuntu 16.04**.

**yad** é uma aplicação de autoria de [Victor Ananjevsky](http://linkedin.com/in/victor-ananjevsky-68589415) que permite criar interfaces gráficas para procedimentos em
  _shell script_ no mesmo espírito de seu companheiro **[zenity](http://gitlab.gnome.org/GNOME/zenity)**, mas com bem mais recursos.
Isso dito, qualquer outra informação a respeito do seu uso é melhor obtida com os tutoriais e exemplos do Professor [Julio Neves](http://linkedin.com/in/juliocezarneves),
  como [essa introdução](http://dicas-l.com.br/arquivo/uma_breve_apresentacao_do_yad__yet_another_dialog_.php) publicada na lista **Dicas-L**, por exemplo.



# Sumário
[[_TOC_]]



# Referências
- [Repositório oficial no GitHub](http://github.com/v1cont/yad)
- [Releases](http://github.com/v1cont/yad/releases)
- [YAD Guide](https://yad-guide.ingk.se)



# Diferenças entre versões
- [yad 0.40.0  x 14.1 ]()
- [yad 0.40.0  x 11.0 ](http://diffchecker.com/zT15KYn0)
- [yad 10.0    x 10.90](http://diffchecker.com/vxBBEYk9)
- [yad  9.1    x 10.0 ](http://diffchecker.com/OdrB9oUd)
- [yad  8.0    x  9.1 ](http://diffchecker.com/Q2QrYtSO)
- [yad  0.40.0 x  8.0 ](http://diffchecker.com/hpINtzKs)



# TL;DR
Para quem é macaco velho e só quer o "caminho da roça", o primeiro _script_ abaixo sobe um _container_ `podman` onde executa o segundo _script_ que, por sua vez, em um
subdiretório `yad/`, baixa e compila o código fonte no Ubuntu 24.04 e então coloca os binários resultantes e todas as suas dependências em outro subdiretório, o que
permite a execução dos binário compilados basicamente em qualquer versão de qualquer distribuição via `chroot`.

1. [container.sh](./container.sh)
2. [build.sh](./build.sh)
3. Execução via `chroot` no diretório `yad-<yad_version>--ubuntu-<ubuntu_version>--<build_date_and_time>/` criado após a compilação do código fonte:
   ```bash
   builds=$(ls -d yad-build/yad-*)
   root=${builds##*$'\n'}
   echo "$root"

   run()
   {
     sudo true || return 1
     local bin=$1 dirs=( tmp/.X11-unix run/user/$UID/at-spi dev usr/share/icons )
   # local userspec=--userspec=$(id -u):$(id -g)
     shift
     for i in ${dirs[*]}; { mkdir -p $root/$i; sudo mount -B /$i ${root?}/$i; }
     LC_ALL=C sudo chroot $userspec $root $bin "$@"
     for i in ${dirs[*]}; { sudo umount ${root?}/$i; }
   }

   xhost + local:

   run yad --version
   run yad --help-all
   run yad

   run yad-icon-browser

   run yad-tools -c --pick

   run yad-settings
   ```


# Detalhamento



## Preparação do ambiente de compilação

O comando `sudo` será necessário quando o usuário não for o _root_. Se o usuário for _root_ e o comando não estiver instalado (como normalmente em containers), então não
deverá ser usado, uma vez que a composição de comando resultante será considerada um erro.

O comando abaixo popula uma variável `$sudo` que será usada como comando nessas condições (`sudo` ou "nada")

```bash
((UID>0)) && hash sudo 2>&- && sudo="sudo -E"; echo $sudo
```



Variáveis auxiliares:
```bash
export user=v1cont
export name=yad
export repo=https://github.com/$user/$name
ldir=/export/src/git/github.com # diretório local do clone do repositório
```



Diretório de trabalho:
```bash
$sudo mkdir -p     /export
$sudo chown $USER: /export

mkdir -p $ldir
cd       $ldir

mkdir -p $name-build
cd       $name-build
```



<details>

<summary>Compilação do código fonte em um container</summary>

Uma boa abordagem para compilar o código fonte é fazê-lo em um _container_. Isso garante tanto que o procedimento não sofrerá interferência de customizações aplicadas à
instalação do _host_, quanto que não "poluirá" com pacotes de desenvolvimento uma instalação que provavelmente não tem esse objetivo. O uso de um _container_ em detrimento
ao de uma VM também traz vantagens, principalmente na simplicidade de integração do resultado da compilação com o _host_. Além disso, com a mesma facilidade, tem-se acesso
não só à mesma versão "limpa" de SO instalado no _host_, como à qualquer versão de qualquer distribuição, simplificando a possibilidade de uma eventual criação de pacote
com o binário resultante.

A opção pelo uso do [`podman`](https://podman.io) em vez do popular [`docker`](https://docker.io) é devida a algumas características interessantes do `podman` para o uso
proposto acima. Em especial, ele é executado sem a necessidade de um _daemon_ e de privilégios de _root_ o que o torna mais leve e mais seguro. Também, por ser executado
como usuário, um diretório do _host_ compartilhado como volume para o _container_ terá seu conteúdo gravado pelo root do _container_ automaticamente pertencente ao usuário
que o executou, o que torna transparente o intercâmbio entre o que é feito do lado do _host_ e do que é feito do lado do _container_.

Apesar do `podman` estar presente nos repositórios padrão do Ubuntu, sua versão normalmente é mais antiga, especialmente nas distribuições mais antigas. Por isso, uma
alternativa à compilação do seu código fonte é o uso de repositórios de terceiros, como mostrado abaixo. O Ubuntu 22.04.4, por exemplo, vem com o `podman` 3.4.3, enquanto
que esse repositório tem a versão 4.6.2 para esse mesmo Ubuntu.



### Instalação do repositório do `podman`
```bash
. /etc/os-release

      tmp=/tmp/kr
   source=podman

    spath=/etc/apt/sources.list.d/$source.list
    kpath=/etc/apt/keyrings/$source.asc

  options="arch=amd64,i386 signed-by=$kpath"
      uri="https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/unstable/xUbuntu_$VERSION_ID"
    suite=/
component=

   re='^([^-/]* )?\K[^-/]+(?=.* [0-9][^ ]+ [^ ]+ +-$)'
dists=$(lynx -width=100 -dump -nolist $uri/dists | grep -oP "$re" | sort -u) # distros disponíveis
     # ex:
     # uri=https://ppa.launchpadcontent.net/openshot.developers/libopenshot-daily/ubuntu
     # uri=https://esm.ubuntu.com/apps/ubuntu

apt_args=( -oDir::Etc::source{list=sources.list.d/${spath##*/},parts=-} -oAPT::Get::List-Cleanup=0 )
gpg_args=( --no-default-keyring --keyring $tmp --keyserver keyserver.ubuntu.com --recv-keys )
asc_args=( --no-default-keyring --keyring $tmp --output $kpath --armor --export --yes )

echo deb [$options] $uri $suite $components | $sudo tee $spath

fprint=$(time sudo LC_ALL=C apt update "${apt_args[@]}" |& grep -oPm1 'NO_PUBKEY \K.*' | tee /dev/stderr) &&
{
  time sudo LC_ALL=C gpg "${gpg_args[@]}" $fprint
  time sudo LC_ALL=C gpg "${asc_args[@]}"
  $sudo rm $tmp
}

apt list -a podman 2>&- | grep --color -P '[ :]\K[0-9]+\.[^+~-]+(?=.*amd64)'
# podman/desconhecido,now 4:4.6.2-0ubuntu22.04+obs81.12 amd64
# podman/jammy-updates,jammy-security 3.4.4+ds1-1ubuntu1.22.04.2 amd64
# podman/jammy 3.4.4+ds1-1ubuntu1 amd64
```



### Instalação do `podman`
```bash
$sudo apt install -y podman containernetworking-plugins
$sudo usermod --add-subuids 100000-165535 --add-subgids 100000-165535 $USER
cat /etc/sub?id
podman system migrate
ls -la ~/.local/share/containers
```



### Configuração do _container_
```bash
expose=(
  $PWD:/src
# $HOME/.Xauthority:/{root,build}/.Xauthority:rw
  /tmp/.X11-unix
  /run/user/$UID/bus
# /run/dbus/system_bus_socket
)



args=(

# --rm
  --detach

  --interactive

  --cap-add SYS_PTRACE # strace -f strace /bin/ls failed with PTRACE_TRACEME EPERM (Operation not permitted) <http://stackoverflow.com/a/49820875>
  --cap-add SYS_ADMIN  # How do I mount --bind inside a Docker container? <http://stackoverflow.com/a/36554252>

  --env user
  --env name
  --env repo
  --env DISPLAY
  --env DBUS_SESSION_BUS_ADDRESS
  --env locale=pt_BR.utf8
  --env HISTCONTROL=
  --env DEBIAN_FRONTEND=noninteractive
  --env TZ=$(</etc/timezone)
  --env NO_AT_BRIDGE=1 # para sanar o erro do yad: dbind-WARNING **: 18:13:55.012: Couldn't connect to accessibility bus: Failed to connect to socket /run/user/10101/at-spi/bus: No such file or directory

  --workdir /src

  --userns  keep-id
  --user    $(id -u):$(id -g) # exigirá presença do comando sudo com a devida configuração em /etc/sudoers

  $( for i in ${!expose[@]}; { echo --volume ${expose[i]%%:*}:${expose[i]#*:}; } )

)

declare -p args | tr ' ' '\n'
```



### Execução do _container_
```bash
. /etc/os-release

imgv=$VERSION_ID
imgv=latest

podman images
podman ps -as

podman rm -ft0 build
podman run "${args[@]}" --{host,}name=build ubuntu:$imgv
# 1.47kB [22.04]
# 14.2kB [24.04]

podman container inspect build | jq '.[]|keys'
podman container inspect build | jq '.[]|.State|keys'
podman container inspect build | jq '.[]|.State.Pid'

podman container inspect build -f '{{.State.Pid}}'

podman exec -tiu root build bash -c "apt update; apt install -y sudo; echo 'ALL ALL=(ALL:ALL) NOPASSWD:SETENV:ALL' > /etc/sudoers; rm ~/.{bashrc,profile}"
# 55.8MB [22.04]
# 40.7MB [24.04]

podman exec -ti build bash -l
```



### Configuração básica do _container_
```bash
alias l="LC_ALL= LC_NUMERIC=pt_BR.UTF-8 BLOCK_SIZE=\'1 ls -lapisvT0 --quoting-style=shell --color=always --time-style='+%Y/%m/%d %a %T %:::z' --group-directories-first"
((UID>0)) && hash sudo 2>&- && sudo="sudo -E"; echo $sudo

apt list --upgradable -a
time $sudo apt install -y apt-utils
time $sudo apt upgrade -y
time $sudo apt install -y dialog command-not-found ncurses-term readline-common locales tzdata gnupg
# [22.04]
#
# [24.04]
# 0 upgraded, 46 newly installed, 0 to remove and 0 not upgraded.
# Need to get 19.8 MB of archives.
# After this operation, 82.2 MB of additional disk space will be used.
# 0m30.443s
# 110MB

bind -f /etc/inputrc

$sudo locale-gen $locale

locale -a
export LANG{,UAGE}=$locale
export LC_{CTYPE,NUMERIC,TIME,MONETARY,MESSAGES,PAPER,NAME,ADDRESS,TELEPHONE,MEASUREMENT,IDENTIFICATION}=$locale
export LC_COLLATE=C
locale
grep ${locale%.*} /etc/locale.gen

date
```

</details>



## Procedimento propriamente dito (dentro ou fora de _container_)



### Pacotes necessários ou úteis para compilação de código versionado em git
```bash
time $sudo apt install -y git{,-lfs} curl jq # apt-file apt-rdepends checkinstall
# [22.04]
#
# [24.04]
# 0 upgraded, 91 newly installed, 0 to remove and 0 not upgraded.
# Need to get 39,0 MB of archives.
# After this operation, 167 MB of additional disk space will be used.
# 0m37,962s
# 267MB

time $sudo apt-file update
#                     [22.04]
# 0m15,888s   378MB   [24.04]
```



### Pacotes úteis para aplicações de interface gráfica
```bash
time $sudo apt install -y gdebi vim-gtk3 yad fonts-noto-cjk hicolor-icon-theme
# [22.04]
#
# [24.04]
# 0 upgraded, 354 newly installed, 0 to remove and 0 not upgraded.
# Need to get 201 MB of archives.
# After this operation, 640 MB of additional disk space will be used.
# 3m37,633s
# 980MB
```



### Verificação do `yad` da distribuição
```bash
yad
```

Se na execução for recebida a mensagem
```
Gtk-WARNING **: 16:40:11.332: Error loading icon from file 'yad':
        Image file “yad” contains no data
```
possivelmente o diretório em que está sendo executado o `yad` possui um arquivo ou diretório com o mesmo nome (`yad`), provavelmente o clone do repositório git.
Basta renomeá-lo.



Se o _locale_ `pt_BR.utf8` foi instalado e a sessão do _container_ não foi reiniciada, as mensagens abaixo podem ser exibidas na execução de comandos que envolvam _locale_.
(Sair e retornar ao _container_ vai resolver o problema, mas como resolvê-lo sem sair?)
```
bash: warning: setlocale: LC_NUMERIC: cannot change locale (): No such file or directory
bash: warning: setlocale: LC_NUMERIC: cannot change locale (pt_BR.utf8)

Gtk-WARNING **: 06:03:54.514: Locale not supported by C library.
        Using the fallback 'C' locale.
```



### Identificação da distribuição usada e versão do `yad` instalado a partir dos repositórios
```bash
. /etc/os-release

# [22.04]
echo $PRETTY_NAME
# Ubuntu 22.04.4 LTS
yad --version
# 0.40.0 (GTK+ 3.24.33)

# [24.04]
echo $PRETTY_NAME
# Ubuntu 24.04 LTS
yad --version
# 0.40.0 (GTK+ 3.24.41)

which -a yad
# /usr/bin/yad
# /bin/yad
```



### Pacotes para compilação do código fonte
- `libgtk-3-dev              ` GTK
- `libgtksourceviewmm-3.0-dev` SOURCEVIEW
- `libwebkit2gtk-4.{0,1}-dev ` HTML
- `libgspell-1-dev           ` SPELL
- `?                         ` PFD
```bash
apt-rdepends    intltool
apt-rdepends -r intltool

time $sudo apt install -y build-essential intltool gawk lib{gtk-3,gtksourceviewmm-3.0,webkit2gtk-4.1,gspell-1}-dev
# [22.04]
#
# [24.04]
# 0 upgraded, 253 newly installed, 0 to remove and 0 not upgraded.
# Need to get 132 MB of archives.
# After this operation, 569 MB of additional disk space will be used.
# 2m7,748s
# 1.54GB
```



### Identificação do número da versão e _download_ do código fonte

<details>

<summary>Releases disponíveis</summary>

```bash
curl -s https://api.github.com/repos/$user/$name/releases |
  jq -r '.[] | [ "#", (.tag_name|gsub("v";"")), (.published_at|gsub("[TZ]";" ")) ] | join("\t")'
#  14.1    2024-07-04 07:14:42
#  14.0    2024-06-21 09:58:33
#  13.0    2023-05-31 12:59:04
#  12.3    2022-12-16 09:28:52
#  12.2    2022-12-15 10:28:22
#  12.1    2022-11-22 06:04:20
#  12.0    2022-05-03 08:25:23
#  11.1    2022-04-10 17:44:39
#  11.0    2022-03-02 14:45:43
#  10.1    2021-05-05 07:55:03
#  10.0    2021-05-04 13:43:47
#   9.3    2021-04-13 09:59:43
#   9.2    2021-04-05 10:42:06
#   9.1    2021-03-16 11:46:50
#   9.0    2021-03-14 13:29:58
#   8.0    2021-02-17 17:31:54
#   7.3    2020-10-21 10:25:10
#   7.2    2020-09-24 03:37:23
#   7.1    2020-09-07 09:48:03
#   7.0    2020-09-04 14:22:34
#   6.0    2020-04-04 05:46:55
#   5.0    2019-10-30 04:46:32
#   4.1    2019-08-11 10:44:57
#   4.0    2019-08-11 08:34:49
#   3.0    2019-08-02 03:05:14
#   2.0    2019-07-23 11:39:48
#   1.0    2019-07-04 07:32:37
#   0.42.0 2019-02-23 13:16:23
#   0.41.0 2019-02-06 11:47:07
```

</details>

Usando a última _release_:
```bash
v=$(curl -s https://api.github.com/repos/$user/$name/releases/latest | jq -r '.tag_name | gsub("v";"")')
echo $v
# 14.0
curl -sL {$repo/archive/refs/tags/v,-o$name-}$v.tar.gz
tar ztvf $name-$v.tar.gz
tar zxf  $name-$v.tar.gz
dir=$PWD/$name-$v
cd $dir
```

**OU**

Usando a _rolling release_:
```bash
dir=$PWD/git
[[ -d $dir/.git ]] || git clone $repo $dir
cd $dir
git pull
v=$(sed -r '/AC_INIT/!d;s/.+\[([0-9.]+)\].+/\1/' configure.ac)
echo $v
# 14.0
```



### Compilação
```bash
make distclean 2>&- || echo Nada a limpar

autoreconf -ivf && intltoolize --force

standalone=--enable-standalone # compila sem integração com o gsetings

./configure $standalone | grep --color -E '|.*\bno\b.*'

time make
# 0m12,529s [22.04]
# 0m17,726s [24.04]

BLOCK_SIZE=\'1 ls -lsgG src/yad
# 1.093.632 -rwxr-xr-x 1 1.093.016 jul  2 19:13 src/yad [22.04]
# 1.069.056 -rwxr-xr-x 1 1.066.936 jul  7 13:08 src/yad [24.04]

strip src/yad
strip src/yad-icon-browser
strip src/yad-tools

BLOCK_SIZE=\'1 ls -lsgG src/yad
#   315.392 -rwxr-xr-x 1   312.528 jul  2 19:14 src/yad [22.04]
#   294.912 -rwxr-xr-x 1   292.048 jul  7 13:08 src/yad [24.04]
```



## Execução



### Sem instalação

É possível executar o binário sem instalá-lo, mas para isso é preciso compilar suas configurações do `gsettings`:
```bash
mkdir -p share/glib-2.0
[[ -e share/glib-2.0/schemas ]] || ln -s ../../data share/glib-2.0/schemas
glib-compile-schemas $PWD/share/glib-2.0/schemas/
l -tr $PWD/share/glib-2.0/schemas/

export XDG_DATA_DIRS=$PWD/share:/usr/share
export PATH=$PWD/src:$PATH
```



```bash
yad

yad --version
# 14.1 (GTK+ 3.24.33) [22.04]
# 14.1 (GTK+ 3.24.41) [24.04]
```
![yad](yad.png)



```bash
yad-icon-browser
```
![yad-icon-browser](yad-icon-browser.png){width=400px}



```bash
yad-tools --help-all

yad-tools -c --pick

yad-tools --show-langs
# en
# en_AU
# en_CA
# en_US
# en_GB

yad-tools --show-themes
# Classic
# Cobalt
# Kate
# Oblivion
# Solarized Dark
# Solarized Light
# Tango
```



```bash
chmod +x src/yad-settings
yad-settings
cat src/yad-settings
```
![yad-settings](yad-settings.png){width=400px}



Eventualmente a execução de alguma aplicação gráfica a partir do _container_ com o X11-Forwarding ativado pode falhar e uma mensagem semelhante à abaixo pode ser exibida:
```
(yad:13620): Gdk-ERROR **: 09:00:58.618: The program 'yad' received an X Window System error.
This probably reflects a bug in the program.
The error was 'BadAccess (attempt to access private resource denied)'.
  (Details: serial 220 error_code 10 request_code 130 (MIT-SHM) minor_code 1)
  (Note to programmers: normally, X errors are reported asynchronously;
   that is, you will receive the error a while after causing it.
   To debug your program, run it with the GDK_SYNCHRONIZE environment
   variable to change this behavior. You can then get a meaningful
   backtrace from your debugger if you break on the gdk_x_error() function.)
Trace/breakpoint trap
```
Apenas ignore-a e execute novamente o aplicativo.



### Com instalação

O aplicativo pode ser instalado a partir da árvore do código fonte. Dessa forma, seus arquivos não serão gerenciáveis pelo gerenciador de pacotes,
  dificultando sua manutenção e remoção.
```bash
$sudo make install
```
MAS, quando o Makefile contiver a tag `uninstall` (como é o caso no `yad`), pode-se removê-los com:
```bash
$sudo make uninstall
```

Se não houver interesse em distribuir e/ou gerenciar várias versões de pacotes compilados, o processo pode terminar por aqui.



### Via `chroot`

Outra opção de execução é através do comando `chroot`.

A principal vantagem do método é poder executar o binário, mesmo dinamicamente linkado, em uma grande variedade de distribuições e versões, mais antigas ou mais
novas do que a que foi usada na compilação. A principal desvantagem é precisar executar o _jail_ como _root_.

É uma alternativa que segue a linha dos containers (docker/containerd) e dos empacotamentos Flatpak, AppImage e Snap, mas muito mais básica, simplificada e enxuta.
E exige uma significativa dose de confiança, uma vez que depende do comando `sudo`.



```bash
cd $dir/..

ts=$(stat -c%Y ${dir?}/src/yad)
printf -v build '%(%Y%m%d-%H%M%S)T' ${ts?}
root=$name-$v--$ID-$VERSION_ID--$build
echo $root



bin=(

      # binários do yad
      $dir/src/yad
      $dir/src/yad-icon-browser
      $dir/src/yad-tools
      $dir/src/yad-settings

      # binários usados pelo yad-settings
      /bin/env
      /bin/bash
      /bin/mktemp
      /bin/rm
      /bin/gsettings
      /bin/update-mime-database

      # binários auxiliares
      /bin/ls
      /bin/ln
      /bin/cp
      /bin/mv
      /bin/cat
      /bin/grep
      /bin/chmod
      /bin/mkdir
      /bin/rmdir
      /bin/less
      /bin/ldd
      /bin/xauth

    )



cpdeps()
{
  local bin=${1?} dir=${2?} libs
  echo Copying $bin
  libs=$(ldd $bin 2>&-)
  grep -w not <<< $libs && return
  libs=$(cut -d\  -f3 <<< $libs)
  mkdir -p $dir/{lib64,lib,bin}
  cp -aL $bin  $dir/bin
  [[ $libs ]] &&
  {
    cp -aL $libs $dir/lib
    cp -aL /lib64/ld-linux-x86-64.so.2 $dir/lib64
  }
}



populate()
{

  rm -rf ${root?}

  for i in ${bin[*]}; { cpdeps $i $root; }

  mkdir -p                             $root/usr/share/glib-2.0/schemas
  cp    -a $dir/data/gschemas.compiled $root/usr/share/glib-2.0/schemas

  mkdir -p                             $root/usr/share/mime
  cp    -a                          {,$root}/usr/share/mime/packages

  mkdir -p                             $root/etc/fonts
  mkdir -p                             $root/var/cache/fontconfig
  mkdir -p                             $root/usr/share/fonts/opentype/noto
  cp    -a                          {,$root}/usr/share/fonts/opentype/noto/NotoSansCJK-Regular.ttc

  ln    -s                             ../bin $root/usr

  cat << EOT > $root/etc/fonts/fonts.conf
<fontconfig>
<dir>/usr/share/fonts</dir>
<cachedir>/var/cache/fontconfig</cachedir>
</fontconfig>
EOT

  [[ -e $root/bin/bash ]] &&
  cat << EOT > $root/etc/bash.bashrc
unset LC_COLLATE LANG LANGUAGE
export           PS1='$ '
export          HOME=/
export       DISPLAY=:0.0
export XDG_DATA_DIRS=/usr/share
alias              l='ls -lap --color'
EOT

  time LC_ALL=C $sudo chroot $root update-mime-database /usr/share/mime

  du -hs $root

}

populate
#             215M [22.04]
# 1m38,042s   217M [24.04]



run()
{
  sudo true || return 1
  local bin=$1 dirs=( tmp/.X11-unix run/user/$UID/at-spi dev usr/share/icons )
# local userspec=--userspec=$(id -u):$(id -g)
  shift
  for i in ${dirs[*]}; { mkdir -p $root/$i; sudo mount -B /$i ${root?}/$i; }
  LC_ALL=C sudo chroot $userspec $root $bin "$@"
  for i in ${dirs[*]}; { sudo umount ${root?}/$i; }
}

xhost + local:

alias              yad0='run yad'
alias yad-icon-browser0='run yad-icon-browser'
alias        yad-tools0='run yad-tools'
alias     yad-settings0='run yad-settings'

yad0 --version
yad0 --help-all
yad0

yad-icon-browser0

yad-tools0 -c --pick

yad-settings0
# (yad:14578): Gtk-WARNING **: 16:18:27.376: Could not load a pixbuf from /org/gtk/libgtk/theme/Adwaita/assets/bullet-symbolic.svg.
# This may indicate that pixbuf loaders or the mime database could not be found.

time tar jcf $root.tar.bz2 $root
# 0m18.116s   217.007.622 --> 83.661.443 (38,552%) [22.04]
# 0m20,698s   223.588.816 --> 86.472.944 (38,675%) [24.04]

time tar Jcf $root.tar.xz  $root
# 1m36.443s   217.007.622 --> 64.342.456 (29,650%) [22.04]
# 2m4,614ss   223.588.816 --> 66.781.112 (29,868%) [24.04]

tar Jtf $root.tar.xz
tar Jxf $root.tar.xz
```



Teste em uma versão antiga da distribuição
```bash
imgv=16.04

podman rm -ft0 $name-test
podman run "${args[@]}" --{host,}name=$name-test ubuntu:$imgv
podman exec -tiu root $name-test bash -c "apt update; apt install -y sudo; echo 'ALL ALL=(ALL:ALL) NOPASSWD:SETENV:ALL' > /etc/sudoers; rm ~/.{bashrc,profile}"
podman exec -ti $name-test bash

  root=yad-14.1--ubuntu-24.04--20240707-130859
# printf -v root yad/yad-*
  echo "$root"

run()
{
  sudo true || return 1
  local bin=$1 dirs=( tmp/.X11-unix run/user/$UID/at-spi dev usr/share/icons )
# local userspec=--userspec=$(id -u):$(id -g)
  shift
  for i in ${dirs[*]}; { mkdir -p $root/$i; sudo mount -B /$i ${root?}/$i; }
  LC_ALL=C sudo chroot $userspec $root $bin "$@"
  for i in ${dirs[*]}; { sudo umount ${root?}/$i; }
}

xhost + local:

run yad
run yad-icon-browser
run yad-tools
run yad-settings
```

</details>



## Comparação do help da versão compilada com o da versão do repositório

O pacote será instalado como uma atualização ao yad do repositório.
Para retornar essa versão padrão, primeiro será necessário remover o pacote da versão nova:
```bash
for i in $(which -a $name); { echo -n "$i "; $i --version; } | column -ts' '
# /usr/local/bin/yad  11.0  (GTK+  3.24.30)
vn=$(yad --version | grep -oE '^[^ ]+')
{ yad --version; yad --help-all; } > yad-help-$vn

$sudo apt remove  -y yad
$sudo apt install -y yad
eval PATH=$(echo $PATH) # atualiza como a sessão vê o PATH
for i in $(which -a $name); { echo -n "$i "; $i --version; } | column -ts' '
# /usr/bin/yad  0.40.0  (GTK+  3.24.30)
# /bin/yad      0.40.0  (GTK+  3.24.30)
vo=$(yad --version | grep -oE '^[^ ]+')
{ yad --version; yad --help-all; } > yad-help-$vo

gvim -d yad-help-*
```



☐
