


Mostra como usar o `checkinstall` (uma aplicação abandonada mas ainda útil e funcional) para gerar um pacote `.deb` que permite fácil instalação via `gdebi` e fácil remoção
  com gerenciadores de pacote como `apt` e `synaptic`.
- [How to manage multiple package dependencies with checkinstall](http://stackoverflow.com/a/35360317)

# TODO
- [ ] `Dockerfile` para compilação em massa entre várias (versões de )?distro
- [ ] criar _script_ `postinstall` para executar essa tarefa e incluí-lo no pacote `.deb`
- [ ] criação de ppa
- [ ] procedimento de envio para o ppa



<details>

<summary>Instalação do repositório PPA do Docker CE</summary>

```bash
  # WORKAROUND para o erro
  #   http://security.ubuntu.com/ubuntu/dists/kinetic-security/InRelease:
  #   The key(s) in the keyring /etc/apt/trusted.gpg.d/ubuntu-keyring-2012-cdimage.gpg
  #   are ignored as the file is not readable by user '_apt' executing apt-key.
# apt_opts='-oAcquire::AllowInsecureRepositories=true -oAcquire::AllowDowngradeToInsecureRepositories=true --allow-unauthenticated'

  . /etc/os-release
  repo_name=docker
  repo_data="https://download.docker.com/linux/$ID/ $VERSION_CODENAME stable"
  repo_file=/etc/apt/sources.list.d/$repo_name.list
  repo_keyr=/etc/apt/keyrings/$repo_name.gpg

  echo deb [arch=amd64 signed-by=$repo_keyr] $repo_data > $repo_file
  cat $repo_file
  fingerprint=$($sudo apt -y $apt_opts update -oDir::Etc::source{list=$repo_file,parts=-} -oAPT::Get::List-Cleanup=0 |& grep -oE [[:xdigit:]]{16}$i | sort -u)
  echo $fingerprint
  if [[ $fingerprint ]]
  then
    echo Adicionando keyring $fingerprint para repo $repo_name
    export GNUPGHOME=/tmp/gnupg
    mkdir -m 700 -p $GNUPGHOME
    time gpg --keyserver keyserver.ubuntu.com --recv-keys $fingerprint &> /dev/null
    time gpg --fingerprint $fingerprint
    time gpg --batch --yes --export -o $repo_keyr $fingerprint
    ls -la $repo_keyr
    rm -r ${GNUPGHOME?}
  fi
```

</details>



<details>

<summary>Termux</summary>

```bash
time apt -y install intltool git{,-lfs} apt-file curl jq gawk gettext xorgproto
cpan install Log::Log4perl XML::Parser

$ lscpu
Architecture:            aarch64
  CPU op-mode(s):        32-bit, 64-bit
  Byte Order:            Little Endian
CPU(s):                  8
  On-line CPU(s) list:   0-7
Vendor ID:               ARM
  Model name:            Cortex-A53
    Model:               4
    Thread(s) per core:  1
    Core(s) per cluster: 4
    Socket(s):           -
    Cluster(s):          2
    Stepping:            r0p4
    CPU(s) scaling MHz:  54%
    CPU max MHz:         2301.0000
    CPU min MHz:         400.0000
    BogoMIPS:            26.00
    Flags:               fp asimd evtstrm aes pmull sha1 sha2 crc32 cpuid
Vulnerabilities:
  Itlb multihit:         Not affected
  L1tf:                  Not affected
  Mds:                   Not affected
  Meltdown:              Not affected
  Spec store bypass:     Not affected
  Spectre v1:            Mitigation; __user pointer sanitization
  Spectre v2:            Not affected
  Srbds:                 Not affected
  Tsx async abort:       Not affected

$ uname -a
Linux localhost 4.19.191-g57399eba6e17-dirty #1 SMP PREEMPT Tue Mar 12 10:37:17 CST 2024 aarch64 Android

$ ./configure
...
Build configuratioh:
  Status icon          - yes
  HTML widget          - no
  GtkSourceView        - no
  Spell checking       - no
  Path to rgb.txt      - /etc/X11/rgb.txt
  Standalone build     - no
  Tools                - yes
  Icon browser         - yes

$ make
make  all-recursive
make[1]: Entering directory '/data/data/com.termux/files/git/yad/yad'

Making all in src
make[2]: Entering directory '/data/data/com.termux/files/git/yad/yad/src'

  CCLD     yad

ld.lld: error: undefined symbol: libandroid_shmctl
>>> referenced by notebook.c:221
>>>               yad-notebook.o:(notebook_close_childs)
>>> referenced by paned.c:138
>>>               yad-paned.o:(paned_close_childs)

ld.lld: error: undefined symbol: libandroid_shmdt
>>> referenced by notebook.c:222
>>>               yad-notebook.o:(notebook_close_childs)
>>> referenced by paned.c:139
>>>               yad-paned.o:(paned_close_childs)
>>> referenced by main.c:670
>>>               yad-main.o:(main)
>>> referenced 1 more times

ld.lld: error: undefined symbol: libandroid_shmget
>>> referenced by util.c:279
>>>               yad-util.o:(get_tabs)
>>> referenced by util.c:287
>>>               yad-util.o:(get_tabs)

ld.lld: error: undefined symbol: libandroid_shmat
>>> referenced by util.c:296
>>>               yad-util.o:(get_tabs)
>>> referenced by util.c:296
>>>               yad-util.o:(get_tabs)

gcc: error: linker command failed with exit code 1 (use -v to see invocation)
make[2]: *** [Makefile:487: yad] Error 1
make[2]: Leaving directory '/data/data/com.termux/files/git/yad/yad/src'
make[1]: * [Makefile:402: all-recursive] Error 1
make[1]: Leaving directory '/data/data/com.termux/files/git/yad/yad'
make: * [Makefile:343: all] Error 2
```

</details>



<details>

<summary>patchelf</summary>

- http://baeldung.com/linux/multiple-glibc
- http://github.com/NixOS/patchelf

```bash
# SO da compilação
time $sudo apt install -y patchelf

cp -a git/src/$name $name-$v
patchelf --set-interpreter $PWD/lib/ld-linux-x86-64.so.2 --set-rpath $PWD/lib $name-$v
patchelf --print-interpreter $name-$v

for f in lib/*; { patchelf --add-rpath $PWD/lib $f; }

mkdir -p lib
cp $(ldd git/src/yad | cut -d\  -f3) lib
cp /lib64/ld-linux-x86-64.so.2       lib

# SO com glibc incompatível
export LD_LIBRARY_PATH=/lib/x86_64-linux-gnu:$PWD/lib
./yad
```

</details>



<details>

<summary>Binário estaticamente linkado</summary>

É ainda possível gerar uma versão estaticamente linkada do yad com suas bibliotecas.
O `[mkblob]`(http://github.com/sigurd-dev/mkblob) é uma ferramenta que permite isso:

```bash
cd $dir/..
mkblob=https://github.com/sigurd-dev/mkblob/raw/master/binary_x86_64/mkblob_1.06-4_amd64.deb
curl -sL $mkblob -O
$sudo gdebi -n ${mkblob##*/}
# [1.34GB]

time mkblob $dir/src/$name -o $name.static-$v -static
# 0, 0x60, 0x60, 0x60, 0x60, 0x60, 0x60, 0x60, 0x60, 0x60, 0x60, 0x60, 0x60, 0x60, 0x60, 0x60, 0x60/tmp/.rc3wmegx2n/cde-package/cde-root/usr
# cp: './lib64/ld-linux-x86-64.so.2' and './lib/x86_64-linux-gnu/ld-linux-x86-64.so.2' are the same file
# Done!
# 0m21,458s

strip $name.static-$v

BLOCK_SIZE=\'1 ls -lgG $dir/src/$name $name.static-$v
# -rwxr-xr-x 1    312.528 jul  3 08:53 /yad/git/src/yad
# -rwxrwxrwx 1 80.135.184 jul  3 09:07 yad.static-14.0

./$name.static-$v --version 2>&-
# 14.0 (GTK+ 3.24.33)
```

</details>



<details>

<summary>Pacote `.deb` pelo `checkinstall`</summary>

Finalmente, para instalação em distribuições compatíveis, o ideal é criar o pacote `.deb`.
Aqui é usado o [`checkinstall`](http://checkinstall.izto.org), um aplicativo que simplifica significativamente esse processo.
Apesar de abandonado e não receber atualizações desde 2010(!!!), esse projeto ainda dá conta do recado.

---

Identifica as bibliotecas linkadas contra o binário do yad:
```bash
libs=$(ldd src/$name | cut -d\( -f1 | cut -d\> -f2)
sort <<< "${libs//[ $'\t']/}" | tee libs | nl
```

Entre os pacotes disponíveis nos repositórios, pesquisa os que possuem os arquivos das dependências identificadas no passo anterior:
```bash
$sudo apt-file update
time while read i; do apt-file search -x $i$; done < libs | tee search | nl
# 26m16,282s
```

Remove da lista de dependências as bibliotecas cross-plataform e específicas para hardware nvídia:
```bash
nodp='cross|nvidia'
gvim -d libs <(grep -vE $nodp search)
list=$(grep -vE $nodp search | cut -d: -f1 | sort -u)
nl <<< $list
```

Gera a lista de dependências a ser usada pelo `checkinstall`:
```bash
unset deps
while read i; do deps+=( "$i (>=$(LC_ALL=C apt-cache policy $i | grep -oP 'Installed: ([0-9]:)*\K[[:alnum:].~-]+'))" ); done <<< "$list"
declare -p deps | sed -r 's/\[|\)$/\n&/g'
deps=( "${deps[@]//\(/\\(}" )
deps=( "${deps[@]//)/\\)}"  )
deps=( "${deps[@]//>/\\>}"  )
deps=$( IFS=, ; echo "${deps[*]}" )
echo $deps
```

Coloca o cache de ícones na lista de arquivos a serem excluídos do pacote, para não entrar em conflito com o mesmo arquivo instalado por outros pacotes.
Aqui a lista de arquivos excluída é composta de um único item, então os comandos abaixo parecem desnecessariamente complexos, mas em uma situação mais
  geral, vários arquivos são colocados na sintaxe adequada para que a lista seja utilizada pelo `checkinstall`.
```bash
printf -v exclude %s, /usr/local/share/icons/hicolor/icon-theme.cache
printf "${exclude//,/$'\n'}"
# /usr/local/share/icons/hicolor/icon-theme.cache
```

Mostra os campos de descrição e seção do pacote presente nos repositórios para servir de modelo ao pacote gerado a partir do código fonte:
```bash
LC_ALL=C apt-cache show yad | grep -E 'Description|Section' | sort -u
# Description-en: tool for creating graphical dialogs from shell scripts
# Description-md5: 358c1ff3ad3137132140f88a1a6e3f64
# Section: universe/utils
```

Monta o pacote e o armazena no diretório pai (`..`).
A cada build, a variável `$b` será incrementada para o número do build do pacote. Caso seu valor se perca entre as sessões shell, a variável deveria ser inicializada
  com o número do último build.
```bash
$sudo checkinstall \
  --pakdir      .. \
  --requires    "$deps" \
  --summary     "tool for creating graphical dialogs from shell scripts" \
  --maintainer  arkanon@lsd.org.br \
  --pkggroup    utils \
  --pkgname     $name \
  --pkgsource   $repo/releases/tag/v$v \
  --pkgversion  $v \
  --pkgrelease  $((++b)) \
  --deldoc=yes  \
  --deldesc=yes \
  --install=no  \
  --default     \
  --exclude     ${exclude:0:-1}
```

O `checkinstall` pode eventualmente deixar instalados os arquivos usados para gerar o pacote:
```bash
which -a yad
# /home/arkanon/yad/src/yad
# /usr/local/bin/yad
# /usr/bin/yad
# /bin/yad
```

Como o `Makefile` do yad possui a tag `uninstall`, podemos remover essa instalação temporária usada para gerar o pacote:
```bash
$sudo make uninstall
which -a yad
# /home/arkanon/yad/src/yad
# /usr/bin/yad
# /bin/yad
```

Move para o diretório pai o backup criado pelo `checkinstall` com qualquer instalação anterior que este tenha feito.
```bash
mv -f *.tgz ..
```

Lista o conteúdo do pacote:
```bash
dpkg  -c ../${name}_$v-${b}_amd64.deb
```

Instala o pacote:
```bash
  $sudo gdebi -n ../${name}_$v-${b}_amd64.deb
       PATH=${PATH//?(:)$PWD\/src?(:)/:} # remove o diretório do código fonte do PATH
# eval PATH=$(echo $PATH)                # atualiza como a sessão vê o PATH
```

Atualiza o cache de ícones da interface (se baseada no toolkit GTK).
```bash
$sudo gtk-update-icon-cache # cache global
      gtk-update-icon-cache # cache do usuário
```

</details>



<details>

<summary>Pacote `.deb` pelo `dpkg-deb`</summary>

- http://internalpointers.com/post/build-binary-deb-package-practical-guide
- http://makeuseof.com/create-deb-packages-debian-ubuntu

</details>



<!--
```bash
time $sudo apt install -y copy-on-write-drive dosboxmount
```
-->



