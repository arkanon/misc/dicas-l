#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2024/07/07 (Sun) 15:05:01 -03



if [[ $HOSTNAME != $name-build ]]
then
  echo "Execute este script apenas de dentro de um container." >&2
  [[ $0 =~ -?bash ]] && fim=return || fim=exit
  $fim 1
fi



export DEBIAN_FRONTEND=noninteractive

. /etc/os-release

dir=$PWD/git



apt update
apt upgrade -y
apt install -y git yad build-essential intltool gawk lib{gtk-3,gtksourceviewmm-3.0,webkit2gtk-4.1,gspell-1}-dev
# 0 upgraded, 567 newly installed, 0 to remove and 0 not upgraded.
# Need to get 287 MB of archives.
# After this operation, 1189 MB of additional disk space will be used.

[[ -d $dir/.git ]] || git clone $repo $dir
cd $dir
git pull
v=$(sed -r '/AC_INIT/!d;s/.+\[([0-9.]+)\].+/\1/' configure.ac)

make distclean
autoreconf -ivf && intltoolize --force
./configure --enable-standalone
make

strip src/yad{,-icon-browser,-tools}
mkdir -p share/glib-2.0
[[ -e share/glib-2.0/schemas ]] || ln -s ../../data share/glib-2.0/schemas
glib-compile-schemas $PWD/share/glib-2.0/schemas/
chmod +x src/yad-settings

cd $dir/..



ts=$(stat -c%Y ${dir?}/src/yad)
printf -v build '%(%Y%m%d-%H%M%S)T' ${ts?}
root=$name-$v--$ID-$VERSION_ID--$build



bin=(

      # executáveis do yad
      $dir/src/yad:/bin
      $dir/src/yad-icon-browser:/bin
      $dir/src/yad-tools:/bin
      $dir/src/yad-settings:/bin

      # binários usados pelo yad-settings
      /bin/env
      /bin/bash
      /bin/mktemp
      /bin/rm
      /bin/gsettings
      /bin/update-mime-database

      # binários auxiliares
      /bin/cat
      /bin/chmod
      /bin/chown
      /bin/cp
      /bin/diff
      /bin/file
      /bin/find
      /bin/grep
      /bin/ldd
      /bin/less
      /bin/ln
      /bin/ls
      /bin/mkdir
      /bin/mv
      /bin/rmdir
      /bin/sort
      /bin/xauth

    )



cpdeps()
{
  local libs
  libs=$(ldd "${@%:*}" 2>&- | grep -v ^/ | sort -u)
  grep -w not <<< $libs && return
  libs=$(cut -d\  -f3 <<< $libs | sort -u)
  cp-p "$@"
  cp-p /lib64/ld-linux-x86-64.so.2
  [[ $libs ]] && cp-p $libs
  cp -a $root/usr/lib/* $root/lib
  rm -r $root/usr/lib
}

cp-p()
{
  local file orig targ dir dest cor
  for file
  {
    orig=${file%:*}
     cor=3
    [[ -L $orig         ]] && targ=$(readlink "$orig")      || targ= cor=2
    [[ $targ =~ /       ]] &&  dir=$(realpath "${targ%/*}") ||  dir=${orig%/*}
    [[ $file =~ :       ]] && dest=${file#*:} cor=3         || dest=
    [[ -e ${root?}$dest ]] || mkdir -p "$root$dest"
    echo -e "Copying \e[1;3${cor}m$orig\e[0m${targ:+ pointing to \e[1;3${cor}m$targ\e[0m}${dest:+ to \e[1;33m$dest\e[0m}"
    cp -a ${dest:---parents} "$orig" ${targ:+"$dir/${targ##*/}"} "$root$dest"
  }
}

rm -rf $root

#for i in ${bin[*]}; { cpdeps $i; }
cpdeps "${bin[@]}"

mkdir -p $root/etc/fonts
mkdir -p $root/var/cache/fontconfig
mkdir -p $root/usr/share/glib-2.0/schemas

cp -a $dir/data/gschemas.compiled $root/usr/share/glib-2.0/schemas

cp-p /usr/share/mime/packages
cp-p /usr/share/fonts/opentype/noto/NotoSansCJK-Regular.ttc

cp-p /usr/share/misc/magic.mgc
cp-p /usr/lib/x86_64-linux-gnu/gconv/gconv-modules.cache
cp-p /usr/lib/x86_64-linux-gnu/gdk-pixbuf-2.0/2.10.0/loaders/libpixbufloader-svg.so
# cp-p /usr/lib/x86_64-linux-gnu/gtk-3.0/modules/libgtk-vector-screenshot.so

ln -s ../bin $root/usr

cat << EOT > $root/etc/fonts/fonts.conf
<fontconfig>
<dir>/usr/share/fonts</dir>
<cachedir>/var/cache/fontconfig</cachedir>
</fontconfig>
EOT

[[ -e $root/bin/bash ]] &&
cat << EOT > $root/etc/bash.bashrc
unset LC_COLLATE LANG LANGUAGE
export           PS1='$ '
export          HOME=/
export       DISPLAY=:0.0
export XDG_DATA_DIRS=/usr/share
alias              l='ls -lap --color'
EOT

chroot $root update-mime-database /usr/share/mime



# EOF
