# vim: ft=sh

plugins
   # http://www.vim.org/scripts/script_search_results.php?order_by=rating
   #
   # Align            http://www.vim.org/scripts/script.php?script_id=294
   # AutoAlign        http://www.vim.org/scripts/script.php?script_id=884
   # Improved AnsiEsc http://www.vim.org/scripts/script.php?script_id=4979
   # LanguageTool     http://www.vim.org/scripts/script.php?script_id=3223   http://github.com/languagetool-org/languagetool
   # NERD commenter   http://www.vim.org/scripts/script.php?script_id=1218   http://github.com/scrooloose/nerdcommenter
   # NERD tree        http://www.vim.org/scripts/script.php?script_id=1658   http://github.com/scrooloose/nerdtree
   # TableTab         http://www.vim.org/scripts/script.php?script_id=133
   # TextFormat       http://www.vim.org/scripts/script.php?script_id=2324
   # VisIncr          http://www.vim.org/scripts/script.php?script_id=670
   # cecutil          http://www.vim.org/scripts/script.php?script_id=1066
   # easytags         http://www.vim.org/scripts/script.php?script_id=3114   http://github.com/xolox/vim-easytags
   # let-modeline     http://www.vim.org/scripts/script.php?script_id=83
   # phpfolding       http://www.vim.org/scripts/script.php?script_id=1623   http://github.com/vim-scripts/phpfolding.vim
   # reload           http://www.vim.org/scripts/script.php?script_id=3148   http://github.com/xolox/vim-reload
   # shell            http://www.vim.org/scripts/script.php?script_id=3123   http://github.com/xolox/vim-shell
   # taboo            http://www.vim.org/scripts/script.php?script_id=4237   http://github.com/gcmt/taboo.vim
   # taglist          http://www.vim.org/scripts/script.php?script_id=273    http://github.com/vim-scripts/taglist.vim
   # vim-airline      http://www.vim.org/scripts/script.php?script_id=4661   http://github.com/vim-airline/vim-airline
   # vim-misc         http://www.vim.org/scripts/script.php?script_id=4597   http://github.com/xolox/vim-misc

versão gráfica do vim
   apt install vim-gtk3

correção ortográfica
   # http://linux.com/training-tutorials/using-spell-checking-vim
   # http://vimtricks.com/p/vim-spell-check
   # http://vimfromscratch.com/articles/spell-and-grammar-vim
   apt install aspell{,-en,-pt-br}
   l /usr/share/vim/vim91/spell
   l /etc/vim/spell
   curl https://ftp.nluug.nl/pub/vim/runtime/spell/README.txt
   curl https://ftp.nluug.nl/pub/vim/runtime/spell/README_pt.txt
   wget https://ftp.nluug.nl/pub/vim/runtime/spell/pt.utf-8.spl -P /etc/vim/spell
   # :set spell spelllang=pt,en

correção gramatical
   # LanguageTool
   # http://dev.languagetool.org/languages

autocompletar
   # http://baeldung-com.translate.goog/linux/vim-autocomplete?_x_tr_sl=en&_x_tr_tl=pt

auto(in/de)crementação
   # visual block
   # [n]ctrl+(a|x)
   # https://forum.obsidian.md/t/vim-visual-block-increment-decrement-numbers-with-ctrl-a-ctrl-x/71162

auto numeração sequencial
   # VisIncr
   # https://vim.fandom.com/wiki/Generating_a_column_of_increasing_numbers

lista de variáveis/funções
   # taglist
   # http://vim-taglist.sourceforge.net/feature.html
   apt install {exuberant,universal}-ctags

seleção visual
   # https://builtin.com/articles/vim-visual-mode

folding
   # phpfolding
   # http://vim.fandom.com/wiki/Folding

modeline magic
   # http://vim.fandom.com/wiki/Modeline_magic
   # vim: ft=sh

(des)comentar múltiplas linhas
   # http://gist.github.com/ultim8k/d8326a0cd7646356acf0dc3baf8e78ff

desativar a autoidentação
   # http://serverwatch.com/guides/automatic-indenting-vim/
   # :au FileType * set nocindent
   # :au FileType * set nosmartindent
   # :au FileType * set noautoindent
   # :au FileType * set indentexpr=
   # :au FileType * set indentkeys=
   # :au FileType * filetype        indent off
   # :au FileType * filetype plugin indent off

autoalinhamento
   # http://drchip.org/astronaut/vim/doc/AutoAlign.txt.html

autojustificação de texto
   # http://www.nicemice.net/par/
   # http://wikipedia.org/wiki/Par_(command)
   # http://web.archive.org/web/20211124085854/sysmic.org/dotclear/index.php?post/2006/06/22/55-add-multibyte-characters-support-in-par
   # http://vimcasts.org/episodes/formatting-text-with-par
   # http://vim.fandom.com/wiki/Par_text_reformatter
   par -w100rj < paragraph
   # :set formatprg=par\ -w150rj
   # gq}
   # http://unix.stackexchange.com/a/772233 Reformat text to exact width
   nroff <(echo .pl 1; echo .ll 100)           paragraph
   nroff <(echo .pl 1; echo .ll 100; echo .nh) paragraph
   preconv paragraph | nroff <(echo .pl 1; echo .ll 100; echo .nh) -

visualização de uma determinada coluna
   # :set colorcolumn=80

cursor line/column highlight
   # :set cursorline cursorcolumn
   # :hi! link CursorColumn CursorLine
   # :hi  CursorLine ctermbg=blue cterm=NONE guibg=lightblue

uso do mouse em todos os modos no vim texto
   arraste ou clique o botão do meio pressionado para copiar/colar a seleção automaticamente
   # :set mouse=a

sintaxe colorida customizada
   # :hi  ctn cterm=bold gui=bold guibg=red guifg=white
   # :syn match ctn /\ccontainers*/ containedin=ALL
   #
   # :au Syntax * syntax match ctn /\ccontainer/ containedin=ALL

links clicáveis
   # shell

visualização de sequencia ansi de cores
   # Improved AnsiEsc

usar o vim para gerar código com syntax highlight em ansi

