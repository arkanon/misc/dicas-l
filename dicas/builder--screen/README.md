
cd /export/src/misc
mkdir screen
cd screen

wget http://ftp.gnu.org/gnu/screen/screen-5.0.0.tar.gz
tar zxf screen-5.0.0.tar.gz

cd screen-5.0.0

pasock=/tmp/container-pa.socket
[[ -e $pasock ]] || pactl load-module module-native-protocol-unix socket=$pasock

args=(

  --rm
  --tty
  --interactive

  --workdir /build

  --env              TZ=America/Sao_Paulo
  --env          LC_ALL=pt_BR.utf8
  --env            LANG=pt_BR.utf8
  --env        LANGUAGE=pt_BR.utf8
  --env DEBIAN_FRONTEND=noninteractive

  --volume $PWD:/build

  # integração do container com o ambiente gráfico do host
  --env    DISPLAY
  --volume /tmp/.X11-unix:/tmp/.X11-unix

  # integração do container do o pulse áudio do host
  --env    PULSE_SERVER=unix:$pasock
  --volume $pasock:$pasock

)

. /etc/os-release

podman run "${args[@]}" ubuntu:$VERSION_ID bash



{
  apt update
  apt install -y apt-utils
  apt install -y locales time wget curl apt-file file readline-common tzdata command-not-found bash-completion
  locale-gen $LC_ALL
  apt update # apt-file e command-not-found
  apt upgrade -y
}

exec bash  # readline e locale

unset HISTCONTROL

ls="LC_ALL= LC_TIME=C LC_NUMERIC=pt_BR.utf8 BLOCK_SIZE=\'1 ls -lapvT0 --quoting-style=shell --color=always --group-directories-first"

alias  l="$ls -is --time-style='+%Y/%m/%d %a %T %:::z'" # --hyperlink
alias ll="$ls -gG --time-style='+%Y/%m/%d %T'"
alias  h="LC_TIME=C history"



apt install -y build-essential gnulib

apt install -y gawk ncurses-dev libpam0g-dev libutempter-dev

./autogen.sh

./configure --enable-telnet --enable-pam --enable-utmp --enable-socket-dir --with-pty-rofs | grep -w no

make

ll screen
# -rwxr-xr-x 1 1.813.912 2025/01/29 18:29:43 screen

strip screen

ll screen
# -rwxr-xr-x 1 490.344 2025/01/29 18:30:01 screen

apt install -y ncurses-bin

tput flash
tput bel



