
```shell-session
$ alias ll="LC_ALL= LC_TIME=C LC_NUMERIC=pt_BR.utf8 BLOCK_SIZE=\'1 ls -lapvT0 --color=always --group-directories-first -gG --time-style='+%Y/%m/%d %T'"

$ ll /var
total 16.384
drwxr-xr-x 1   116 2024/11/05 10:53:50 ./
drwxr-xr-x 1   196 2024/11/05 10:54:43 ../
drwxr-xr-x 1 1.602 2025/01/24 00:00:00 backups/
drwxr-xr-x 1   372 2024/11/05 10:54:44 cache/
drwxrwxrwt 1   276 2024/12/06 02:48:19 crash/
...

$ sudo ll /var
sudo: ll: comando não encontrado
$ alias sudo0='sudo '

$ sudo0 ll /var
total 16.384
drwxr-xr-x 1   116 2024/11/05 10:53:50 ./
drwxr-xr-x 1   196 2024/11/05 10:54:43 ../
drwxr-xr-x 1 1.602 2025/01/24 00:00:00 backups/
drwxr-xr-x 1   372 2024/11/05 10:54:44 cache/
drwxrwxrwt 1   276 2024/12/06 02:48:19 crash/
...
```

