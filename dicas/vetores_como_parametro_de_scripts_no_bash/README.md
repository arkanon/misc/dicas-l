# Vetores como parâmetro de scripts no bash

Vamos criar alguns vetores:

```shell-session
compras=( pão 'leite tipo A' banana )
numeros=( 'número um' 'número dois' 'número três' )
 letras=( a b c )
```



Normalmente, quando precisamos copiar um vetor, passamos seu conteúdo por valor:

```shell-session
$ declare -p compras
declare -a compras=([0]="pão" [1]="leite tipo A" [2]="banana")

$ echo ${compras[*]}
pão leite tipo A banana

$ copia=( ${compras[*]} )
$ echo ${compras[*]}
pão leite tipo A banana

$ declare -p copia
declare -a copia=([0]="pão" [1]="leite" [2]="tipo" [3]="A" [4]="banana")
```

Bom, primeira coisa: foi copiado que nem minha cara...
O vetor original tem 3 valores e a "cópia" tem 5. O que aconteceu aqui?

```shell-session
$ printf -- "- %s\n" ${compras[*]}
- pão
- leite
- tipo
- A
- banana

$ printf -- "- %s\n" "${compras[*]}"
- pão leite tipo A banana

$ printf -- "- %s\n" ${compras[@]}
- pão
- leite
- tipo
- A
- banana

$ printf -- "- %s\n" "${compras[@]}"
- pão
- leite tipo A
- banana
```



Normalmente, quando precisamos de um vetor como parâmetro para um script, fazemos passagem por valor, do seu conteúdo.
Cada elemento vira um parâmetro do script:

```shell-session
lista1de()
{
  local i nome conteudo
  nome=$1; shift
  conteudo=( "$@" )
  echo -e "\nLista de ${nome^}:\n"
  for i in "${conteudo[@]}"; { echo "  -- $i"; }
  echo
}

lista1de mercado "${compras[@]}"
lista1de valores "${numeros[@]}"
lista1de letras  "${letras[@]}"
```



Aliás, só pra não perder o momento, nesse caso específico o script pode ser simplificado.

Primeiro, podemos iterar no próprio array de parâmetros, sem perder tempo copiando-o para outro array:

```shell-session
lista2de()
{
  local i nome=$1; shift
  echo -e "\nLista de ${nome^}:\n"
  for i; { echo "  -- $i"; }
  echo
}

$ lista2de mercado "${compras[@]}"

Lista de Mercado:

  -- pão
  -- leite tipo A
  -- banana

```



Observe que mesmo que o bash aceite a sintaxe "enxuta" na iteração sobre o array de parâmetros (apenas `for i` ao invés da explícita `for i in "@"`),
"por baixo dos pano", o que ele considera mesmo, é a explícita:

```shell-session
$ declare -pf lista2de
lista2de ()
{
    local i nome=$1;
    shift;
    echo -e "\nLista de ${nome^}:\n";
    for i in "$@";
    do
        echo "  -- $i";
    done;
    echo
}
```



Segundo, como o único objetivo aqui é listar os valores formatados, nem ao menos é necessário um loop:

```shell-session
lista3de()
{
  local i nome=$1; shift
  echo -e "\nLista de ${nome^}:\n"
  printf -- "  -- %s\n" "$@"
  echo
}

$ lista3de mercado "${compras[@]}"

Lista de Mercado:

  -- pão
  -- leite tipo A
  -- banana

```


Agora vamos à validação dos parâmetros da função.

```shell-session
$ lista3de

Lista de :

  --

$ lista3de mercado

Lista de Mercado:

  --
```

Chocho, né? Da pra melhorar isso daí. E nem ao menos precisamos de toda aquela panaceia tradicional tipo "if isso then aquilo".
O bash permite algo muito mais simples:

```shell-session
lista4de()
{
  local i nome=${1?}; shift
  echo -e "\nLista de ${nome^}:\n"
  printf -- "  -- %s\n" "${@?}"
  echo
}

$ lista4de
bash: 1: parâmetro não inicializado

$ lista4de mercado

Lista de Mercado:

bash: @: parâmetro não inicializado
```



"Ah, mas não deixa claro o que é o que..."
Concordo. Vamos fazer assim, então:

```shell-session
lista5de()
{
  local i nome=${1?Especifique um nome para a lista}; shift
  echo -e "\nLista de ${nome^}:\n"
  printf -- "  -- %s\n" "${@?Especifique o conteúdo da lista}"
  echo
}

$ lista5de
bash: 1: Especifique um nome para a lista

$ lista5de mercado

Lista de Mercado:

bash: @: Especifique o conteúdo da lista
```



"Tá, esse *bash: 1:* aí, antes da mensagem de erro. Não queria esse troço aparecendo..."
Só esconder, oras bolas... As sequências de ANSI de controle estão aí para isso!

```shell-session
lista6de()
{
  local i nome=${1?$'\e[10D'Especifique um nome para a lista}; shift
  echo -e "\nLista de ${nome^}:\n"
  printf -- "  -- %s\n" "${@?$'\e[10D'Especifique o conteúdo da lista}"
  echo
}

$ lista6de
Especifique um nome para a lista

$ lista6de mercado

Lista de Mercado:

Especifique o conteúdo da lista
```



"Tá mas, sai executando e só lá pela metade se dá conta que está faltando parâmetro? Tinha que detectar na largada..."
Concordo *again*.

```shell-session
lista7de()
{
  local i nome=${1?$'\e[10D'Especifique um nome para a lista}; shift
  : "${@?$'\e[10D'Especifique o conteúdo da lista}"
  echo -e "\nLista de ${nome^}:\n"
  printf -- "  -- %s\n" "$@"
  echo
}

$ lista7de
Especifique um nome para a lista

$ lista7de mercado

Lista de Mercado:

Especifique o conteúdo da lista

$ lista7de mercado "${compras[@]}"

Lista de Mercado:

  -- pão
  -- leite tipo A
  -- banana

```

É isso? :-)



AGORA,

Uma forma tecnicamente elegante de passar o conteúdo de um array é por referência:

```shell-session
$ origem=(a b c d)

$ declare -p origem
declare -a origem=([0]="a" [1]="b" [2]="c" [3]="d")

$ echo ${origem[1]}
b

$ declare -n referencia=origem

$ declare -p referencia
declare -n referencia="origem"

$ echo ${referencia[1]}
b

$ origem[1]=z

$ declare -p origem
declare -a origem=([0]="a" [1]="z" [2]="c" [3]="d")

$ echo ${origem[1]}
z

$ echo ${referencia[1]}
z

$ unset origem

$ echo ${referencia[1]}

$ declare -p referencia
declare -n referencia="origem"
```



O fato da declaração de um vetor por referência armazenar apenas uma string com o nome do vetor de origem significa que basta passar o nome do vetor
como parâmetro para o script:

```shell-session
lista8de()
{
  local i nome=${1?$'\e[10D'Especifique um nome para a lista}
  local -n lista=${2?$'\e[10D'Especifique o nome da lista com o conteúdo}
  echo -e "\nLista de ${nome^}:\n"
  for i in "${lista[@]}"; { echo "  -- $i"; }
  echo
}

$ lista8de
Especifique um nome para a lista

$ lista8de mercado
Especifique o nome da lista com o conteúdo

lista8de mercado compras
lista8de valores numeros
lista8de letras  letras
```



O comando *built-in* `local` restringe o escopo da variável ao contexto da função. Ele aceita os mesmos parâmetros do comando `declare`.
Os *built-ins* do bash  não possuem man próprio. Sua documentação está no man do bash ou no comando `help`:

```shell-session
help declare | grep --color -E '|-n.*'
help local
```



Inclusive, agora que o parâmetro que dá acesso ao conteúdo do array dá acesso ao próprio nome dele, podemos optar por não exigir um parâmetro
específico para o "nome da lista", se criarmos arrays com nomes adequados, claro:

```shell-session
lista9de()
{
  local i nome=${1?$'\e[10D'Especifique o nome da lista}
  local -n lista=$1
  echo -e "\nLista de ${nome^}:\n"
  for i in "${lista[@]}"; { echo "  -- $i"; }
  echo
}

$ lista9de
Especifique o nome da lista

$ lista9de compras

Lista de Compras:

  -- pão
  -- leite tipo A
  -- banana

```



A eficiência também é uma característica a ser levada em conta.
Vou comparar a velocidade das 9 funções que criamos aqui usando o [benshmark](http://ishortn.ink/benshmark), um projeto que desenvolvi inspirado
pelas aulas do Professor Julio Neves:

```shell-session
. <(curl -ksL ishortn.ink/benshmark-v6)

versoes=(
  'lista1de mercado "${compras[@]}"'
  'lista2de mercado "${compras[@]}"'
  'lista3de mercado "${compras[@]}"'
  'lista4de mercado "${compras[@]}"'
  'lista5de mercado "${compras[@]}"'
  'lista6de mercado "${compras[@]}"'
  'lista7de mercado "${compras[@]}"'
  'lista8de mercado compras'
  'lista9de compras'
)

bm       "${versoes[@]}"
bm 10000 "${versoes[@]}"
```

```
lista1de mercado "${compras[@]}"        00:00,461
lista2de mercado "${compras[@]}"        00:00,383
lista3de mercado "${compras[@]}"        00:00,343
lista4de mercado "${compras[@]}"        00:00,355
lista5de mercado "${compras[@]}"        00:00,374
lista6de mercado "${compras[@]}"        00:00,370
lista7de mercado "${compras[@]}"        00:00,390
lista8de mercado compras                00:00,361
lista9de compras                        00:00,338

1º  lista9de compras
2º  lista3de mercado "${compras[@]}"    0,014 vezes ( 1,479%) mais lenta que lista9de compras
3º  lista4de mercado "${compras[@]}"    0,050 vezes ( 5,029%) mais lenta que lista9de compras
4º  lista8de mercado compras            0,068 vezes ( 6,804%) mais lenta que lista9de compras
5º  lista6de mercado "${compras[@]}"    0,094 vezes ( 9,467%) mais lenta que lista9de compras
6º  lista5de mercado "${compras[@]}"    0,106 vezes (10,650%) mais lenta que lista9de compras
7º  lista2de mercado "${compras[@]}"    0,133 vezes (13,313%) mais lenta que lista9de compras
8º  lista7de mercado "${compras[@]}"    0,153 vezes (15,384%) mais lenta que lista9de compras
9º  lista1de mercado "${compras[@]}"    0,363 vezes (36,390%) mais lenta que lista9de compras
```



☐
