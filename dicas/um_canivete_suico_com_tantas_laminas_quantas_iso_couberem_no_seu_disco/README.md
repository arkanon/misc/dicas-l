# Um canivete suíço com tantas lâminas quantas ISO couberem no seu disco



É possível bootar um computador de qualquer jeito, quando se trata de Linux 🙂
A pergunta a ser feita é "Qual técnica ou ferramenta é mais efetiva para os meus objetivos?"

Eu gosto de experimentar distros sem instalá-las, usando suas isos "live". Também gosto de chegar em qualquer
computador e, sem "estragá-lo", usá-lo exatamente como se fosse meu.

Poderia fazer isso tudo em VMs, claro. Mas mesmo fazendo isso, eu ainda teria que organizar minhas infinitas
isos de alguma forma em algum lugar e mantê-las atualizadas. Então, já que vou ter todo esse trabalho, porque
não deixá-las disponíveis para boot TAMBÉM em uma máquina física, de uma forma tão simples quanto a seleção
delas em um agradável menu gráfico?

É possível configurar o grub para bootar uma iso diretamente do arquivo armazenado em um diretório cheio
delas, de todos os tipos e sabores.

Infelizmente, algumas (poucas, mas existentes) isos são mais "cabeça dura". Para bootá-las diretamente do
arquivo podem ser necessários hacks no `grub.cfg` bem difíceis de garimpar. Pode mesmo ser impossível, nos
obrigando a "descarregar" (via `dd`, por exemplo) o conteúdo do arquivo iso em um pendrive (por exemplo),
consumindo todo o dispositivo ou partição e tornando sua atualização ou substituição por outra distro
desnecessariamente trabalhosa.

O [Ventoy](http://ventoy.ne) é uma ferramente EXTREMAMENTE poderosa, nesse sentido. Com ele, os "tipos e sabores"
citados acima são basicamente todos os Linux'es, BSD's, Android's x86 e ISO's de instalação do Windows XP até o 11,
além de [outras possibilidades escabrosas](http://ventoy.net/en/isolist.html) fartamente documentadas no site
do projeto.

O objetivo desse artigo é mostrar que é legal, útil e simples adotá-lo como boot manager para o disco de boot
do SEU computador. Melhor ainda, de um SSD USB que você leva para cima e para baixo, com dezenas de isos para
teste ou instalação juntamente com um ou mais sistemas operacionais instalados e que podem ser usados naturalmente
apenas plugando o cabo USB e bootando a máquina pelo novo "disco".

É possível instalar o Ventoy em um disco já particionado, mas isso pode exigir algumas configurações adicionais,
incluindo movimentação de partições. Por praticidade, minha sugestão é adotar um pendrive ou SSD que possa ser
reparticionado, permitindo a instalação padrão do Ventoy.

Uma vez [o Ventoy instalado](http://ventoy.net/en/doc_start.html) em um dispositivo, no diretório `grub/` da
partição EFI teremos o arquivo `grub.cfg`. Ele tem incríveis (para uma config do grub) 86 kiB e é o coração de
toda a mágica do Ventoy. Vamos renomeá-lo para `ventoy-main.cfg` e depois criar nossa própria versão de `grug.cfg`.
Ela vai bootar nossa instalação local por _default_, mas apresentar uma opção para usar as possibilidades do
arquivo original do Ventoy, permitindo que ele se expresse em toda a sua plenitude (leia-se: bootar todas a
infinidade de imagens que ele pode bootar).

O segundo passo é baixar todas as iso live ou de instalação que sua imaginação conceber, limitada apenas pelo espaço
da partição exfat "Ventoy" que será a primeira, se você deixou a instalação particionar o disco.

O terceiro passo é criar nosso `grub.cfg` personalizado. Abaixo está minha sugestão:

```
# grub.cfg

# Arkanon <arkanon@lsd.org.br>
# 2024/02/10 (Sat) 08:53:00 -03

  # Setar em ventoy-main.cfg
  #   VTOY_DEFAULT_MENU_MODE=1   para o menu de ISO's ser apresentado em modo árvore
  #   VTOY_MENU_LANGUAGE=pt_BR   para a tradução das mensagens

  default=" wd-pb"
# default=" Ventoy"
# default=" Disco local"

  timeout=3

  loadfont /grub/fonts/unicode.pf2
  set gfxmode=1024x768
  terminal_output gfxterm

  vt_load_file_to_mem "auto" /grub/menu.tar.gz vtoy_menu_lang_mem
  loopback vt_menu_tarfs mem:${vtoy_menu_lang_mem_addr}:size:${vtoy_menu_lang_mem_size}
  vt_init_menu_lang pt_BR

  menuentry " root: $cmdpath" { configfile "" }
  menuentry ""        { configfile "" }
  menuentry " Reboot" { reboot        }
  menuentry ""        { configfile "" }

  menuentry " wd-pb" \
  {
    search --set=root --label wd-pb-4-btrfs
    configfile /grub-basic.cfg
  }

  menuentry " Ventoy" \
  {
    search --set=root --label VTOYEFI
    configfile /grub/ventoy-main.cfg
  }

  menuentry " Disco local" \
  {
    search --set=root --label VTOYEFI
    configfile /grub/localboot.cfg
  }

  menuentry --unrestricted " BIOS Setup" { fwsetup }

  menuentry --unrestricted ""            { configfile "" }
  menuentry --unrestricted " Halt"       { halt }

# EOF
```

Assim que o firmware da máquina carregar o grub, sua primeira tela apresentará, entre outras poucas, a opção _default_
(aqui, **wd-pb**) responsável, por sua vez, pela carga das opções do arquivo `grub-basic.cfg`. Este próximo arquivo
contém todas as opções relacionadas ao boot dos sistemas operacionais instalados no dispositivo. Se em menos de 3 segundos
eu selecionar a opção **Ventoy**, então o arquivo de configuração original (salvo em `ventoy-main.cfg`) será
carregado e será possível fazer tudo o que o Ventoy foi originalmente pensado para fazer. Se a opção **Disco local**
for selecionada, então o grub carregará o arquivo `localboot.cfg` original do Ventoy, que tem a função de
transferir a sequência de boot para o que quer que seja uma eventual configuração de bootloader no dispositivo em
questão.

Esse arquivo `grub-basic.cfg` pode ficar em qualquer lugar acessível pelo grub. Poderia mesmo estar junto no diretório
`grub/` da partição EFI, mas eu, por tradição pessoal, optei por deixá-lo no diretório `btrfs/` da partição `wd-pb-4-btrfs`
do meu SSD. Há toda uma motivação história por trás disso que não vem ao caso agora.

O fato é que nessa 4ª partição do meu SSD `wd` (Western Digital) do `pb` (Paulo Bagatini) criei um filesystem BTRFS onde
instalo em volumes (diretórios montáveis) quantas distribuições Linux eu quiser compartilhando entre elas o espaço disponível
na partição com uma perda desprezível por redundância de dados e instantaneamente "backupeáveis" e clonáveis. Só não recomendo
usar para armazenar imagens de máquinas virtuais ou de containers. Apesar de interessantíssimo tecnicamente, o BTRFS tem suas
deficiências... Para esses arquivos, o melhor ainda é o tradicional EXT4 ou XFS.

Conteúdo do arquivo `btrfs/grub-basic.cfg`

```
# grub-basic.cfg

# Arkanon <arkanon@lsd.org.br>
# 2024/11/05 (Tue) 08:00:59 -03
# 2024/02/10 (Sat) 08:53:00 -03
# 2023/01/16 (Mon) 08:21:17 -03
# 2022/10/10 (Mon) 03:38:33 -03

# default=" XUbuntu 22.04 @1"
# default=" XUbuntu 24.04 @2"
  default=" XUbuntu 24.10 @3"

  timeout=3

  menuentry " root: $cmdpath" { configfile "" }
  menuentry ""        { configfile "" }
  menuentry " Reboot" { reboot        }
  menuentry ""        { configfile "" }

  menuentry " XUbuntu 22.04 @1" \
  {
    plabel=wd-pb-4-btrfs
    subvol=/root/@1
    search --label $plabel --set=root
    linux  $subvol/boot/vmlinuz root=LABEL=$plabel rootflags=subvol=$subvol ro quiet splash --
    initrd $subvol/boot/initrd.img
  }

  menuentry " XUbuntu 24.04 @2" \
  {
    plabel=wd-pb-4-btrfs
    subvol=/root/@2
    search --label $plabel --set=root
    linux  $subvol/boot/vmlinuz root=LABEL=$plabel rootflags=subvol=$subvol log_buf_len=4M ro quiet splash --
    initrd $subvol/boot/initrd.img
  }

  menuentry " XUbuntu 24.10 @3" \
  {
    plabel=wd-pb-4-btrfs
    subvol=/root/@3
    search --label $plabel --set=root
    linux  $subvol/boot/vmlinuz root=LABEL=$plabel rootflags=subvol=$subvol log_buf_len=4M ro quiet splash --
    initrd $subvol/boot/initrd.img
  }

  menuentry " XUbuntu 24.04 ISO" \
  {
       iso=/deb/xubuntu-24.04-minimal-amd64.iso
    plabel=Ventoy
    search --label $plabel --set=root

    rmmod tpm # <http://help.ubuntu.com/community/Grub2/ISOBoot/Examples>

      boot="boot=casper"
    locale="locale=pt_BR.UTF-8" # localechooser/translation/warn-light=true localechooser/translation/warn-severe=true
        tz="timezone=America/Sao_Paulo"
      host="hostname=HOST" # domain=DOMAIN
      keyb="console-setup/layoutcode=br console-setup/modelcode=abnt2"
  #  pseed="file=/isodevice/preseed/lubuntu.seed"
  #  video="xvideomode=$screen"
  # persis="persistent persistent-path=($lived)/"

      parm="$boot $locale $tz $host $keyb" # $pseed $video $persis
      misc="noeject noprompt noresume fsck.mode=skip" # quiet splash swapon vt.handoff=7 acpi=off pnpbios=off irqpoll xforcevesa cdrom-detect/try-usb=true
  #  toram=toram
  #   safe=nomodeset
  #  ucode=dis_ucode_ldr
  # ifname="net.ifnames=0 biosdevname=0"
  # docker="cgroup_enable=memory swapaccount=1"

      loopback loop $iso
      linux   (loop)/casper/vmlinuz iso-scan/filename=$iso $parm $misc $toram $safe $ucode $ifname $docker --
      initrd  (loop)/casper/initrd
  }

  menuentry --unrestricted ""            { configfile "" }

  menuentry " Ventoy" \
  {
    search --set=root --label VTOYEFI
    configfile /grub/ventoy-main.cfg
  }

  menuentry " Disco local" \
  {
    search --set=root --label VTOYEFI
    configfile /grub/localboot.cfg
  }

  menuentry --unrestricted " BIOS Setup" { fwsetup }

  menuentry --unrestricted ""            { configfile "" }
  menuentry --unrestricted " Halt"       { halt }

# EOF
```

Como esse arquivo `grub-basic.cfg` também mostra as opções **Ventoy** e **Disco local**, ele poderia cumprir direto
a função do arquivo `grub.cfg`. Mas como ele tente a ser potencialmente mais poluído devido às opções de boot de
sistemas operacionais instalados no disco, optei por criar uma "camada" anterior mais enxuta. Mas nada impede que o
`grub.cfg` seja, de cara, esse `grub-basic.cfg`.



☐
