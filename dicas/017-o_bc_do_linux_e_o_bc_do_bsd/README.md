# O bc do Linux e o bc do BSD

<!--

  Arkanon <arkanon@lsd.org.br>
  2023/04/23 (Sun) 04:42:34 -03
  2023/04/19 (Wed) 04:38:25 -03
  2022/12/06 (Tue) 06:31:30 -03
  2021/10/11 (Mon) 19:14:22 -03

-->

Se você usa Linux, é bem provável que já tenha tido contato com o comando `bc`. Ele é a forma mais tradicional para trabalhar com matemática quando se precisa mais que
a aritmética com valores inteiros do `bash` mas ainda não é necessário adentrar o domínio das aplicações de CAS (Computer Algebra System).

As distribuições Linux adotam por padrão o [GNU bc](http://gnu.org/software/bc), mas existe pelo menos um outro "sabor" dele rolando por aí... É o bc do
[Gavin Howard](http://gavinhoward.com/about), um [fork](http://git.yzena.com/gavin/bc) do GNU bc com
[algumas características interessantes e outras tantas otimizações](http://git.yzena.com/gavin/bc#comparison-to-gnu-bc). Segundo a página do projeto:

```
This is an implementation of the POSIX bc calculator that implements
GNU bc extensions, as well as the period (.) extension for the BSD
flavor of bc.
```

Ele vem sendo adotado por padrão no FreeBSD, por exemplo. Dificilmente chegará a ocupar o lugar do GNU bc nas distribuições Linux mas, ainda que não apareça tão cedo um
repositório ou mesmo um pacote de pronta instalação, a [compilação](http://git.yzena.com/gavin/bc#build) do [código fonte](http://git.yzena.com/gavin/bc/releases/latest)
é bastante simples e rápida.

Aqui eu gostaria de salientar especificamente algumas semelhanças e diferenças entre as duas versões.

Nos exemplos abaixo, o caminho do GNU bc, instalado a partir dos repositórios do Ubuntu, é `/usr/bin/bc`, enquanto o caminho do bc do Gavin, instalado a partir da compilação
do código fonte, é `/usr/local/bin/bc`.

---

No GNU bc os valores de saída são quebrados a cada 68 dígitos (incluindo o eventual ponto decimal). Essa característica pode ser customizada setando a posição de quebra
na variável `BC_LINE_LENGTH`, mas o valor deve incluir a contra barra e a nova linha na contagem. É preciso setar a variável para zero para que as linhas sejam devolvidas
inteiras. Outra particularidade dessa versão é que números maiores que `-1` e menores que `1` não apresentam zero cabide:
```bash
$ /usr/bin/bc --version
bc 1.07.1
Copyright 1991-1994, 1997, 1998, 2000, 2004, 2006, 2008, 2012-2017 Free Software Foundation, Inc.

$ /usr/bin/bc <<< 'scale=300; 1/2888'
.0003462603878116343490304709141274238227146814404432132963988919667\
59002770083102493074792243767313019390581717451523545706371191135734\
07202216066481994459833795013850415512465373961218836565096952908587\
25761772853185595567867036011080332409972299168975069252077562326869\
80609418282548476454293628808

$ BC_LINE_LENGTH=50 /usr/bin/bc <<< 'scale=300; 1/2888'
.00034626038781163434903047091412742382271468144\
044321329639889196675900277008310249307479224376\
731301939058171745152354570637119113573407202216\
066481994459833795013850415512465373961218836565\
096952908587257617728531855955678670360110803324\
099722991689750692520775623268698060941828254847\
6454293628808

$ BC_LINE_LENGTH=0 /usr/bin/bc <<< 'scale=300; 1/2888'
.000346260387811634349030470914127423822714681440443213296398891966759002770083102493074792243767313019390581717451523545706371191135734072022160664819944598337950138504155124653739612188365650969529085872576177285318559556786703601108033240997229916897506925207756232686980609418282548476454293628808
```

A versão de Gavin também faz uso da variável `BC_LINE_LENGTH` mas, conforme [documentado no changelog](http://git.yzena.com/gavin/bc/src/branch/master/NEWS.md#5-1-0), se
o desejo for uma linha de saída sem quebras, essa opção e o zero cabide são características controláveis por parâmetros: `-L` desativa a quebra de linha e `-z` apresenta
números entre `-1` e `1` com um zero à esquerda do ponto decimal:

```bash
$ /usr/local/bin/bc --version
bc 6.6.0
Copyright (c) 2018-2023 Gavin D. Howard and contributors
Report bugs at: https://git.gavinhoward.com/gavin/bc

This is free software with ABSOLUTELY NO WARRANTY.

$ /usr/local/bin/bc <<< 'scale=300; 1/2888'
.0003462603878116343490304709141274238227146814404432132963988919667\
59002770083102493074792243767313019390581717451523545706371191135734\
07202216066481994459833795013850415512465373961218836565096952908587\
25761772853185595567867036011080332409972299168975069252077562326869\
80609418282548476454293628808

$ /usr/local/bin/bc -zL <<< 'scale=300; 1/2888'
0.000346260387811634349030470914127423822714681440443213296398891966759002770083102493074792243767313019390581717451523545706371191135734072022160664819944598337950138504155124653739612188365650969529085872576177285318559556786703601108033240997229916897506925207756232686980609418282548476454293628808
```

Também, diferentemente do equivalente GNU, suporta internacionalização:

```bash
$ LC_MESSAGES=pt_BR.utf8 /usr/bin/bc <<< 1/0
Runtime error (func=(main), adr=3): Divide by zero

$ LC_MESSAGES=pt_BR.utf8 /usr/local/bin/bc <<< 1/0
Erro de cálculo: dividir por 0
    Função: (main)
```

Infelizmente parece que, pelo menos por enquanto e semelhantemente ao equivalente GNU, ignora o valor de `LC_NUMERIC` e portanto não "trabalha" nativamente com a vírgula
como separador decimal e o ponto como separador de milhar em locales latinos como `pt_BR`, exigindo alguma técnica complementar para isso:

```bash
$ LC_ALL= LC_NUMERIC=pt_BR.utf8 /usr/local/bin/bc <<< 'scale=10; 10^4*22/7'
31428.571428
```

O comando `printf`, por exemplo, honra o formato numérico associado ao locale:

```bash
$ LC_ALL= LC_NUMERIC=en_US.utf8 printf "%'012.4f\n" 31428.571428
031,428.5714

$ LC_ALL= LC_NUMERIC=pt_BR.utf8 printf "%'012.4f\n" 31428.571428
-bash: printf: 31428.571428: número inválido
031.428,0000

$ LC_ALL= LC_NUMERIC=pt_BR.utf8 printf "%'012.4f\n" 31428,571428
031.428,5714
```

Portanto é ideal para driblar essa deficiência, bastando usá-lo com o resultado dos `bc`'s:

```bash
$ val=$(/usr/local/bin/bc <<< 'scale=10; 10^4*22/7')
$ echo $val
31428.5714285714
$ echo ${val/./,}
31428,5714285714

$ LC_ALL= LC_NUMERIC=pt_BR.utf8 printf -v val "%'012.4f\n" ${val/./,}
$ echo $val
031.428,5714
```

Outras alternativas para formatar a saída do `bc` incluem o (gnu) `sed`, o (gnu) `awk` e o comando `numfmt`. Não sendo built-ins do `bash` como o `printf`, são
sensivelmente mais lentos, mas cada um possui características suficientemente interessantes para que, dependendo da situação, seu uso se justifique.

## Comandos mencionados nesse artigo

- [bash](http://manpages.debian.org/bullseye/bash/bash.1.en.html)
- [gnu bc](http://manpages.debian.org/bullseye/bc/bc.1.en.html)
- [bc](http://freebsd.org/cgi/man.cgi?query=bc)
- [gnu sed](http://manpages.debian.org/bullseye/sed/sed.1.en.html)
- [sed](http://freebsd.org/cgi/man.cgi?query=sed)
- [gnu awk](http://manpages.debian.org/bullseye/gawk/gawk.1.en.html)
- [awk](http://freebsd.org/cgi/man.cgi?query=awk)
- [printf](http://manpages.debian.org/bullseye/bash/bash.1.en.html#printf)
- [numfmt](http://manpages.debian.org/bullseye/coreutils/numfmt.1.en.html)

