# Formatando _strings_ multibyte com `printf`

***Colaboração: Paulo Roberto Bagatini***

***Data de Publicação:***

Tenho um projeto em _shell_ chamado `shellect` (http://gitlab.com/arkanon/bash/shellect) onde me deparei com a necessidade de alinhar verticalmente caracteres quando as linhas possuem caracteres multibyte, como `ó` ou 🤓, por exemplo.

Apesar de ser um tanto improvável encontrar caracteres assim em um nome de diretório no Unix/Linux/BSD (ainda que tecnicamente possível), vou usar isso como desculpa para introduzir o assunto.

Começamos garantindo que temos instalada uma fonte monoespaçada com emojis gráficos que será (ou pelo menos espera-se que seja) usada pelo terminal. Se ainda não estava, após instalada pode ser necessário reiniciar o terminal.
```shell-session
$ sudo apt install fonts-noto-color-emoji
```

Vamos lá:
```shell-session
$ mkdir multibyte
$ cd multibyte
$ mkdir ó 🤓

$ LANG=pt_BR.utf8 ls -1
ó
🤓

$ LANG=C ls -1
''$'\303\263'
''$'\360\237\244\223'

$ LANG=C ls -1 --quoting-style=shell
??
????

$ LANG=C ls -1 --quoting-style=shell --show-control-chars
ó
🤓

$ LANG=C ls -1 --quoting-style=escape
\303\263
\360\237\244\223

$ estilos='literal locale shell shell-always shell-escape shell-escape-always c escape'
$ dir=ó
$ for i in $estilos; { printf '  %-21s' $i; LANG=C ls -d --quoting-style=$i $dir; }
  literal              ??
  locale               '\303\263'
  shell                ??
  shell-always         '??'
  shell-escape         ''$'\303\263'
  shell-escape-always  ''$'\303\263'
  c                    "\303\263"
  escape               \303\263
```

Na listagem acima vemos "coisas estranhas".

Em _locale_ `pt_BR.utf8`, tudo certo. O "problema" aparece em _locale_ `C`, que não suporta UTF-8 e portanto a codificação das _strings_ acaba sendo apresentada como uma sequência de números octais, que é uma forma possível para ele representar os caracteres na codificação original (UTF-8).
Listando em estilo _shell_, vemos duas interrogações no diretório `ó` e 4 no 🤓. Adicionando o parâmetro `--show-control-chars` voltamos a ver os caracteres corretamente, mas ainda percebe-se que o `ó` ocupa uma coluna na tela enquanto o 🤓 ocupa duas.

Por quê?

Criamos um vetor com alguma _strings_ que ocupam visualmente a mesma quantidade de colunas na tela em fonte monoespaçada:
```shell-session
$ multibyte=(
    'oo  oo'
    'oo  oó'
    'oo  óó'
    'oo  o✓'
    'oo  🤓'
    'oó  óó'
    'óó  óó'
    'óó  🤓'
    '🤓  🤓'
  )
```

A primeira observação é que o caractere 🤓 DE FATO ocupa duas colunas. O Unicode, ao estender o conjunto de caracteres ASCII, abrangeu classes que exigem mais espaço físico para serem impressos.

Agora observamos o resultado de um loop que utiliza o `printf` para deixar todas as _strings_ do vetor com 11 colunas de largura:
```shell-session
$ set "${multibyte[@]}"

$ for i; { printf "(%-11s)\n" "$i"; }
(oo  oo     )
(oo  oó    )
(oo  óó   )
(oo  o✓   )
(oo  🤓   )
(oó  óó  )
(óó  óó )
(óó  🤓 )
(🤓  🤓 )
```

Só a primeira _string_, formada apenas por `o`'s minúsculos não acentuados, teve o resultado esperado. As outras formaram um carnaval de desalinhamento... 🫤

Como o `printf` leva em consideração apenas a quantidade de bytes das _strings_ na formatação `%[N]s`, usá-lo com base apenas na quantidade de colunas desejadas é inefetivo para formatação de _strings_ unicode.
Esse comportamento com caracteres multibyte pode ser considerado um pequeno pesadelo: cada linha é retornada com um _padding_ diferente e aparentemente aleatório de espaços em branco, apesar da "largura" na tela ser a mesma.

Mas existe uma forma bastante razoável de resolver isso em _shell_.

Vamos analisar essas várias _strings_ com o comando `wc`:
```shell-session
$ wc --help | grep [mcL],
  -c, --bytes            mostra a quantidade de bytes
  -m, --chars            mostra a quantidade de caracteres
  -L, --max-line-length  emite o comprimento da linha mais longa

$ for i
  {
    read m c L <<< $(printf "$i" | wc -mcL)
    printf "%s %2s %s (%-11s)\n" $m $c $L "$i"
  }
6  6 6 (oo  oo     )
6  7 6 (oo  oó    )
6  8 6 (oo  óó   )
6  8 6 (oo  o✓   )
5  8 6 (oo  🤓   )
6  9 6 (oó  óó  )
6 10 6 (óó  óó )
5 10 6 (óó  🤓 )
4 10 6 (🤓  🤓 )
```

A terceira coluna da saída, resultado do parâmetro `-L` do `wc`, mostra que as _strings_ ocupam visualmente a mesma quantidade de colunas no terminal, apesar de formadas por quantidades diferentes de caracteres (primeira coluna, parâmetro `-m`).
A diferença entre elas, estruturalmente falando, é a quantidades de bytes (caracteres ASCII propriamente ditos) que cada uma ocupa, como mostra a segunda coluna, do parâmetro `-c`.

Podemos "ver" a estrutura desses caracteres. Eles podem ser entendidos como uma sequencia de caracteres ASCII...
```shell-session
$ printf o | od -An -tx1
 6f
$ echo -e '\x6f'
o

$ printf ó | od -An -tx1
 c3 b3
$ echo -e '\xc3\xb3'
ó

$ printf ✓ | od -An -tx1
 e2 9c 93
$ echo -e '\xe2\x9c\x93'
✓

$ printf 🤓 | od -An -tx1
 f0 9f a4 93
$ echo -e '\xf0\x9f\xa4\x93'
🤓
```

... ou caracteres propriamente ditos na codificação Unicode; no exemplo abaixo, de 32 bits (4 bytes):
```shell-session
$ printf o | iconv -t UTF-32LE | od -An -tx4
 0000006f
$ echo -e '\U6f'
o

$ printf ó | iconv -t UTF-32LE | od -An -tx4
 000000f3
$ echo -e '\Uf3'
ó

$ printf ✓ | iconv -t UTF-32LE | od -An -tx4
 00002713
$ echo -e '\U2713'
✓

$ printf 🤓 | iconv -t UTF-32LE | od -An -tx4
 0001f913
$ echo -e '\U1f913'
🤓
```

O pulo do gato, então, é perceber que o erro da formatação do `printf` pode ser compensado adicionando ao valor da largura desejada (11), a diferença entre a quantidade de bytes que forma a _string_ (dada pelo `-c` do `wc`) e a quantidade de colunas do terminal que a _string_ ocupa (dada pelo `-L`):
```shell-session
$ for i
  {
    read c L <<< $(printf "$i" | wc -cL)
    printf "(%-$((11+c-L))s)\n" "$i"
  }
(oo  oo     )
(oo  oó     )
(oo  óó     )
(oo  o✓     )
(oo  🤓     )
(oó  óó     )
(óó  óó     )
(óó  🤓     )
(🤓  🤓     )
```

Talvez na tela que você esteja lendo esse texto o alinhamento dos `)` acima não seja perfeito, mas isso é devido à renderização do seu _software_. Num terminal texto eles ficarão perfeitamente alinhados na vertical:

![printf](screenshot.png "printf")



## Um pouco de água fria

Para nossa tristeza, enquanto alguns caracteres ocupam 2 colunas para serem mostrados e os programas identificam isso, outros precisam de mais mas isso é ignorado.

O caractere ﷽ (Basmala, expressão árabe que significa "Em nome de Deus, o Mais Gracioso, o Mais Misericordioso"), por exemplo, mesmo também sendo codificado em 4 bytes (`\U0000fdfd`) precisa 12 (doze!) colunas na tela, mas tanto o `wc` quanto o terminal reconhecem apenas 1.

![Basmala](basmala.png "Basmala")

É bem provável que os sistemas de renderização de texto que usamos para ler esse texto apresentem adequadamente o caractere mesmo em fonte monoespaçada:
```shell-session
$ printf '1234567890123\n﷽\n'
1234567890123
﷽

$ printf '﷽' | wc -cL
      3       1

$ i=﷽🤓
$ read c L <<< $(printf "$i" | wc -cL)
$ printf "(%-$((11+c-L))s)\n" "$i"
(﷽🤓        )
```
Mas por enquanto, o que conseguimos na maioria dos terminais e editores de texto é algo como:

![Wider](wider.png "Wider")

Essa idiossincrasia está relacionada à incapacidade do _software_ de manipular as características gráficas dos caracteres unicode mais novos.
Assim como os caracteres que exigem 2 colunas antigamente ficavam mal representados no terminal e alguns editores de código, eventualmente os que exigem 3 ou mais colunas virão a ser adequadamente representados em algum momento futuro.
Até lá, o `printf` vai continuar apresentando resultados inconsistentes se algum deles estiver presente na _string_ a ser formatada...



## Referências

- [O que é UTF-8, UTF-16, UTF-32?](https://sriramadasvijay-medium-com.translate.goog/what-is-utf-8-utf-16-utf-32-d6d52f1f2ec7?_x_tr_sl=auto&_x_tr_tl=pt)
- [O mínimo absoluto que todo desenvolvedor de software deve saber absolutamente e positivamente sobre Unicode e conjuntos de caracteres (sem desculpas!)](https://www-joelonsoftware-com.translate.goog/2003/10/08/the-absolute-minimum-every-software-developer-absolutely-positively-must-know-about-unicode-and-character-sets-no-excuses/?_x_tr_sl=auto&_x_tr_tl=pt)
- [UTF-8 4-byte Character Chart](https://design215.com/toolbox/utf8-4byte-characters.php)
- [Unicode Character “﷽” (U+FDFD)](https://compart.com/en/unicode/U+FDFD)


## Comandos usados nesse artigo

-   [apt](http://man.he.net/?section=all&topic=apt)
-  [bash](http://man.he.net/?section=all&topic=bash):
             [cd](http://gnu.org/software/bash/manual/bash.html#index-cd),
           [echo](http://gnu.org/software/bash/manual/bash.html#index-echo),
            [for](http://gnu.org/software/bash/manual/bash.html#index-for),
         [printf](http://gnu.org/software/bash/manual/bash.html#index-printf),
           [read](http://gnu.org/software/bash/manual/bash.html#index-read),
            [set](http://gnu.org/software/bash/manual/bash.html#index-set)
-    [ls](http://man.he.net/?section=all&topic=ls)
- [mkdir](http://man.he.net/?section=all&topic=mkdir)
-    [od](http://man.he.net/?section=all&topic=od)
-  [sudo](http://man.he.net/?section=all&topic=sudo)
-    [wc](http://man.he.net/?section=all&topic=wc)
- [iconv](http://man.he.net/?section=all&topic=iconv)

☐
