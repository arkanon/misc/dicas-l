[[_TOC_]]

# Modificando o BASh para uso de *here-string* sem *linefeed* ao final da *string*

Quando precisamos processar uma *string* com um comando no `bash`, temos duas opções:
a tradicional e desaconselhável `echo`*`string`*`| comando` e a recomendável mas ainda pouco conhecida `comando <<<`*`string`*.

Acontece que, mesmo a primeira opção gerando um *fork* do *shell*, há situações em que somos obrigados a utilizá-la.
Um exemplo é na inspeção dos *bytes* que compõe uma *string*:

```shell-session
$ echo Ação | od -tx1
0000000 41 c3 a7 c3 a3 6f 0a
0000007
```

O último *byte* do retorno acima é o *linefeed* (`\x0a`), oriundo do fato do comando `echo`, por padrão, devolver sua entrada com uma quebra de linha.
Podemos evitar essa quebra de linha com o parâmetro `-n` do `echo`:

```shell-session
$ echo -n Ação | od -tx1
0000000 41 c3 a7 c3 a3 6f
0000006
```

Para evitar o *fork* do *shell* criado pelo *pipe* (`|`), podemos usar o redirecionamento específico para *string*, chamado *here-string*.
O problema é que ele obrigatóriamente incluirá a quebra de linha no final da *string*:

```shell-session
$ od -tx1 <<< Ação
0000000 41 c3 a7 c3 a3 6f 0a
0000007
```

Isso pode ser um problema se a *string* não puder de forma alguma vir acompanhada dessa quebra de linha, como no caso do cálculo de *hashes*:

```shell-session
$ echo Ação | md5sum
bc7b136759981a6e0991a30f024bba19  -

$ echo -n Ação | md5sum
369b1710073ecd32e2648f40437337b2  -

$ md5sum <<< Ação
bc7b136759981a6e0991a30f024bba19  -
```



---



Pensando nesse comportamento, criei um *patch* para a versão **5.3-alpha** do `bash` que implementa a possibilidade de executar um redirecionamento por *here-string*
sem que uma quebra de linha seja adicionada ao final da *string*. Adotei a notação `<<<-` para isso:

```shell-session
$ echo Ação | md5sum
bc7b136759981a6e0991a30f024bba19  -

$ echo -n Ação | md5sum
369b1710073ecd32e2648f40437337b2  -

$ md5sum <<< Ação
bc7b136759981a6e0991a30f024bba19  -

$ md5sum <<<- Ação
369b1710073ecd32e2648f40437337b2  -
```



## Aplicação do *patch*



Identificação da última versão do `bash`, disponível:

```shell-session
ult=$(lynx -dump -listonly -nonumbers https://ftp.gnu.org/gnu/bash/ | grep -E bash-[0-9].+.tar.[^.]+$ | sort -V | tail -n1)
pkg=${ult##*/}
ver=${pkg%.tar*}

echo "[$ult] [$pkg] [$ver]"
# [https://ftp.gnu.org/gnu/bash/bash-5.3-alpha.tar.gz] [bash-5.3-alpha.tar.gz] [bash-5.3-alpha]

wget $ult
```



Listagem das versões disponíveis do `bash` para *download* da versão desejada:
```shell-session
lynx -dump -listonly -nonumbers https://ftp.gnu.org/gnu/bash/ | grep -E bash-[0-9].+.tar.[^.]+$ | sort -V
ver=bash-5.3-alpha

export ver

wget https://ftp.gnu.org/gnu/bash/$ver.tar.gz
tar axf $ver.tar.gz
```

*Download* do *patch*:
```shell-session
wget https://gitlab.com/arkanon/misc/dicas-l/-/raw/master/ideias/bash-herestrings_without_linefeed/$ver-herestrings_without_linefeed.patch
```

Aplicação do *patch*:
```shell-session
patch -p0 < $ver-herestrings_without_linefeed.patch
```

Resultado esperado:
```shell-session
patching file bash-5.3-alpha/command.h
patching file bash-5.3-alpha/configure
patching file bash-5.3-alpha/copy_cmd.c
patching file bash-5.3-alpha/dispose_cmd.c
patching file bash-5.3-alpha/make_cmd.c
patching file bash-5.3-alpha/parse.y
patching file bash-5.3-alpha/parser-built
patching file bash-5.3-alpha/print_cmd.c
patching file bash-5.3-alpha/redir.c
```



## Se usando *container* para compilar



Execução do *container*

```shell-session
podman run -ti -v $PWD:/src -w /src -e ver --rm ubuntu:24.04 bash
```



Preparação do *container*

```shell-session
export TIMEFORMAT=%lR
time {
  unset HISTCONTROL
  export DEBIAN_FRONTEND=noninteractive
  export            TERM=xterm
  export              TZ=GMT+3
  apt update
  apt install -y apt-utils
  apt install -y command-not-found ncurses-term readline-common locales
  apt update
  apt upgrade -y
  bind -f /etc/inputrc
  bind -x '"\C-l": clear'
  . /etc/profile
  locale -a
  locale
  locale-gen pt_BR.utf8
  export LC_ALL=pt_BR.utf8
  alias l="ls -lapvT0 --color=always --group-directories-first -gG --time-style='+%Y/%m/%d %T'"
}
# 0m36,666s
```



## Em qualquer ambiente



### Compilação

```shell-session
time apt install -y build-essential bison
# 0m32,783s

export TIMEFORMAT=%lR

cd $ver

rm y.tab.?
make distclean

time ./configure | grep --color -wE '|no'
# 0m23,515s

time make
# 0m49,999s

stat -c %s bash
# 4998352

strip bash
stat -c %s bash
# 1421648

bash --version | head -n1
# GNU bash, version 5.2.21(1)-release (x86_64-pc-linux-gnu)

./bash --version | head -n1
# GNU bash, version 5.3.0(1)-alpha+hs_nolf (x86_64-pc-linux-gnu)
```



### Teste

```shell-session
env -i ./bash

echo $BASH_VERSION

export LC_ALL=pt_BR.utf8
export   TERM=xterm
bind -x '"\C-l": clear'

echo Ação | md5sum
# bc7b136759981a6e0991a30f024bba19  -

echo -n Ação | md5sum
# 369b1710073ecd32e2648f40437337b2  -

md5sum <<< Ação
# bc7b136759981a6e0991a30f024bba19  -

md5sum <<<- Ação
# 369b1710073ecd32e2648f40437337b2  -
```



## Procedimento utilizado para criação do *patch*



As principais modificações foram feitas nos arquivos `redir.c` e `parse.y`.
Os arquivos `y.tab.c` e `y.tab.h` contém inúmeras referências que precisariam ser adequadas se não eles fossem criados automaticamente durante a compilação.
De qualquer forma, o arquivo [ajustes_no_y.tab.c.md](ajustes_no_y.tab.c.md) mostra um procedimento que ajusta, em especial, as referências a linhas, que existem em `y.tab.c`.

```shell-session
lynx -dump -listonly -nonumbers https://ftp.gnu.org/gnu/bash/ | grep -E bash-[0-9].+.tar.[^.]+$ | sort -V

ver=bash-5.3-alpha

wget https://ftp.gnu.org/gnu/bash/$ver.tar.gz
tar axf $ver.tar.gz
cp -a $ver $ver-ori

podman run -ti -v $PWD:/src -w /src --rm ubuntu:24.04 bash

cd $ver
```



### Identificação dos arquivos com referências a *here-string*

```shell-session
grep -Er 'LESS_LESS_LESS|r_reading_string'
```

Resultado:
```shell-session
redir.c       :  if (ri != r_reading_string && (redirectee->flags & W_QUOTED))
redir.c       :  document = (ri == r_reading_string) ? expand_assignment_string_to_string (redirectee->word, 0)
redir.c       :  if (ri == r_reading_string)
redir.c       :    case r_reading_string:
redir.c       :    case r_reading_string:

command.h     :  r_appending_to, r_reading_until, r_reading_string,
copy_cmd.c    :    case r_reading_string:
dispose_cmd.c :  case r_reading_string:
make_cmd.c    :    case r_reading_string:           /* <<< foo */
print_cmd.c   :    case r_reading_string:

---------------

y.tab.c       :    LESS_LESS_LESS = 293,          /* LESS_LESS_LESS  */
y.tab.c       :#define LESS_LESS_LESS 293
y.tab.c       :  YYSYMBOL_LESS_LESS_LESS = 38,            /* LESS_LESS_LESS  */
y.tab.c       :  "LESS_LESS_LESS", "GREATER_AND", "SEMI_SEMI", "SEMI_AND",
y.tab.c       :  case 33: /* redirection: LESS_LESS_LESS WORD  */
y.tab.c       :                          (yyval.redirect) = make_redirection (source, r_reading_string, redir, 0);
y.tab.c       :  case 34: /* redirection: NUMBER LESS_LESS_LESS WORD  */
y.tab.c       :                          (yyval.redirect) = make_redirection (source, r_reading_string, redir, 0);
y.tab.c       :  case 35: /* redirection: REDIR_WORD LESS_LESS_LESS WORD  */
y.tab.c       :                          (yyval.redirect) = make_redirection (source, r_reading_string, redir, REDIR_VARASSIGN);
y.tab.c       :  { "<<<", LESS_LESS_LESS },
y.tab.c       :   token == LESS_LESS || token == LESS_LESS_LESS || \
y.tab.c       :                return (LESS_LESS_LESS);

parse.y       :%token AND_AND OR_OR GREATER_GREATER LESS_LESS LESS_AND LESS_LESS_LESS
parse.y       :        |       LESS_LESS_LESS WORD
parse.y       :                          $$ = make_redirection (source, r_reading_string, redir, 0);
parse.y       :        |       NUMBER LESS_LESS_LESS WORD
parse.y       :                          $$ = make_redirection (source, r_reading_string, redir, 0);
parse.y       :        |       REDIR_WORD LESS_LESS_LESS WORD
parse.y       :                          $$ = make_redirection (source, r_reading_string, redir, REDIR_VARASSIGN);
parse.y       :  { "<<<", LESS_LESS_LESS },
parse.y       :   token == LESS_LESS || token == LESS_LESS_LESS || \
parse.y       :                return (LESS_LESS_LESS);

y.tab.h       :    LESS_LESS_LESS = 293,          /* LESS_LESS_LESS  */
y.tab.h       :#define LESS_LESS_LESS 293

parser-built  :    LESS_LESS_LESS = 293,          /* LESS_LESS_LESS  */
parser-built  :#define LESS_LESS_LESS 293
```



### Listagem dos arquivos com referências a *here-string*

```shell-session
grep -Erl 'LESS_LESS_LESS|r_reading_string' | sort
```

Resultado:
- command.h
- copy_cmd.c
- dispose_cmd.c
- make_cmd.c
- parse.y
- parser-built
- print_cmd.c
- redir.c
- y.tab.c
- y.tab.h



### Modificação dos arquivos identificados

```shell-session
gvim -p $(grep -Erl 'LESS_LESS_LESS|r_reading_string' | sort)
```



### Visualização das adições

```shell-session
cgrep()
{
  local c=$1; shift
  local -x GREP_COLORS=mt=${c//,/;}
  grep --line-buffered --color=always -E "$@"
}

cgrep 1,34 -rn 'LESS_LESS_LESS|r_reading_string|<<<' 2>&- |
cgrep 1,33     '|_MINUS|_nolf|<<<-'                       |
cgrep 1,41     '|-'                                       |
 grep   -E -iv 'doc/|examples/|tests/|change|news|patch'
```



### Ajuste no `configure` para identificação do *patch* no número da versão

```shell-session
sed -r  's/(RELSTATUS=)([^+]+)(\+hs_nolf)?/\1\2+hs_nolf/' configure | grep RELSTATUS=

sed -ri 's/(RELSTATUS=)([^+]+)(\+hs_nolf)?/\1\2+hs_nolf/' configure

grep RELSTATUS= configure
# RELSTATUS=alpha+hs_nolf
```



### Criação do *patch*

```shell-session
cd ..
diff -ruN ${ver?}-ori $ver > $ver-herestrings_without_linefeed.patch
```



☐
