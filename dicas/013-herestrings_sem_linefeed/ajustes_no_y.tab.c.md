[[_TOC_]]

# Correção do número de linha indicado em `y.tab.c`

```shell-session
f=$PWD-ori/y.tab.c
f=y.tab.c-line_err
f=y.tab.c
```



## Linhas com o padrão `#line [0-9]+ .+y.tab.c`

```shell-session
re='#line [0-9]+ .+y.tab.c'

grep -En "$re" $f # 180 ocorrências
```



```shell-session
grep -En "$re" $f  |
sed -r ' s/:#line / / ; s/ [^ ]+$// ' |
while read line numb
do
  echo "Linha $line de $f deveria conter o valor $((line+1)) -> $line-$((line+1))=$((line-(line+1))), mas contém $numb -> $line-$numb=$((line-numb))"
done

line=3630
sed -rn "${line}{s/[0-9]+/$((line+1))/;p}" $f

grep -En "$re" $f |
cut -d: -f1 |
while read line
do
# sed -rn "${line}{s/[0-9]+/$((line+1))/;p}" $f
  sed -ri "${line}{s/[0-9]+/$((line+1))/}"   $f
done
```



## Linhas com o padrão `#line [0-9]+ .+parse.y`

```shell-session
re='#line [0-9]+ .+parse.y'

N=$(grep -En "$re" $f | wc -l) # 180 ocorrências após o patch, 177 no original

ref=$PWD-ori/parse.y
ref=parse.y

r=$'\e[1;31m'
g=$'\e[1;32m'
y=$'\e[1;33m'
b=$'\e[1;44m'
o=$'\e[0m'
```



```shell-session
n=1
grep -En "$re" $f  |
sed -r ' s/:#line / / ; s/ [^ ]+$// ' |
while read line numb
do
  numbp=$numb
# [[ $f =~ ori ]] || (( n<57 || n>=177 && n<=179 )) &&
# numbp=$numb ||
# { ((numbp=numb+18)); but=", mas deveria ser $y$numbp$o"; }
  cont1=$b$(sed -n "$((line+1))p" $f)$o
  cont2=$b$(sed -n "${numbp}p" $ref)$o
  echo     "Ref $r$((n))/$N$o - Linha $y$line$o de $f faz referência à linha $y$numb$o de parse.y$but"
  echo -e  "A linha $numb deveria conter\n$cont1\nmas contém\n${cont2//$'\t'/        }"
  if (( numb != numbp ))
  then
    echo -en "$g"
    sed  -rn "${line}{s/[0-9]+/$numbp/;p}" $f
    sed  -ri "${line}{s/[0-9]+/$numbp/}"   $f
    echo -en "$o"
  fi
  echo
  ((n++))
done | less
```



☐
