

No vídeo de hoje (https://youtu.be/vnJXwAQBetI), o Pssor Julio falou sobre mais algumas mallandragens do comando `tput`.

Duas delas, em especial, me chamaram atenção: o `tput flash`, que faz a tela piscar (*visual bell*) e o `tput bel` (*audible bell*), que provoca o
tradicional *beep*. A primeira porque, mesmo conhecendo o conceito, nunca implementei, propriamente. A segunda, porque estou costumado a usar com o
comando `echo -e '\a'` (ou o equivalente com `printf` ou código em hexa, octal ou literal).

Aí fui testar, só pra constar. Nenhum dos dois funcionou.

Antes de pegar um machado e sair quebrando tudo, resolvi dar uma pesquisada no Google e, impressionantemente, precisei de um BOM TEMPO para identificar
o problema. Ou melhor: problemas.

Basicamente, minha combinação de aplicativos/configurações estava completamente fora do padrão: `pulse audio` + `xfce-terminal` + `screen`, cada um com
algum problema na sua configuração.

Uso a tanto tempo essa combinação e meu home está a tanto tempo "indo daqui pra lá", que não sei se são problemas que vem por *default* ou foram inseridos
por mim em algum momento.

```bash
sudo cp -a /etc/pulse/default.pa{,-ori}

cat << EOT | sudo tee /etc/pulse/default.pa.d/bell.pa
# audible bell
load-sample-lazy x11-bell /usr/share/sounds/freedesktop/stereo/bell.oga
load-module module-x11-bell sample=x11-bell
EOT

pulseaudio -k
```

Ainda é preciso garantir que o dispositivo de saída de áudio configurado é o correto e ele não só não está no mudo como tem um volume adequado. É possível
verificar isso com algum dos mixers disponíveis, tanto por linha de comando quanto gráficos. No meu caso, é o `pavucontrol` e a saída é um *headset* USB
Plantronics.

O *bell*, nesse caso, não é o som senoidal puro do *pc speaker*. É um arquivo `oga`, e pode ser livremente escolhido ou mesmo criado.

Bom. Isso tudo, executando o `xfce-terminal` "puro". Que raramente é o meu caso. Há muito anos, já, adotei o `screen` como "intermediário" entre minhas
sessões `bash` interativas e o emulador de terminal que for. Fiz isso porque estava cansado de fechar uma janela inteira ao invés de uma aba ou de perder
processos remotos demorados ou críticos por perda de conexão. Assim, um *wrapperzinho* no meu `/etc/profile` inicia sessões interativas do bash com um
contador que me dá tempo de escolher entre cancelar a sessão, iniciar uma sessão sem `screen` ou (default) iniciá-la com o screen. Muito prático.
Considerando que algumas características de terminal precisam de configurações ou ações especiais.

O `control`+`a`, por exemplo, que desde tempos imemoriais leva o cursor para o início da linha, "não funciona". Mas o `control`+`e`, que leva para o fim
dela, funciona... :-/ Por muito tempo quebrei a cabeça sem entender a razão disso, até que me dei conta que o `screen` usa a combinação `control`+`a` para
ativar suas próprias combinações de controle, e que, portanto, para ir para o início de uma linha no `bash`, deveria pressionar `control`+`a` duas vezes!
Claro que isso é inaceitável para alguém nascido no tempo das cavernas, então a configuração `escape ^Jj` (ou qualquer outra letra que não interfira
no uso harmonioso do shell) alterou o caractere de comando de `a` (acionado por `control`+`a`) para `j`.

Relatei o "causo" acima apenas para exemplificar o tipo de interferência que o `screen` pode provocar. Ele cumpre uma função extremamente nobre para
alguém que vive em terminais remotos, mas tem suas idiossincrasias.

```bash
cat << EOT | tee ~/.config/gtk-3.0/gtk.css
button.suggested-action {
  font-weight: normal;
  color: white;
  outline-color: rgba(255, 255, 255, 0.3);
  background-color: #bd93f9;
  text-shadow: none;
  box-shadow: inset 0px 1px 0px 0px rgba(255, 255, 255, 0.1), inset 0px -1px 0px 0px rgba(0, 0, 0, 0.1);
}
EOT

xfce4-panel -r
```


Da mesma forma, os comandos `tput smcup` e `tput rmcup` que entram e saem da tela alternativa só funcionarão no `screen` se estiver configurado com
`altscreen on`.


