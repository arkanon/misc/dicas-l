

newip()
{
  local nc chat n
  n='nc 127.0.0.1 9051'
  c=$'AUTHENTICATE ""\r\nsignal NEWNYM\r\nQUIT'
  i=1
  $n <<< $c > /dev/null
  while ! getip
  do
    $n <<< $c > /dev/null
    echo -n "$i "
    sleep 1
    ((i++))
  done
}

getip()
{
  local out=$(torsocks curl $pubip 2>&-)
  ! grep -q Forbidden <<< $out && echo $out || return 1
}


apt install -y curl tor netcat-openbsd

echo -e 'ControlPort 9051\nCookieAuthentication 0' | sudo tee /etc/tor/torrc

ps h -o pid:1 -C tor || sudo /etc/init.d/tor start

pubip=ifconfig.me
pubip=icanhazip.com
pubip=api.ipify.org
pubip=bot.whatismyipaddress.com
pubip=ipinfo.io/ip
pubip=ipecho.net/plain

curl $pubip 2>&-; echo
getip
newip

killall tor


