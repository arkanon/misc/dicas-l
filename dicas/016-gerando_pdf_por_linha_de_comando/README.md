# Gerando PDF por linha de comando



## Man page para PDF

```bash
sudo apt install groff
man -Tpdf ls > ls.pdf

sudo apt install manpages
sudo locale-gen en_US.utf8
locale -a
LC_ALL=en_US.utf8 man -Tpdf ls > ls-en_US.pdf

sudo apt install manpages-de
sudo locale-gen de_DE.utf8
locale -a
LC_ALL=de_DE.utf8 man -Tpdf ls > ls-de_DE.pdf

sudo apt install manpages-pt-br
sudo locale-gen pt_BR.utf8
locale -a
LC_ALL=pt_BR.utf8 man -Tpdf ls > ls-pt_BR.pdf
```

Só cuidado, que as man pages em outras línguas que não inglês podem estar defasadas em relação à versão do comando/pacote instalado.



## Texto ANSI para PDF

```bash
a2h=http://raw.githubusercontent.com/pixelb/scripts/master/scripts/ansi2html.sh

sudo curl -sL $a2h -o /usr/local/bin/ansi2html
sudo chmod +x /usr/local/bin/ansi2html

sudo apt install wkhtmltopdf
sudo ln -s /usr/bin/wkhtmltopdf /usr/local/bin/html2pdf

# TODO [verificar início de opção com - para assumir resto de parâmetros como opções extra do grep]
ecgrep()
{
  local c
  [[ $1 =~ [0-9,] ]] && { c=$1; shift; } || c=1,31
  GREP_COLORS=mt=${c//,/;} grep --line-buffered --color=always -E "|$@"
}

cgrep()
{
  local c=$1; shift
  local -x GREP_COLORS=mt=${c//,/;}
  grep --line-buffered --color=always -E "$@"
}

cgrep 1,34 -rn 'LESS_LESS_LESS|r_reading_string|<<<' 2>&-|
cgrep 1,33     '|_MINUS|_nolf|<<<-'                      |
cgrep 1,41     '|-'                                      |
 grep   -E -iv 'doc/|examples/|tests/|change|news|patch' |
ansi2html --bg=dark                                      |
html2pdf -O Portrait - grep.pdf
```



## Imagens para PDF

```bash
prefix=inventario-2024-01

img2pdf $prefix-p*.jpg -o $prefix-600dpi.pdf

args=(

  -dBATCH
  -dQUIET
  -dNOCACHE
  -dNOPAUSE
  -dNOSAFER
  -dCompatibilityLevel=1.7

  -sDEVICE=pdfwrite

  # [página]
# -sPAPERSIZE=a4         # letter a4 legal
# -g8112x7596
# -dDEVICEWIDTHPOINTS=674
# -dDEVICEHEIGHTPOINTS=912
# -dPDFFitPage

  # [resolução]
  -dPDFSETTINGS=/ebook # default screen:72 ebook:150 printer:300 prepress:300
# -r600

  -sOutputFile=$prefix.pdf

  $prefix-600dpi.pdf

# viewjpeg.ps
# -c \(${prefix}-p{1..6}.jpg\)\ viewJPEG\ showpage

)

gs "${args[@]}"
```


## Referências
- [Paper Sizes Known to GhostScript](http://ghostscript.readthedocs.io/en/latest/Use.html#appendix-paper-sizes-known-to-ghostscript)
- [How to set custom page size with GhostScript](http://stackoverflow.com/a/12675710/3149974)
- [Convert JPEG Image to PDF Using GhostScript](http://leimao.github.io/blog/JPEG-Image-to-PDF-Ghostscript)
- [Setting auto-height/width for converted Jpeg from PDF using GhostScript](http://stackoverflow.com/a/10022199/3149974)



☐
