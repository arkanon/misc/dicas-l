<!--
# 2022/08/10 (Wed) 05:39:41 -03
# 2022/08/10 (Wed) 02:41:48 -03
-->

# Builtins Carregáveis no BASh

[[_TOC_]]



## Pacote de exemplos
```shell-session
$ apt info bash-builtins
Package: bash-builtins
Version: 5.1-3ubuntu2
Priority: optional
Section: universe/utils
Source: bash
Origin: Ubuntu
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Original-Maintainer: Matthias Klose <doko@debian.org>
Bugs: https://bugs.launchpad.net/ubuntu/+filebug
Installed-Size: 1.076 kB
Depends: bash (= 5.1-3ubuntu2)
Homepage: http://tiswww.case.edu/php/chet/bash/bashtop.html
Download-Size: 143 kB
APT-Manual-Installed: yes
APT-Sources: http://old-releases.ubuntu.com/ubuntu impish/universe amd64 Packages
Description: "builtins" carregáveis do Bash - cabeçalhos & exemplos
 Bash pode carregar dinamicamente novos comandos builtin. Incluídos, estão
 os cabeçalhos necessários para compilar seus próprios "builtins" e muitos
 exemplos.

$ sudo apt install bash-builtins

$ BLOCK_SIZE=\'1 ls -l /usr/lib/bash
total 548
 -rwxr-xr-x  1  root root  14.888  out  7  2021  accept
 -rwxr-xr-x  1  root root  14.632  out  7  2021  basename
 -rwxr-xr-x  1  root root  14.696  out  7  2021  csv
 -rwxr-xr-x  1  root root  23.272  out  7  2021  cut
 -rwxr-xr-x  1  root root  14.600  out  7  2021  dirname
 -rwxr-xr-x  1  root root  14.840  out  7  2021  fdflags
 -rwxr-xr-x  1  root root  14.920  out  7  2021  finfo
 -rwxr-xr-x  1  root root  14.696  out  7  2021  head
 -rwxr-xr-x  1  root root  14.632  out  7  2021  id
 -rwxr-xr-x  1  root root  14.696  out  7  2021  ln
 -rw-r--r--  1  root root     993  out  7  2021  loadables.h
 -rwxr-xr-x  1  root root  14.632  out  7  2021  logname
 -rw-r--r--  1  root root   2.826  out  7  2021  Makefile.inc
 -rwxr-xr-x  1  root root  14.792  out  7  2021  mkdir
 -rwxr-xr-x  1  root root  14.696  out  7  2021  mkfifo
 -rwxr-xr-x  1  root root  14.856  out  7  2021  mktemp
 -rwxr-xr-x  1  root root  14.600  out  7  2021  mypid
 -rwxr-xr-x  1  root root  14.696  out  7  2021  pathchk
 -rwxr-xr-x  1  root root  14.728  out  7  2021  print
 -rwxr-xr-x  1  root root  14.632  out  7  2021  printenv
 -rwxr-xr-x  1  root root  14.728  out  7  2021  push
 -rwxr-xr-x  1  root root  14.664  out  7  2021  realpath
 -rwxr-xr-x  1  root root  14.664  out  7  2021  rm
 -rwxr-xr-x  1  root root  14.600  out  7  2021  rmdir
 -rwxr-xr-x  1  root root  18.952  out  7  2021  seq
 -rwxr-xr-x  1  root root  14.632  out  7  2021  setpgid
 -rwxr-xr-x  1  root root  14.600  out  7  2021  sleep
 -rwxr-xr-x  1  root root  14.664  out  7  2021  strftime
 -rwxr-xr-x  1  root root  14.536  out  7  2021  sync
 -rwxr-xr-x  1  root root  14.664  out  7  2021  tee
 -rwxr-xr-x  1  root root  14.328  out  7  2021  truefalse
 -rwxr-xr-x  1  root root  14.632  out  7  2021  tty
 -rwxr-xr-x  1  root root  14.632  out  7  2021  uname
 -rwxr-xr-x  1  root root  14.568  out  7  2021  unlink
 -rwxr-xr-x  1  root root  14.600  out  7  2021  whoami

# O código fonte dos exemplos está nos diretórios abaixo.
$ BLOCK_SIZE=\'1 ls -l /usr/include/bash /usr/share/doc/bash/examples/loadables
```



## Controle
```shell-session
$ help enable
enable: enable [-a] [-DnPs] [-f ARQUIVO] [NOME ...]
    Habilita e desabilita comandos internos do shell.

    Habilita e desabilita comandos internos do shell. Desabilitar
    permite que você executa um comando do disco que possui o mesmo
    nome que um outro comando interno sem usar um caminho completo.

    Opções:
      -a        mostra uma lista de comandos internos mostrando se cada
                um está habilitado
      -n        desabilita cada NOME ou exibe uma lista de comandos
                internos desabilitados
      -p        exibe a lista de comandos internos em um formato usável
      -s        exibe apenas nomes dos comandos internos 'especial' Posix

    Opções de controle de carregamento dinâmico:
      -f        carrega comando interno NOME do objeto compartilhado ARQUIVO
      -d        remove um comando interno carregado com -f

    Não sendo informado uma opção, cada NOME é habilitado.

    Para usar o `test' encontrado em $PATH, ao invés da versão de comando
    interno do shell, digite `enable -n test'.

    Status de saída:
    Retorna sucesso, a menos que NOME não seja um comando interno de shell
    ou ocorrer um erro.

$ enable
enable .
enable :
enable [
enable alias
enable bg
enable bind
enable break
enable builtin
enable caller
enable cd
enable command
enable compgen
enable complete
enable compopt
enable continue
enable declare
enable dirs
enable disown
enable echo
enable enable
enable eval
enable exec
enable exit
enable export
enable false
enable fc
enable fg
enable getopts
enable hash
enable help
enable history
enable jobs
enable kill
enable let
enable local
enable logout
enable mapfile
enable popd
enable printf
enable pushd
enable pwd
enable read
enable readarray
enable readonly
enable return
enable set
enable shift
enable shopt
enable source
enable suspend
enable test
enable times
enable trap
enable true
enable type
enable typeset
enable ulimit
enable umask
enable unalias
enable unset
enable wait
```



## Uso
```shell-session
$ head /usr/share/doc/bash/examples/loadables/cut.c -n35
/* cut,lcut - extract specified fields from a line and assign them to an array
              or print them to the standard output */

[REMOVIDO TEXTO DA LICENÇA]

/* See Makefile for compilation details. */

#include <config.h>

#if defined (HAVE_UNISTD_H)
#  include <unistd.h>
#endif
#include "bashansi.h"
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>

#include "loadables.h"
#include "shmbutil.h"
```



```shell-session
$ type -a cut
cut é /usr/bin/cut
cut é /bin/cut

$ enable | grep cut

$ enable -f /usr/lib/bash/cut cut

$ type -a cut
cut é um comando interno do shell
cut é /usr/bin/cut
cut é /bin/cut

$ enable | grep cut
enable cut

$ enable -d cut

$ type -a cut
cut é /usr/bin/cut
cut é /bin/cut

$ export BASH_LOADABLES_PATH=/usr/lib/bash
$ enable -f cut cut
```



## Comparação de eficiência
Comando externo `cut` com seu builtin carregável e a expansão de parâmetros equivalente.  
Comparação através da função [benshmark](http://gitlab.com/arkanon/benshmark).
```shell-session
$ . ../../benshmark/v3.sh
$ alias benshmark=benshmark-v3

$ string=abc
$ externo(){ /bin/cut -c1 <<< $string; }
$ builtin(){ cut -c1 <<< $string; }
$ xpansao(){ echo ${string:0:1}; }

$ externo; builtin; xpansao
a
a
a


$ benshmark 10000 externo builtin xpansao

externo  00:08,620
builtin  00:00,130
xpansao  00:00,060

1º  xpansao
2º  builtin/xpansao    1,166 vezes mais lenta
3º  externo/xpansao  142,666 vezes mais lenta


$ benshmark 10000 externo builtin

externo  00:08,717
builtin  00:00,127

1º  builtin
2º  externo/builtin  67,637 vezes mais lenta


$ benshmark 100000 builtin xpansao

builtin  00:01,293
xpansao  00:00,598

1º  xpansao
2º  builtin/xpansao  1,162 vezes mais lenta
```



## Referências
- http://gnu.org/software/bash/manual/html_node/Bash-Builtins.html#index-enable
- http://git.savannah.gnu.org/cgit/bash.git/tree/NEWS
- http://git.savannah.gnu.org/cgit/bash.git/tree/CHANGES
- http://flylib.com/books/en/4.109.1.109/1
- http://web.archive.org/web/20160303032434/http://cfajohnson.com/shell/articles/dynamically-loadable
<!-- -->
- http://tiswww.case.edu/php/chet/bash/bash-intro.html
- http://shell-tips.com/bash/what-is-new-in-gnu-bash-5
- http://mywiki.wooledge.org/BashLoadableBuiltins
- http://unix.stackexchange.com/questions/582361/how-to-build-loadable-builtins-for-bash
- http://github.com/geirha/bash-builtins



☐
