# symb.sh

# Arkanon <arkanon@lsd.org.br>
# 2025/01/21 (Tue) 06:28:45 -03
# 2025/01/19 (Sun) 12:07:36 -03

# <https://unix-stackexchange-com.translate.goog/questions/117280/what-is-the-rationale-for-the-bash-shell-not-warning-you-of-arithmetic-overflow?_x_tr_sl=auto&_x_tr_tl=pt>



  testes()
  {

    getconf -a | grep -i long
    # LONG_BIT   64
    # ULONG_MAX  18446744073709551615

    echo $(( 2**63  )) # -9223372036854775808
    echo $(( 2**63-1)) #  9223372036854775807
    echo $((-2**63  )) # -9223372036854775808
    echo $((-2**63-1)) #  9223372036854775807

    . symb.sh

    bc <<< 'scale=15; 1234/7' # 176.285714285714285
    razao 1234 7 15           # 176.285714285714285

    bc <<< 'scale=16; 1234/7' # 176.2857142857142857
    razao 1234 7 16           # -87.2392010529935945

    bc <<< 'scale=15; 1/1888' # .000529661016949
    razao 1 1888 15           # -bash: -precisao: expressão de substring < 0

    razao 10000 7    14 # 1.428,57142857142857
    razao 1000  7    14 #   142,85714285714285
    razao 100   7    14 #    14,28571428571428
    razao 10    7    14 #     1,42857142857142
    razao 1     7    14 #     0,14285714285714
    razao 1     70   14 #     0,01428571428571
    razao 1     700  14 #     0,00142857142857
    razao 1     7000 14 #     0,00014285714285

    . <(curl -ksL ishortn.ink/benshmark-v6)

    bm 'bc <<< "scale=15;1/700"' 'razao 1 700 15'
    # bc <<< "scale=15;1/700"  .001428571428571
    # razao 1 700 15          0.001428571428571

    bm 2000 'bc <<< "scale=15;1/700"' 'razao 1 700 15'
    # bc <<< "scale=15;1/700" 00:03,341
    # razao 1 700 15          00:00,160
    #
    # 1º  razao 1 700 15
    # 2º  bc <<< "scale=15;1/700"   19,881 vezes (  1.988,125%) mais lenta que razao 1 700 15



    n=1 d=1888 p=10

    bm razao{1..8}
    # razao1  0.0005296610
    # razao2  0,0005296610
    # razao3  0,0005296610
    # razao4  0,0005296610
    # razao5  0,0005296610
    # razao6  0,0005296610
    # razao7  0,0005296610
    # razao8  0,0005296610

    bm 200 razao{1..8}
    # razao1  00:00,421
    # razao2  00:00,027
    # razao3  00:00,476
    # razao4  00:12,756
    # razao5  00:00,019
    # razao6  00:00,013
    # razao7  00:00,015
    # razao8  00:00,026
    #
    # 1º  razao6
    # 2º  razao7    0,153 vezes (     15,384%) mais lenta que razao6
    # 3º  razao5    0,461 vezes (     46,153%) mais lenta que razao6
    # 4º  razao8    1,000 vez   (    100,000%) mais lenta que razao6
    # 5º  razao2    1,076 vez   (    107,692%) mais lenta que razao6
    # 6º  razao1   31,384 vezes (  3.138,461%) mais lenta que razao6
    # 7º  razao3   35,615 vezes (  3.561,538%) mais lenta que razao6
    # 8º  razao4  980,230 vezes ( 98.023,076%) mais lenta que razao6

    n=1 d=1888 p=300

    bm razao{1..8}
    # razao1  0.000529661016949152542372881355932203389830508474576271186440677966101694915254237288135593220338983050847457627118644067796610169491525423728813559322033898305084745762711864406779661016949152542372881355932203389830508474576271186440677966101694915254237288135593220338983050847457627118644067796610
    # razao2  overflow1
    # razao3  overflow1
    # razao4  0,000529661016949152542372881355932203389830508474576271186440677966101694915254237288135593220338983050847457627118644067796610169491525423728813559322033898305084745762711864406779661016949152542372881355932203389830508474576271186440677966101694915254237288135593220338983050847457627118644067796610
    # razao5  0,000529661016949152542372881355932203389830508474576271186440677966101694915254237288135593220338983050847457627118644067796610169491525423728813559322033898305084745762711864406779661016949152542372881355932203389830508474576271186440677966101694915254237288135593220338983050847457627118644067796610
    # razao6  0,000529661016949152542372881355932203389830508474576271186440677966101694915254237288135593220338983050847457627118644067796610169491525423728813559322033898305084745762711864406779661016949152542372881355932203389830508474576271186440677966101694915254237288135593220338983050847457627118644067796610
    # razao7  0,000529661016949152542372881355932203389830508474576271186440677966101694915254237288135593220338983050847457627118644067796610169491525423728813559322033898305084745762711864406779661016949152542372881355932203389830508474576271186440677966101694915254237288135593220338983050847457627118644067796610
    # razao8  0,000529661016949152542372881355932203389830508474576271186440677966101694915254237288135593220338983050847457627118644067796610169491525423728813559322033898305084745762711864406779661016949152542372881355932203389830508474576271186440677966101694915254237288135593220338983050847457627118644067796610

    bm 20 razao{1..8}
    # razao1  00:00,057
    # razao2  erro
    # razao3  erro
    # razao4  00:35,150
    # razao5  00:00,033
    # razao6  00:00,029
    # razao7  00:00,034
    # razao8  00:00,036
    #
    # 1º  razao6
    # 2º  razao5      0,137 vezes (     13,793%) mais lenta que razao6
    # 3º  razao7      0,172 vezes (     17,241%) mais lenta que razao6
    # 4º  razao8      0,241 vezes (     24,137%) mais lenta que razao6
    # 5º  razao1      0,965 vezes (     96,551%) mais lenta que razao6
    # 6º  razao4  1.211,068 vezes (121.106,896%) mais lenta que razao6

  }



  razao1()
  {
    ((n<d)) && printf 0
    BC_LINE_LENGTH=0 bc <<< "scale=$p; $n/$d"
  }



  razao2()
  {

    local lc uso numerador denominador precisao diferenca complemento inteiro decimal

             lc=${LC_ALL:-${LC_NUMERIC:-C}}

           uso+=$'\e[10D          \n'
           uso+=$'Uso: [LC_ALL=<locale>] '$FUNCNAME$' <numerador> <denominador> [precisão]\n\n'
           uso+=$'Devolve para o stderr numerador dividido por denominador com a precisão indicada (5 dígitos por default).\n'
           uso+=$'Popula a variável $razao com o resultado.\n'

      numerador=${n?"$uso"}
    denominador=${d?"$uso"}
       precisao=${p:-5}
      diferenca=$((${#denominador}-${#numerador}))

    local -A decimalsep=(
      [pt_BR]=,
      [en_US]=.
      [POSIX]=.
          [C]=.
    )

    ((diferenca>0)) && printf -v complemento %0${diferenca}d

      razao=$complemento$((numerador*10**precisao/denominador ))
    ((precisao>${#razao})) && { echo overflow1 >&2; return 2; }
    inteiro=${razao:0:-precisao}
    decimal=${razao: -precisao}
      razao=$inteiro${decimalsep[${lc%.*}]}$decimal

    printf -v razao "%'.${precisao}f" $razao 2>&- || { echo overflow2 >&2; return 3; }
    printf  "$razao\n" >&2

  }



  razao3()
  {

    local lc uso numerador denominador precisao diferenca complemento inteiro decimal

             lc=${LC_ALL:-${LC_NUMERIC:-C}}

           uso+=$'\e[10D          \n'
           uso+=$'Uso: [LC_ALL=<locale>] '$FUNCNAME$' <numerador> <denominador> [precisão]\n\n'
           uso+=$'Devolve para o stderr numerador dividido por denominador com a precisão indicada (5 dígitos por default).\n'
           uso+=$'Popula a variável $razao com o resultado.\n'

      numerador=${n?"$uso"}
    denominador=${d?"$uso"}
       precisao=${p:-5}
      diferenca=$((${#denominador}-${#numerador}))

    local -A decimalsep=(
      [pt_BR]=,
      [en_US]=.
      [POSIX]=.
          [C]=.
    )

    ((diferenca>0)) && printf -v complemento %0${diferenca}d

      razao=$complemento$((numerador*10**precisao/denominador ))
    ((precisao>${#razao})) && { echo overflow1 >&2; return 2; }
    inteiro=${razao:0:-precisao}
    decimal=${razao: -precisao}
      razao=$inteiro${decimalsep[${lc%.*}]}$decimal

    # Bash printf float formatting became nonsensical and random <http://unix.stackexchange.com/a/783724>
    local printf
    LC_ALL=C printf -v x %.1f 0.1
    [[ $x == 0.1 ]] && printf=printf || printf=/bin/printf
  # echo [$x] [$printf]
    razao=$(LC_ALL=$lc $printf "%'.${precisao}f" $razao 2>&-) || { echo overflow2 >&2; return 3; }

  # printf -v razao "%'.${precisao}f" $razao

    printf  "$razao\n" >&2

  }



  razao4()
  {
    if  [ $# -ne 0 ]
    then
      echo "$0: Sintaxe: $0 <Dividendo> <Divisor> <Qtd. Decimais na Saida>"
      return 1
    fi
    Divid=$n
    Divis=$d
    Decim=$p
    Resp=`expr $Divid / $Divis`,
    while [ "$Decim" -gt 0 ]
    do
      Divid=`expr $Divid % $Divis \* 10`
       Resp=$Resp`expr $Divid / $Divis`
      Decim=`expr $Decim - 1`
    done
    echo $Resp
  }



  razao5()
  {
    local n=$n d=$d p=$p r
    r=$((n/d)),
    while ((p-->0))
    do
       n=$((10*(n%d)))
      r+=$((n/d))
    done
    echo $r
  }



  razao6()
  {
    local n=$n d=$d p=$p r
    r=$((n/d)),
    while ((p-->0))
    do
       n=$((n%d))0
      r+=$((n/d))
    done
    echo $r
  }



  razao7()
  {
    local n=$n d=$d p=$p r
    printf -v r "%'.1f" $((n/d)) # bug no printf built-in <https://unix-stackexchange-com.translate.goog/questions/783623/bash-printf-float-formatting-became-nonsensical-and-random/783724?_x_tr_sl=auto&_x_tr_tl=pt#783724>
    r=${r:: -1}
    while ((p-->0))
    do
       n=$((n%d))0
      r+=$((n/d))
    done
    echo $r
  }



  razao8()
  {

    local numerador=$n divisor=$d precisao=$p lc=${LC_ALL:-${LC_NUMERIC:-C.utf8}}

    local -A decimalsep=(
      [pt_BR]=,
      [en_US]=.
      [POSIX]=.
          [C]=.
    )

    LC_ALL=$lc printf -v razao "%'d" $((numerador/divisor))
    razao+=${decimalsep[${lc%.*}]}

    while ((precisao-->0))
    do
       numerador=$((numerador%divisor))0
      razao+=$((numerador/divisor))
    done

    echo $razao >&2

  }



  razao-debug()
  {

    local uso numerador denominador precisao diferenca complemento razao

            uso=$'\e[10D'"Uso: $FUNCNAME <numerador> <nominador> [precisão]"
      numerador=${1?$uso}
    denominador=${2?$uso}
       precisao=${3:-5}

    diferenca=$((${#denominador}-${#numerador}))
    ((diferenca>0)) && printf -v complemento %0${diferenca}d

    razao=$complemento$((numerador*10**precisao/denominador ))

    echo -e "\nsh: $numerador / $denominador => $razao\n    $numerador: ${#numerador} dig\n    $denominador: ${#denominador} dig\n    #den-#num: $diferenca\n    complemento: $complemento\n"
    echo -n 'bc: '; bc <<< "scale=$precisao; $numerador/$denominador"
    echo -e "sh: ${razao:0:-precisao}.${razao: -precisao}\n"



    bm 2000 'bc<<<"scale=15;1/700"' 'razao 1 700 15'

    # [bash 5.2.37] built-in printf armazenando o resultado com printf -v
    #
    # bc<<<"scale=15;1/700"   00:04,073
    # razao 1 700 15  00:00,183
    #
    # 1º  razao 1 700 15
    # 2º  bc<<<"scale=15;1/700"   21,256 vezes (2.125,683%) mais lenta que razao 1 700 15

    # [bash 5.2.37] built-in printf armazenando o resultado com $()
    #
    # bc<<<"scale=15;1/700"   00:03,805
    # razao 1 700 15  00:01,761
    #
    # 1º  razao 1 700 15
    # 2º  bc<<<"scale=15;1/700"    1,160 vez (116,070%) mais lenta que razao 1 700 15

    # [bash 5.2.32] /bin/printf
    #
    # bc<<<"scale=15;1/700"   00:03,562
    # razao 1 700 15  00:04,185
    #
    # 1º  bc<<<"scale=15;1/700"
    # 2º  razao 1 700 15           0,174 vezes (17,490%) mais lenta que bc<<<"scale=15;1/700"

  }



# EOF
