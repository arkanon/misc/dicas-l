<!--
  2024/08/28 (Wed) 08:01:53 -03
-->



[[_TOC_]]



# Traducao Automatica de Conjuntos de Caracteres

***Colaboração: Paulo Roberto Bagatini***

***Data de Publicação:***



## Subtitle

Text
same line text  
new line text:
```
Text↵
same line text  ↵
new line text:
```

Unordered list (indent to subitem)
- *italic*
- **bold**
- ***italic bold***
- `preformatted`
- [anchor](http://site.com)
- ![alt](image.png "title"){width=400px}

Ordered list:
1. something
1. other thing
1. and this
```
1. something
1. other thing
1. and this
```

Code (prefer "bash" over "shell-session" type, for `t2t` translation):
```bash
$ echo command
# comment
```



## Referências
- http://wikipedia.org/wiki/character_encoding
- http://wikipedia.org/wiki/category:character_sets
- http://wikipedia.org/wiki/list_of_information_system_character_sets
- http://wikipedia.org/wiki/list_of_unicode_characters
- http://wikipedia.org/wiki/ascii
- http://wikipedia.org/wiki/extended_ascii
- http://wikipedia.org/wiki/iso/iec_8859
- http://wikipedia.org/wiki/iso/iec_8859-1
- http://wikipedia.org/wiki/latin-1_supplement
- http://wikipedia.org/wiki/utf-8
- http://wikipedia.org/wiki/unicode



## Comandos usados nesse artigo

- [iconv](   http://man.archlinux.org/man/iconv.1)
- [bash](    http://man.archlinux.org/man/bash.1):
    [echo](  http://man.archlinux.org/man/bash.1#echo%7e2),
    [printf](http://man.archlinux.org/man/bash.1#printf)



☐
