
Uma coisa que reparei um tempo atrás, e aparentemente ainda não se comenta muito nos círculos sociais nerds por aí, hehe: o comando printf (só agora?) tem um código de formatação para conversão de timestamp em data 🤓 Isso, associado aos fatos dele ser builtin e ter a capacidade de atribuir sua saída à uma variável, não apenas nos libera da necessidade de usar o comando date e seu respectivo subshell para captura do resultado como, quando for o caso, o torna a melhor e mais rápida opção:

# timestamp atual
$ printf "%(%s)T\n"
1666218519

# hora atual (sem valor explícito de timestamp ou com valor -1)
$ printf "%(%Y/%m/%d %a %H:%M:%S %Z)T\n"
2022/10/19 qua 19:29:50 -03

# hora específica (com o timestamp do momento desejado)
$ LC_ALL=C printf "%(%Y/%m/%d %a %H:%M:%S %Z)T\n" 1666666666
2022/10/24 Mon 23:57:46 -03

# salvando a saída em uma variável
$ TZ=UTF printf -v data "%(%Y/%m/%d %a %H:%M:%S %Z)T\n" 1666666666
$ echo $data
2022/10/25 ter 02:57:46 UTF


Eficiência:

. <(curl -sL bit.ly/benshmark-v3)
alias bm=benshmark-v3

# sem armazenar a saída em variável (impressão direta na tela)
s1(){ LC_ALL=C TZ=UTC date    +'%Y/%m/%d %a %H:%M:%S.%3N %Z'; }
s2(){ LC_ALL=C TZ=UTC printf '%(%Y/%m/%d %a %H:%M:%S.%3N %Z)T\n'; }

$ s1; s2
2022/10/19 Wed 22:57:43.144 UTC
2022/10/19 Wed 22:57:43.%3N UTC
# infelizmente o printf (ainda?) não reconhece o código para captura de ns

$ bm 2000 s1 s2
s1  00:02,115
s2  00:00,226

1º  s2
2º  s1/s2       8,358 vezes mais lenta

# armazenando a saída para variável (subshell no date)
s1(){ local dt=$(LC_ALL=C TZ=UTC date +'%Y/%m/%d %a %H:%M:%S.%3N %Z'); echo $dt; }
s2(){ local dt; LC_ALL=C TZ=UTC printf -v dt '%(%Y/%m/%d %a %H:%M:%S.%3N %Z)T\n'; echo $dt; }

$ s1; s2
2022/10/19 Wed 23:26:02.413 UTC
2022/10/19 Wed 23:26:02.%3N UTC

$ bm 2000 s1 s2
s1  00:02,912
s2  00:00,245

1º  s2
2º  s1/s2      10,885 vezes mais lenta


Como o date ainda é a forma mais padrão de converter data em timestamp e operar com elas (soma, subtração, hoje, ontem, etc), obviamente não perdeu sua serventia.

