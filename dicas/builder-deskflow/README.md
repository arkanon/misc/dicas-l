<!--
  2024/10/05 (Sat) 04:05:09 -03
  2024/10/04 (Fri) 23:55:55 -03
  2024/09/05 (Thu) 18:25:00 -03
  2024/08/20 (Tue) 21:49:09 -03
  2020/07/07 (Tue) 01:17:21 -03
  2017/11/15 (Wed) 00:45:11 -02
-->



[[_TOC_]]



# Synergy agora é DeskFlow

***Colaboração: Paulo Roberto Bagatini***

***Data de Publicação:***



## Compilação
```shell-session
mkdir -p /export/src/git/github.com
cd /export/src/git/github.com

podman rm  -ft0 build
podman run -di -v $PWD:/src -w /src --name build ubuntu:24.04
podman exec -ti build bash

export DEBIAN_FRONTEND=noninteractive

apt update
apt install -y git{,-lfs} python3

[[ -d deskflow ]] || time git clone http://github.com/deskflow/deskflow # 0m12.370s 109M

cd deskflow

time scripts/install_deps.py # 6m55.636s 167M
# Run-time dependency cli11 found: NO (tried pkgconfig and cmake)

rm -r build
time cmake -B build --preset=linux-release # 0m7.964s
# libei     >= 1.3 not found
# libportal >= 0.8 not found

time cmake --build build -j8 # 2m58.529s 210M
exit

time ./build/bin/unittests   # 0m1,281s

sudo apt install -y libqt6dbus6t64 libqt6widgets6t64 libxkbfile1 libnotify4

cd build/bin
du -chs # 12M
strip *
du -chs # 8,9M
./deskflow-server --version | head -n1 # deskflow-server v1.18.0, protocol v1.8
./deskflow-server --help
./deskflow-client --help
./deskflow

ver=$(./deskflow-server --version | grep -oP 'v\K[^ ]+(?=,)')

sudo chown -R $USER: /export/app
mkdir -p    /export/app/deskflow/$ver
cp -a desk* /export/app/deskflow/$ver
ln -s $ver  /export/app/deskflow/default
ln -s       /export/app/deskflow/default/deskflow{,-server,-client} /fs/bin
ls -la /fs/bin/deskflow*
```



## Atalho para o aplicativo gráfico
```shell-session
curl -s https://raw.githubusercontent.com/deskflow/deskflow-artwork/refs/heads/main/icon/deskflow-icon-pad.svg > /fs/share/icons/deskflow.svg

# cat << EOT | sudo tee /usr/share/applications/deskflow.desktop
  cat << EOT > /fs/share/applications/deskflow.desktop
[Desktop Entry]
Version=1.0
Name=DeskFlow
Exec=/fs/bin/deskflow
Terminal=false
X-MultipleArgs=false
Type=Application
Icon=/fs/share/icons/deskflow.svg
Categories=Network;
StartupNotify=true
EOT
```



## Servidor (XUbuntu 24.04)

Arquivo de configuração:
```shell-session
 server=server
lclient=lclient
rclient=rclient

cat << EOT > /fs/etc/deskflow.conf
section: screens
  $lclient:
  $server:
  $rclient:
end
section: links
  $lclient:
    right = $server
  $server:
    left  = $lclient
    right = $rclient
  $rclient:
    left  = $server
end
EOT

deskflow-server -c /fs/etc/deskflow.conf
```

Defaults do arquivo de configuração:
```
section: screens
  $host:
    halfDuplexCapsLock     = false
    halfDuplexNumLock      = false
    halfDuplexScrollLock   = false
    xtestIsXineramaUnaware = false
    switchCorners          = none
    switchCornerSize       = 0
end

section: links
end

section: aliases
end

section: options
  relativeMouseMoves   = false
  win32KeepForeground  = false
  disableLockToScreen  = false
  clipboardSharing     = true
  clipboardSharingSize = 3072
  switchCorners        = none
  switchCornerSize     = 0
end
```

Certificado:
```shell-session
pem=/fs/etc/deskflow.pem

openssl req -nodes -newkey rsa:2048 -x509 -subj /CN=Deskflow -days 365 -keyout - -out - 2>&- > $pem

openssl x509 -in $pem -noout -text
# Certificate:
#     Data:
#         Version: 3 (0x2)
#         Serial Number:
#             05:62:af:27:1a:91:b2:1f:da:26:3e:6e:95:d2:f2:67:33:00:d2:df
#         Signature Algorithm: sha256WithRSAEncryption
#         Issuer: CN = Deskflow
#         Validity
#             Not Before: Oct  5 05:05:26 2024 GMT
#             Not After : Oct  5 05:05:26 2025 GMT
#         Subject: CN = Deskflow
#         Subject Public Key Info:
#             Public Key Algorithm: rsaEncryption
#                 Public-Key: (2048 bit)

openssl x509 -in $pem -noout -fingerprint -sha256 | cut -d= -f2 > ${pem%.*}.fp
# 1E:2F:27:86:73:1B:BF:CE:E6:94:8A:06:B2:AE:5E:EA:CB:1E:CD:3B:57:84:58:49:47:D5:11:16:D4:72:54:EB
```

Executar o servidor na tela de login:
```shell-session
cat << EOT | sudo tee /etc/lightdm/lightdm.conf
[Seat:*]
autologin-user=

[SeatDefaults]
greeter-setup-script=/fs/bin/deskflow-server -n server -c /fs/etc/deskflow.conf --enable-crypto --tls-cert /fs/etc/deskflow.pem
EOT

sudo systemctl restart lightdm

ps -ef | grep [d]eskflow
```



## Cliente (Ubuntu 24.04)

Forçar a tela de login a usar o Xorg:
```shell-session
sudo sed -r 's/.*(WaylandEnable=).*/\1false/' /etc/gdm3/custom.conf
```

Colocar a fingerprint do certificado no homes dos usuários `gdm` e `root`:
```shell-session
for i in root gdm
{
  eval sudo mkdir -p ~$i/.deskflow/SSL/Fingerprints
  eval sudo ln -fs /fs/etc/deskflow.fp ~$i/.deskflow/SSL/Fingerprints/TrustedServers.txt
  eval sudo chown -R $i: ~$i/.deskflow
}
```

Criar um script que inicia o cliente deskflow apropriadamente conforme o usuário:
```shell-session
cat << \EOT > /fs/bin/deskflow-client.sh
#!/bin/bash

  server=192.168.0.102

  if [[ $USER != gdm ]]
  then
    export    DISPLAY=:1
    export XAUTHORITY=/run/user/$(id -u $USER)/gdm/Xauthority
    killall deskflow-client
  fi

# declare -p > /tmp/env-$USER

  /fs/bin/deskflow-client -n rclient --sync-language --enable-crypto $server

EOT

chmod +x /fs/bin/deskflow-client.sh
```

Executar o script do cliente na tela de login:
```shell-session
# cat << EOT | sudo tee /usr/share/gdm/greeter/autostart/deskflow-client.desktop
  cat << EOT > /fs/share/applications/deskflow-client.desktop
[Desktop Entry]
Type=Application
Name=DeskFlow Client
Exec=/fs/bin/deskflow-client.sh
X-GNOME-AutoRestart=true
EOT

sudo ln -fs /fs/share/applications/deskflow-client.desktop /usr/share/gdm/greeter/autostart/deskflow-client.desktop
```

Executar o script do cliente na sessão do usuário logado (após o login no Ubuntu, haverá um certo delay até a conexão se reestabelecer):
```shell-session
# cat << EOT | sudo tee /etc/gdm3/PostLogin/Default
  cat << EOT >       /fs/etc/gdm3/PostLogin/Default
#!/bin/bash
/fs/bin/deskflow-client.sh
EOT

chmod +x    /fs/etc/gdm3/PostLogin/Default
sudo ln -fs /fs/etc/gdm3/PostLogin/Default /etc/gdm3/PostLogin/Default
```

Reiniciar o GDM:
```shell-session
sudo systemctl restart gdm
ps -ef | grep [d]eskflow
```



## Resolução de problemas
Tanto no servidor quanto nos clientes, o `syslog` pode ser analizado para resolver eventuais problemas de conexão:
```shell-session
sudo tail -f /var/log/syslog | grep -i deskflow
```



## Referências
- http://github.com/deskflow/deskflow/tree/v2-dev
<!-- -->
- http://github.com/deskflow/deskflow/wiki/Getting-Started
- http://github.com/deskflow/deskflow/wiki/Compiling#ubuntu-1604-and-up
- http://github.com/deskflow/deskflow/wiki/Compiling#Linux
<!-- -->
- http://digitalocean.com/community/tutorials/openssl-essentials-working-with-ssl-certificates-private-keys-and-csrs



## Comandos usados nesse artigo

- [bash](     http://man.archlinux.org/man/bash.1):
    [cd](     http://man.archlinux.org/man/bash.1#cd),
    [declare](http://man.archlinux.org/man/bash.1#declare),
    [eval](   http://man.archlinux.org/man/bash.1#eval),
    [exit](   http://man.archlinux.org/man/bash.1#exit),
    [export]( http://man.archlinux.org/man/bash.1#export),
    [for](    http://man.archlinux.org/man/bash.1#for),
    [if](     http://man.archlinux.org/man/bash.1#if),
    [test](   http://man.archlinux.org/man/bash.1#test),
    [time](   http://man.archlinux.org/man/bash.1#Pipelines)
- [apt](      http://man.archlinux.org/man/apt.8)
- [cat](      http://man.archlinux.org/man/cat.1)
- [chmod](    http://man.archlinux.org/man/chmod.1)
- [chown](    http://man.archlinux.org/man/chown.1)
- [cmake](    http://man.archlinux.org/man/cmake.1)
- [cp](       http://man.archlinux.org/man/cp.1)
- [curl](     http://man.archlinux.org/man/curl.1)
- [cut](      http://man.archlinux.org/man/cut.1)
- [git](      http://man.archlinux.org/man/git.1)
- [grep](     http://man.archlinux.org/man/grep.1)
- [killall](  http://man.archlinux.org/man/killall.1)
- [ln](       http://man.archlinux.org/man/ln.1)
- [ls](       http://man.archlinux.org/man/ls.1)
- [mkdir](    http://man.archlinux.org/man/mkdir.1)
- [openssl](  http://man.archlinux.org/man/openssl.1)
- [podman](   http://man.archlinux.org/man/podman.1)
- [ps](       http://man.archlinux.org/man/ps.1)
- [rm](       http://man.archlinux.org/man/rm.1)
- [sed](      http://man.archlinux.org/man/sed.1)
- [sudo](     http://man.archlinux.org/man/sudo.8)
- [systemctl](http://man.archlinux.org/man/systemctl.1)
- [tail](     http://man.archlinux.org/man/tail.1)
- [tee](      http://man.archlinux.org/man/tee.1)



☐
