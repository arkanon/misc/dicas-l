#!/bin/bash

# xcf2jpg.sh

# Arkanon <arkanon@lsd.org.br>
# 2020/11/15 (Sun) 17:49:01 -03

# <http://stackoverflow.com/a/28364748>
# <http://billauer.co.il/blog/2009/07/gimp-xcf-jpg-jpeg-convert-bash-script>

  gimp -ib - << EOT

    (
      define (convert-xcf-to-jpeg filename outfile)
      (
        let*
        (
          ( image    ( car ( gimp-file-load RUN-NONINTERACTIVE filename filename ) ) )
          ( drawable ( car ( gimp-image-merge-visible-layers image CLIP-TO-IMAGE ) ) )
        )
        ( file-jpeg-save RUN-NONINTERACTIVE image drawable outfile outfile .9 0 0 0 " " 0 1 0 1 )
        ( gimp-image-delete image ) ; or the memory will explode
      )
    )

    ( gimp-message-set-handler 2 ) ; messages to standard error

    ( gimp-message        "$1" )
    ( convert-xcf-to-jpeg "$1" "${1%%.xcf}.jpg" )
    ( gimp-quit 0 )"

EOT

# EOF
