# _Shebang_ e Outros Paranauês

***Colaboração: Paulo Roberto Bagatini***

***Data de Publicação:***



```bash
#!/bin/sh
#!/bin/bash
#!/usr/bin/bash
#!/usr/local/bin/bash
#!/usr/bin/env bash
```



```bash
java --version
# openjdk 21.0.3 2024-04-16

url=https://github.com/Teradata/jaqy/releases/download/v1.2.0/jaqy-1.2.0.jar
{ echo '#!/usr/bin/env -S java -jar'; curl -sL $url; } > jaqy

head -n1 jaqy
# #!/usr/bin/env -S java -jar

chmod +x jaqy

./jaqy -v
# Jaqy Console 1.2.0

jar tvf  jaqy META-INF/MANIFEST.MF
#    351 Tue Aug 10 22:03:36 BRT 2021 META-INF/MANIFEST.MF

unzip -p jaqy META-INF/MANIFEST.MF
# warning [jaqy]:  28 extra bytes at beginning or within zipfile
#   (attempting to process anyway)
# Manifest-Version: 1.0
# Implementation-Title: Jaqy Console
```



```bash
url=https://gitlab.com/arkanon/misc/show_tz.java/-/raw/master/show_tz.java
{ echo '#!/usr/bin/env -S java --source 19'; curl -sL $url; } > show_tz
chmod +x ./show_tz

./show_tz
# Versão do Java     : 21.0.3
# Class file         : /media/5tb-data/src/git/gitlab.com/dicas-l/dicas/shebang_e_suas_maravilhas/testes/./show_tz
# Compatibilidade    : 12130 (Java SE 12086)
# ...
```



```bash
url=https://gitlab.com/arkanon/misc/show_tz.java/-/raw/master/show_tz.class

{ echo '#!/usr/bin/env java'; curl -sL $url; } > show_tz
chmod +x ./show_tz
./show_tz
```



Acessar http://oldversion.com/games/mine-sweeper-xp-6-1 e salvar o arquivo 6.1_MinesweeperXP.exe. Então:
```bash
7z l 6.1_MinesweeperXP.exe
#    Date      Time    Attr         Size   Compressed  Name
# ------------------- ----- ------------ ------------  ------------------------
# 2001-08-23 08:00:00 ....A       119808       135800  Minesweeper XP/winmine.exe
# 2011-10-14 11:47:29 ....A       176128               Minesweeper XP/winmm.dll
# 2012-02-27 00:57:21 D....            0            0  Minesweeper XP
# ------------------- ----- ------------ ------------  ------------------------
# 2012-02-27 00:57:21             295936       135800  2 files, 1 folders

7z e 6.1_MinesweeperXP.exe */*
# Everything is Ok
#
# Files: 2
# Size:       295936
# Compressed: 298843

wine --version
# wine-9.12 (Staging)

WINEDLLOVERRIDES=mscoree,mshtml= WINEPREFIX=~/.winmine wine winmine.exe
du -hs ~/.winmine
# 1,3G    ~/.winmine

{ echo '#!/usr/bin/env -S WINEDLLOVERRIDES=mscoree,mshtml= WINEPREFIX=~/.winmine wine'; } > winmine
chmod +x winmine
./winmine
# wine: invalid directory ~/.winmine in WINEPREFIX: not an absolute path

{ echo '#!/usr/bin/env -S WINEDLLOVERRIDES=mscoree,mshtml= WINEPREFIX=$HOME/.winmine wine'; } > winmine
./winmine
# /usr/bin/env: Só há suporte à expansão ${VARNAME}, erro em: $HOME/.winmine wine

{ echo '#!/usr/bin/env -S WINEDLLOVERRIDES=mscoree,mshtml= WINEPREFIX=${HOME}/.winmine wine'; } > winmine
./winmine

ps -ef | grep -i [w]in
WINEPREFIX=${HOME}/.winmine wineserver --kill

rm 6.1_MinesweeperXP.exe
```



```bash
{ echo -e '#!/usr/bin/env bash\ntail -n+3 $0 | display; exit'; cat bash.svg; } > bash-logo
head -n+2 bash-logo
# #!/usr/bin/env bash
# tail -n+3 $0 | display; exit
chmod +x bash-logo
./bash-logo

source=inventario.png
script=${source%.*}
autoex='#!/usr/bin/env bash\ntail -n+3 $0 | display; exit'
{ echo -e "$autoex"; cat $source; } > $script
chmod +x $script
./$script
```



## Referências
- [Wikipedia](http://wikipedia.org/wiki/Shebang_%28Unix%29)
- [How to use shebang #! in Bash](http://byby.dev/bash-shebang)
- [Shebang, Hebang and Webang](http://dev.to/iamthecarisma/shebang-hebang-and-webang-lines-bang-lines-for-compiled-source-files-4eb9)



## Comandos usados nesse artigo

-  [7z](        http://man.archlinux.org/man/7z.1)
-  [bash](      http://man.archlinux.org/man/bash.1):
      [echo](   http://man.archlinux.org/man/bash.1#echo%7e2),
      [exit](   http://man.archlinux.org/man/bash.1#exit)
-  [cat](       http://man.archlinux.org/man/cat.1)
-  [chmod](     http://man.archlinux.org/man/chmod.1)
-  [curl](      http://man.archlinux.org/man/curl.1)
-  [display](   http://man.archlinux.org/man/display.1)
-  [du](        http://man.archlinux.org/man/du.1)
-  [env](       http://man.archlinux.org/man/env.1)
-  [grep](      http://man.archlinux.org/man/grep.1)
-  [head](      http://man.archlinux.org/man/head.1)
-  [jar](       http://man.archlinux.org/man/jar-openjdk22.1)
-  [java](      http://man.archlinux.org/man/java-openjdk22.1)
-  [ps](        http://man.archlinux.org/man/ps.1)
-  [rm](        http://man.archlinux.org/man/rm.1)
-  [sudo](      http://man.archlinux.org/man/sudo.8)
-  [tail](      http://man.archlinux.org/man/tail.1)
-  [tee](       http://man.archlinux.org/man/tee.1)
-  [unzip](     http://man.archlinux.org/man/unzip.1)
-  [wine](      http://man.archlinux.org/man/wine.1)
-  [wineserver](http://man.archlinux.org/man/wineserver.1)



☐
