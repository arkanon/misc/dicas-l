# [Artigos publicados](https://www.dicas-l.com.br/autores/paulorobertobagatini.php) na Dicas-L

As dicas devem ser enviadas preferencialmente em formato `t2t` (txt2tags), linguagem de marcação originalmente criada por
[Aurelio Marinho Jargas](https://aurelio.net/about.html) em 2001 e atualmente mantida por [Florent Gallaire](https://github.com/fgallaire) (v2) e
[Jendrik Seipp](https://github.com/jendrikseipp) (v3) em https://txt2tags.org.

Para automatizar a conversão do [Markdown](https://markdownguide.org) usado na documentação do GitLab para `t2t` e `html` no layout do site https://www.dicas-l.com.br,
criei o script [md2t2t.sh](../md2t2t) que executa adequadamente esse trabalho.

☐
