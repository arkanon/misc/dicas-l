<!--
  2025/01/16 (Thu) 01:42:35 -03
  2025/01/14 (Tue) 02:33:57 -03
  2025/01/02 (Thu) 09:29:43 -03
  2025/01/01 (Wed) 07:02:49 -03
  2024/12/31 (Tue) 21:08:03 -03
  2024/12/31 (Tue) 01:53:43 -03
  2024/06/24 (Mon) 21:53:42 -03
-->



[[_TOC_]]



## Compilando código fonte em _container_



### Motivação

O conceito de _container_ não é nenhuma novidade, principalmente entre os círculos sociais mais nerds. Mesmo o Windows
([pasmem!](https://learn.microsoft.com/pt-br/virtualization/windowscontainers/about/))
já possui suporte a esse recurso. O que talvez ainda mereça evoluir é a percepção do usuário de que conteinerização tem seu valor **também** em um
_desktop_.

_Containers_:
- encapsulam uma aplicação com dependências em grande quantidade ou ausentes dos repositórios da distribuição e/ou que dificilmente serão usadas por
  outras aplicações, permitindo um compartilhamento bastante simplificado;
- evitam conflitos em binários compilados contra bibliotecas de versão incompatível com alguma característica da distribuição instalada no _host_
  (como [glibc](https://en-m-wikipedia-org.translate.goog/wiki/Glibc?_x_tr_sl=auto&_x_tr_tl=pt) x [musl](https://en-m-wikipedia-org.translate.goog/wiki/Musl?_x_tr_sl=auto&_x_tr_tl=pt)
  e arquitetura de 64 x 32 bits, por exemplo).
Nesse sentido, seu uso pode ser encarado como uma alternativa aos sistemas de empacotamento
[snap, flatpak e appImage](https://medium-com.translate.goog/@journalehsan/snap-flatpak-and-appimage-which-one-is-better-dc36f7ff1720?_x_tr_sl=auto&_x_tr_tl=pt).
Mas, diferentemente destes últimos, em um _container_ é fácil a interação manual no desenvolvimento e na execução do processo, mesmo que normalmente isso
seja feito de forma automatizada. Como a aplicação contida não é naturalmente integrada ao ambiente gráfico de trabalho do usuário da mesma forma
transparente que esses sistemas de empacotamento, acaba exigindo um pouco de "mão na massa".

Existe todo um rol de vantagens técnicas na adoção de _containers_. Isolamento de processos, granularidade do consumo de recursos e simplificação da
implantação de ambientes em [CI/CD](https://redhat.com/pt-br/topics/devops/what-is-ci-cd), só para citar algumas. Mas não serão elas o foco desse artigo;
há mais uma aplicação à qual _containers_ servem como uma luva: a compilação de código fonte.

É verdade que atualmente as distribuições Linux vêm carregadas com opções pré-compiladas de uma infinidade de aplicações das mais variadas áreas do
conhecimento. O que por vezes acontece é que justamente a versão que **estamos usando** da **nossa** querida distribuição não possui a versão adequada
(ou seja, a última :-p) de determinada aplicação, porque foi publicada ontem ou porque falta mão de obra ou interesse entre os mantenedores da
distribuição ou simplesmente devido à sua política de atualização. Aquela versão nova da aplicação que mais usamos, que implementou justamente aquela
características matadora que vai mudar nossa vida só estará disponível a curto prazo se alguma boa alma de plantão a compilar e disponibilizar o pacote
em algum repositório adicional. A alternativa é rezar para que a próxima edição da distro venha atualizada também nesse quesito. Lamentável...

Bom. Quem não tem sangue de barata para aguentar tamanha dependência, pode fazer sua própria compilação do código fonte: até isso hoje em dia é trivial
no Linux, ainda mais se comparado com a dificuldade de 20 anos atrás.

A adoção de _containers_ também para esse fim traz apenas vantagens em relação à compilação no _host_ ou mesmo em VM's:

- o instanciamento de um _container_ é ridiculamente simples e rápido;
- é possível adotar basicamente qualquer versão de qualquer distribuição;
- garante que o procedimento não sofrerá interferência de customizações já aplicadas à instalação do _host_;
- não gera "poluição" no host com compiladores e pacotes de desenvolvimento, já que a instalação do _desktop_ possivelmente não tem esse objetivo;
- deixa evidentes as dependências envolvidas no processo, facilitando o processo de documentação e replicação.
  Containers de distribuições costumam vir com o mínimo absoluto que os deixe utilizáveis, mas nada impede a adoção de imagens especializadas;
- a integração de _containers_ com o _host_ é muito mais simples e transparentemente que a de VM's;
- exigem muito menos recursos de RAM e CPU;
- permitem também a execução de aplicações gráficas, inclusive de ambientes gráficos, se o _container_ for adequadamente executado e integrado com
  o ambiente gráfico do _host_;
- é trivial a execução automatizada de uma quantidade/sequência arbitrária de _containers_ executando procedimentos também automatizados;
- é possível salvar imagens intermediárias durante o processo de customização, para que sejam usadas como pontos de partida para outros fins;
- simplifica uma eventual criação de pacote com o binário resultante, compatível com a versão da distribuiçao que foi executada no _container_.



### Possibilidades

O Maravilhoso Mundo Encantado dos _Containers_ só é possível porque a
[versão 2.4.19 do Linux, lançada em 2002](http://wikipedia.org/wiki/linux_namespaces#History),
introduziu suporte ao conceito de
_[namespace](https://www-redhat-com.translate.goog/en/blog/7-linux-namespaces?_x_tr_sl=auto&_x_tr_tl=pt)_.
Com base nisso, 10 anos depois, o [kernel 3.8](http://lwn.net/Articles/491236) passou a suportar o conceito de
_[container](https://www.redhat.com/pt-br/topics/containers/whats-a-linux-container)_.
Um _container_ difere de uma máquina virtual por não executar concomitantemente outro sistema operacional, mas sim processos "azeite" controlados pelo
mesmo _kernel_ do _host_ e que "não se misturam" graças aos uso dos _namespaces_. Para simplificar a criação e manutenção de _containers_, criou-se mais
uma camada, chamada "mecanismo (ou motor) de _container_" (_container engine_). É nessa camada que normalmente atuamos quando criamos, modificamos,
exportamos e matamos _containers_.

Dentre os vários mecanismos, o
[docker](https://docs-docker-com.translate.goog/get-started/docker-overview/?_x_tr_sl=auto&_x_tr_tl=pt)
sem dúvida é o mais popular, mas há outros. Alguns se destacam em segurança, outros em economia de recurso, outros em simplicidade de execução e, claro,
alguns são voltados especificamente para o uso em nuvem:

- [podman](https://docs-podman-io.translate.goog/en/latest/?_x_tr_sl=auto&_x_tr_tl=pt)
- [containerd](https://github-com.translate.goog/containerd/containerd/blob/main/README.md?_x_tr_sl=auto&_x_tr_tl=pt)
- [CRI-O](https://cri--o-io.translate.goog/?_x_tr_sl=auto&_x_tr_tl=pt#what-is-cri-o)
- [LXC](https://linuxcontainers-org.translate.goog/lxc/introduction/?_x_tr_sl=auto&_x_tr_tl=pt)
- [runc](https://docs.redhat.com/pt-br/documentation/red_hat_enterprise_linux/8/html/building_running_and_managing_containers/_runc)
- [rkt](https://redhat.com/pt-br/topics/containers/what-is-rkt)



### A adoção do `podman`

Neste artigo, a adoção do `podman` em detrimento do `docker` deve-se às características interessantes do primeiro para o uso proposto. Um _container_
`podman` é executado sem necessidade de um _daemon_ ou de privilégios de _root_, deixando-o mais simples e mais seguro. Sendo executado no espaço do usuário,
um diretório do _host_ compartilhado como volume para o _container_ terá seu conteúdo gravado pelo _root_ do _container_ automaticamente pertencente ao
usuário que o executou, o que torna transparente o intercâmbio entre o que é feito do lado do _host_ e o que é feito do lado do _container_.
Evidentemente nada impede o uso do docker, até porque a sintaxe dos dois mecanismos é idêntica, mas então é possível que alguns passos a mais tornem-se
necessários, especialmente no que diz respeito ao permissionamento.

O binário resultante da compilação no _container_ fica diretamente disponível ao _host_ no diretório do código fonte que foi repassado como volume para o
_container_. Pode ser executado de dentro do _container_, ou a partir do _host_ se o _container_ executou uma distro compatível. Na sequência, é possível
empacotá-lo no formato de pacote utilizado no _container_. A execução de diferentes distros em diferentes versões montando esse mesmo diretório permite
originar um pacote para cada combinação apenas recompilando o código fonte para adequar os binários às versões de dependências da distro
em questão.

Existem outras possibilidades para dar destino ao que foi feito no container, como salvar o _layer_ contendo as modificações em uma imagem separada da
imagem original, por exemplo. Mas o foco desta dica é tratar o _container_ como um ajudante invisível, então não vamos explorar o que pode ser feito no
universo dele.



### Instalação e configuração do mecanismo

O `podman` está disponível em todos os principais sistemas operacionais (Linux, Mac, Windows). No Linux, para praticamente todas as distribuições e
arquiteturas, incluindo ARM. Aqui é usado um Ubuntu Desktop 24.10 como _host_, mas a [página de instalação do podman](https://podman.io/docs/installation)
explica como instalá-lo "no resto" das opções. Assim como o docker, possui sua interface gráfica, o [Podman Desktop](https://podman-desktop.io/).
Curiosamente, a [versão para Linux](https://podman-desktop.io/downloads/linux) dele é distribuída em um flatpak, não em um container. Enfim... :-p

```bash
sudo apt install -y podman

podman --version
# podman version 5.0.3

sudo touch     /etc/sub{u,g}id
sudo usermod --add-subuids 100000-165535 --add-subgids 100000-165535 $USER
sudo chmod 644 /etc/sub{u,g}id
ls -l /etc/sub{u,g}id
cat   /etc/sub{u,g}id
podman system migrate

ls -la     /usr/share/containers/
ls -la           /etc/containers/
ls -la ~/.local/share/containers/
ls -la      ~/.config/containers/
```



### Configuração do _container_

```bash
cd /export/src/git

pasock=/tmp/container-pa.socket

[[ -e $pasock ]] || pactl load-module module-native-protocol-unix socket=$pasock

pactl info
ls -l --color $pasock
systemctl --no-pager --user status pulseaudio

args=(

  --rm
  --tty
  --interactive

  --workdir /build

  --env              TZ=America/Sao_Paulo
  --env          LC_ALL=pt_BR.utf8
  --env            LANG=pt_BR.utf8
  --env        LANGUAGE=pt_BR.utf8
  --env DEBIAN_FRONTEND=noninteractive

  --volume $PWD:/build

  # integração do container com o ambiente gráfico do host
  --env    DISPLAY
  --volume /tmp/.X11-unix:/tmp/.X11-unix

  # integração do container do o pulse áudio do host
  --env    PULSE_SERVER=unix:/tmp/pulseaudio.socket
  --volume $pasock:$pasock

)
```



### Execução do _container_

Como o `podman` pretende ser 100% compatível com o docker, ele usa os mesmos repositórios de imagem que ele, incluindo, portanto, o
[Docker Hub](https://hub.docker.com/). Os arquivos de configuração de repositórios ficam armazenados em `/etc/containers/registries.conf.d/`.

A ideia inicial da dica é executar uma imagem no podmam com uma versão de distribuição **idêntica** à do _host_. Por isso, começamos importando para a
sessão shell o conteúdo do arquivo `/etc/os-release` com o comando `source`. Das variáveis criadas, vão nos interessar especificamente `ID` e `VERSION_ID`.

`ID` contém o nome da distribuição (ubuntu, alpine, fedora, etc) que é o nome que se costuma das às imagens. Eventualmente a _string_ em `ID` não é igual
ao nome da imagem. Se você usa o Rocky Linux, por exemplo, `ID` contém simplesmente `rocky`, mas a imagem no docker hub chama-se `rockylinux`. Para ser
inicialmente "puxada" do repositório (_pull_), será preciso identificar o caminho completo da imagem: `docker.io/rockylinux/rockylinux`. Tendo a cópia
local, basta uma referência ao nome para instanciá-la (_run_): `rockylinux`. Mas ainda assim, uma _string_ diferente da contida em `ID`.

`VERSION_ID` é o número da versão da distribuição. Quando instanciamos uma imagem, podemos nos referir à ela com uma _tag_ junto ao nome. A _tag_
normalmente armazena a _string_ de versão da imagem, mas pode ser usada para caracterizá-la da forma que for desejada. Se nenhuma _tag_ é especificada,
o mecanismo assume a _tag_ _latest_, que aponta para a versão _default_. Em imagens de distros, a versão _default_ costuma ser a última LTS, quando há,
como é o caso do Ubuntu.

```bash
. /etc/os-release

podman pull ubuntu:$VERSION_ID

podman images | sort

podman run "${args[@]}" ubuntu:$VERSION_ID bash

podman ps -as
# CONTAINER ID  IMAGE                           COMMAND  CREATED        STATUS        PORTS  NAMES          SIZE
# f3a7924cf406  docker.io/library/ubuntu:24.10  bash     2 seconds ago  Up 3 seconds         sad_engelbart  11.7kB (virtual 82.6MB)
```



### Customização

```bash
{
  apt update
  apt install -y apt-utils
  apt install -y locales time wget curl apt-file file readline-common tzdata command-not-found bash-completion
  locale-gen $LC_ALL
  apt update # apt-file e command-not-found
  apt upgrade -y
}
exec bash  # readline e locale
```



### Exemplos de uso

Essa dica propõe-se a ser a base para as próximas, que trarão exemplos didáticos e mesmo lúdicos de uso desse ambiente, tanto em relação à compilação
propriamente dita, quanto à funcionalidade do resultado (as dicas em si).

Por hora, vamos apenas compilar a última versão do bash direto do forno (git).

```bash
apt install -y git git-lfs # 48,2 M
apt install -y build-essential bison gnulib lib{audit,ncurses}-dev # 487 M

mkdir -p git.savannah
cd git.savannah
[[ -d bash ]] || git clone https://git.savannah.gnu.org/git/bash
cd bash
git pull

make distclean

./configure | grep --line-buffered --color=always -wE '.+ no$'

make

bash --version | head -n1
# GNU bash, version 5.2.32(1)-release (x86_64-pc-linux-gnu)

./bash --version | head -n1
# GNU bash, version 5.2.37(1)-release (x86_64-pc-linux-gnu)

ls -gG bash
# -rwxr-xr-x 1 4674896 jan 16 01:13 bash

strip bash
ls -gG bash
# -rwxr-xr-x 1 1347704 jan 16 01:14 bash

podman ps -as
# CONTAINER ID  IMAGE                           COMMAND  CREATED         STATUS         PORTS  NAMES        SIZE
# c42704d178de  docker.io/library/ubuntu:24.10  bash     10 minutes ago  Up 10 minutes         crazy_gould  774MB (virtual 857MB)
```



### Referências adicionais

- http://snapcraft.io/about
- http://docs.flatpak.org/en/latest/introduction.html
- http://docs.appimage.org/introduction



### Polindo o processo

```bash
bashrc=/tmp/container-bashrc

cat << \EOT > $bashrc
  unset HISTCONTROL

  ls="LC_ALL= LC_TIME=C LC_NUMERIC=pt_BR.utf8 BLOCK_SIZE=\'1 ls -lapvT0 --quoting-style=shell --color=always --group-directories-first"

  alias  l="$ls -is --time-style='+%Y/%m/%d %a %T %:::z'" # --hyperlink
  alias ll="$ls -gG --time-style='+%Y/%m/%d %T'"
  alias  h="LC_TIME=C history"

  shopt -s checkwinsize
  shopt -u progcomp   # complete path without escape or expand variables
  shopt -s extglob    # recognize [?*+@!] extended pattern matching operators
# shopt -s globstar   # recognize "**" in pathname expansion context to match all files and zero or more directories and subdirectories
# shopt -s dotglob    # when referencing files and directories, include those beginning with ".", except for the "." and ".." directories themselves

  shopt -s cmdhist    # save all lines of a multiple-line command in the same history entry
  shopt -s histappend # append history lines to history file at exit, instead of overwritting the file
  shopt -s histreedit # give the opportunity to re-edit a failed history substitution
  shopt -s histverify # results of history substitution are not immediately passed to the shell parser, loading it to the buffer for further modification

  export           HISTTIMEFORMAT='%Y/%m/%d %a %T %z  '
  export              HISTCONTROL=             # reset behaviour to save duplicates and space started commands to history
  export                 HISTSIZE=             # unlimited history
  export             HISTFILESIZE=             # unlimited history file
  export           PROMPT_COMMAND='history -a' # append each command line to history file immediately after its execution (prevents loss of commands in multiple sessions)
  export command_oriented_history=1            # save all lines of a multiple-line command (eg: while/for loop) in a single history entry

  export   TIMEFORMAT=%lR
  export NO_AT_BRIDGE=1 # (Terminal:29743): dbind-WARNING **: 17:24:22.395: Couldn't connect to accessibility bus: Failed to connect to socket /run/user/10101/at-spi/bus_0: No such file or directory
  export         LESS=-RMSNXFJ

  unset  LC_ALL
  export LC_{ADDRESS,CTYPE,IDENTIFICATION,MEASUREMENT,MESSAGES,MONETARY,NAME,NUMERIC,PAPER,TELEPHONE,TIME}=pt_BR.utf8
  export  LC_COLLATE=C
  export LANG{,UAGE}=pt_BR.utf8

EOT
```



```bash
cd /export/src/git


     dir=build
  pasock=/tmp/container-pa.socket
histfile=/tmp/container-bash_history
    user=root
    home=/$user

printf -v tz '%(%z)T'
printf -v tz 'UTC%+05d\n' $(( -1*( ${tz::1}10#${tz:1:2} ) ))


[[ -e $pasock ]] || pactl load-module module-native-protocol-unix socket=$pasock


pactl info
ls -l --color $pasock
systemctl --no-pager --user status pulseaudio
> $histfile

args=(

  --rm
  --tty
  --interactive

  --workdir  /$dir
  --hostname  $dir
  --name      $dir

  --env              TZ=$tz # America/Sao_Paulo UTC+03 # $(</etc/timezone)
  --env            HOME=$home
  --env DEBIAN_FRONTEND=noninteractive

  --user $user

  --volume      $PWD:/$dir
  --volume   $bashrc:$home/.bashrc
  --volume $histfile:$home/.bash_history

  # integração do container com o ambiente gráfico do host
  --env    DISPLAY
  --volume /tmp/.X11-unix:/tmp/.X11-unix

  # integração do container do o pulse áudio do host
  --env    PULSE_SERVER=unix:/tmp/pulseaudio.socket
  --volume $pasock:$pasock

)
```



```bash
cat /etc/containers/registries.conf.d/shortnames.conf

declare -A distro=(

############ apt
    [ubuntu]=library/ubuntu
    [debian]=library/debian

############ apk
    [alpine]=library/alpine

############ dnf
    [fedora]=library/fedora
    [centos]=library/centos
      [alma]=library/almalinux
     [rocky]=rockylinux/rockylinux
    [redhat]=redhat/ubi9

############ pacman
      [arch]=archlinux/archlinux
   [manjaro]=manjarolinux/base
       [big]=talesam/biglinux-build # imagem não oficial e antiga

############ zypper
  [opensuse]=opensuse/tumbleweed

############ emerge
    [gentoo]=gentoo/stage3

)
```



```bash
for i in ${!distro[*]}
{
  echo -e "\n$i"
  podman run "${args[@]}" -e LC_ALL= docker.io/${distro[$i]} sh -c '
    . /etc/os-release
    case "$ID $ID_LIKE" in
      *debian* ) echo apt    ;; # ubuntu/debian
      *alpine* ) echo apk    ;; # alpine
      *fedora* ) echo dnf    ;; # fedora/centos/redhat/rocky/alma
      *arch*   ) echo pacman ;; # arch/manjaro/big
      *suse*   ) echo zypper ;; # opensuse
      *gentoo* ) echo emerge ;; # gentoo
      *        ) echo classe de distro não testada
    esac
  '
}; echo
```



```bash
d=ubuntu
d=debian
d=alpine
d=fedora
d=centos
d=redhat
d=rocky
d=alma
d=arch
d=manjaro
d=big
d=opensuse
d=gentoo

curl -s https://hub.docker.com/v2/repositories/${distro[$d]}/tags?page_size=100 | jq -r '.results[].name' | sort
```



```bash
podman images -n --format {{.Repository}}:{{.Tag}} > images
podman images -n --format {{.ID}} | sort -u > ids
podman rmi $(<ids)
podman rmi $(podman images -n --format {{.ID}} | sort -u)
rm -r ~/.local/share/containers/*
time for i in $(<images); { time podman pull $i; }
```



## Comandos mencionados nesse artigo

- [bash](   http://man.archlinux.org/man/bash.1):
    [cd](   http://man.archlinux.org/man/bash.1#cd)
- [apt](    http://man.archlinux.org/man/apt.8)
- [cat](    http://man.archlinux.org/man/cat.1)
- [docker]( http://man.archlinux.org/man/docker.1)
- [ls](     http://man.archlinux.org/man/ls.1)
- [mkdir](  http://man.archlinux.org/man/mkdir.1)
- [podman]( http://man.archlinux.org/man/podman.1)
- [sudo](   http://man.archlinux.org/man/sudo.8)
- [usermod](http://man.archlinux.org/man/usermod.8)



☐
