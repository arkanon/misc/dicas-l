#!/bin/bash

# marqif - Gerador de animações estilo Marquee em GIF animado a partir do corte
#          sequencial de uma imagem em frames tomados um determinado número de
#          pixels cada vez mais à direita.
#
# Arkanon <arkanon@lsd.org.br>
# 2020/11/22 (Sun) 19:21:51 -03
# 2013/03/11 (Seg) 03:55:05 -02

### parâmetros ###

### logo
    strip="dicas-l-big-still.gif"                # imagem a ser fatiada e transformada em marquee
  framesd="frames-$RANDOM"                       # diretório de armazenagem temporária dos frames
       w1=200                                    # largura padrão de cada frame
     incr=2                                      # incremento em pixels de um frame para outro
   delay0=90                                     # delay no primeiro frame (centésimos de s)
   delay1=3                                      # delay padrão entre os frames
    loops=0                                      # número de loops (0=infinito)
     anim="dicas-l-big-anim-i$incr.gif"          # gif animado com o resultado
      opt="dicas-l-big-anim-i$incr-opt-gifs.gif" # gif animado otimizado (não parece valer a pena)

### favicon
#   strip="dicas-l-fav-strip.gif"
# framesd="frames-$RANDOM"
#      w1=16
#    incr=1
#  delay0=180
#  delay1=6
#   loops=0
#    anim="dicas-l-fav-anim-i$incr.gif"
#     opt="dicas-l-fav-anim-i$incr-opt-gifs.gif"

### script ###

  pkg()
  {
    local cmd=$1 pkg=$2 s s2 cmds
    [[ $cmd =~ : ]] && { s=s; s2=is; } || s2=l
    [[ $pkg ]] && cmds=" [ ${cmd//:/ } ]" || pkg=$cmd
    which ${cmd%%:*} &> /dev/null || { echo -e "\nInstale o pacote [ $pkg ] para ter o$s comando$s$cmds disponíve$s2.\n\n   sudo apt install $pkg\n"; return 1; }
  }

  pkg identify:convert imagemagick-6.q16 || exit
  pkg gifsicle || exit

  SECONDS=0

  mkdir -p $framesd

  set $(identify -format '%w %h' $strip)
  w=$1 # largura da tira
  h=$2 # altura  da tira

  echo "$strip ${w}x${h}"
  echo "Frames criados a cada $incr px: $((w/incr)) frames"
  echo "Posição"

  # x varia entre todas as colunas da imagem, pulando $incr pixels por vez
  seq -w 0 $incr $((w-1)) | while read x
  do
    # se a posição x permite uma fatia à direita com $w1 pixels de largura
    if (( 10#$x + w1 <= w ))
    then
      # corta uma fatia com essa largura
      convert $strip -crop ${w1}x$h+$x+0   +repage $framesd/frame-$x.gif
    else
      # calcula a largura da fatia auxiliar, da posição $x até o final
      w2=$((w-10#$x))
      convert $strip -crop ${w2}x$h+$x+0   +repage $framesd/frame-$x-p1.gif
      # e a largura de uma fatia auxiliar complementar, do início até completar o frame
      w3=$((w1-w2))
      convert $strip -crop ${w3}x$h+0+0    +repage $framesd/frame-$x-p2.gif
      # emenda as duas fatias em um único frame
      convert $framesd/frame-$x-p{1,2}.gif +append $framesd/frame-$x.gif
      # apaga as fatias auxiliares
      rm $framesd/frame-$x-p{1,2}.gif
    fi
    # indica em que posição da coluna se encontra
    echo -n "$x "
  done
  echo

  # monta a animação, definindo
  #   quantas vezes deve executá-la,
  #   qual a pausa entre os frames exceto o último e
  #   a pausa após o último
               frames=$(ls -1 $framesd/frame-*.gif)
       primeiro_frame=$(head -n 1 <<< "$frames")
  frames_sem_primeiro=$(tail -n+2 <<< "$frames")
  convert -loop $loops -delay $delay0 $primeiro_frame -delay $delay1 $frames_sem_primeiro $anim
  echo "Animação"

  # otimiza o GIF animado
  gifsicle -i $anim --optimize=3 -k 64 -o $opt
  echo "Otimização"

  # se o diretório de frames não for o corrente nem a raiz, apaga-o
  [[ -d $framesd && $framesd != . && $framesd != / ]] && rm -r $framesd

  printf 'Tempo: %02d:%02d:%02d\n' $((SECONDS%(60*60*24)/60/60)) $((SECONDS%(60*60)/60)) $((SECONDS%60))

# EOF
