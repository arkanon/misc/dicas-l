# vim: ft=sh

# Arkanon <arkanon@lsd.org.br>
# 2020/11/22 (Sun) 21:08:30 -03
# 2020/07/11 (Sat) 00:48:38 -03

-- libsixel                      # <http://fb.com/arkanon/posts/10216459579633342>
   período de dízimas periódicas # <http://fb.com/arkanon/posts/10209088252034759>
                                 # <http://gitlab.com/arkanon/playground/-/tree/master/dizima>
   jexer                         # <http://jexer.sourceforge.io/screenshots.html>
   termplay                      # <http://docs.rs/crate/termplay>
   manipulação de formatos especiais em shell
     - pdf    : qpdf, pdftk
     - csv    : csvkit (csvcut csvformat csvsort)
     - xml    : libxml2-utils (xmllint)
     - html   : tidy
     - json   : jq
     - yaml   : yq
     - nodejs : js (phantomjs casperjs slimerjs)
   sessões shell persistentes
   last working directory
   tty-clock
   termsaver
   msmtp
   google earth em placas sem aceleração
   arquivos compactados auto-descompactáveis
   psexec para linux
   powershell para linux
   vi em loop por ssh
   vi renomeando abas
   configs/plugins especialmente úteis para o vimrc

# EOF
