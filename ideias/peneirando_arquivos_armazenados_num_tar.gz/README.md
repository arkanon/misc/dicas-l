# Peneirando arquivos armazenados num tar.gz



Essa dica nasceu de uma troca de ideias no grupo privado de Telegram do curso *[Programação Shell Linux](https://www.dicas-l.com.br/educacao/index/programacao-shell-linux)*
ministrado por nosso estimado professor
[Julio](http://linkedin.com/in/juliocezarneves) *[Papai do Shell](http://linkedin.com/in/juliocezarneves)* [Neves](http://linkedin.com/in/juliocezarneves)
e administrado por [Rubens Queiroz](http://linkedin.com/in/rubens-queiroz-8a21a2), o grande responsável pela [Dicas-L](https://www.dicas-l.com.br) que há décadas nos brinda
com gotas de conhecimento.

O colega [Cesar Rizzo](http://linkedin.com/in/cesarlinux), aluno de uma das primeiras turmas do curso, propôs o desafio de inspecionar via `grep` o conteúdo de arquivos
`tar.gz` indicando, na saída, as linhas casadas em associação aos arquivos que as contenham.

# TL;DR

Como o artigo acabou bem mais longo que o esperado, em consideração aos apressadinhos vou apresentar a dica logo duma vez.

```shell-session
$ export TAR_OPTIONS=--ignore-command-error
$ ls *.tar* | xargs -I+ tar xf + --to-command='grep --label=$TAR_ARCHIVE:$TAR_FILENAME -H w.rd'
t1.tar.bz2:f2.txt:linha 2, word 2
t1.tar.gz:f2.txt:linha 2, word 2
t1.tar.gz:f3.txt:linha 3, w0rd 3
t2.tar:f3.txt:linha 3, w0rd 3
t2.tar:f5.txt:linha 2, w.rd 2
t2.tar:f5.txt:linha 3, w0rd 3
t2.tar.gz:f3.txt:linha 3, w0rd 3
t2.tar.gz:f5.txt:linha 2, w.rd 2
t2.tar.gz:f5.txt:linha 3, w0rd 3
t2.tar.xz:f3.txt:linha 3, w0rd 3
t2.tar.xz:f5.txt:linha 2, w.rd 2
t2.tar.xz:f5.txt:linha 3, w0rd 3
```

Quem quiser ou precisar um detalhamento adequado encontrará abaixo as informações necessárias.



---



Já são razoavelmente conhecidos os comandos `zgrep` e `zipgrep`. Eles permitem pesquisar por ocorrências de expressões regulares dentro de arquivos compactados nos formatos
**[LZ77](http://wikipedia.org/wiki/LZ77)** do `gzip` e **[ZIP](http://wikipedia.org/wiki/ZIP_%28file_format%29)** do `zip`, respectivamente.

Exemplo:

```shell-session
$ grep . f?.txt
f1.txt:linha 1, palavra 1
f1.txt:linha 2, palavra 2
f1.txt:linha 3, palavra 3

f2.txt:linha 1, palavra 1
f2.txt:linha 2, word 2
f2.txt:linha 3, palavra 3

f3.txt:linha 1, palavra 1
f3.txt:linha 2, palavra 2
f3.txt:linha 3, w0rd 3

f4.txt:linha 1, palavra - 1
f4.txt:linha 2, palavra - 2
f4.txt:linha 3, palavra - 3

f5.txt:linha 1, palavra 1
f5.txt:linha 2, w.rd 2
f5.txt:linha 3, w0rd 3

$ ls -1 f?.txt.gz
f1.txt.gz
f2.txt.gz
f3.txt.gz
f4.txt.gz

$ unzip -l f.zip
Archive:  f.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
       57  2023-06-12 23:27   f1.txt
       54  2023-06-12 23:27   f2.txt
       54  2023-06-12 23:28   f3.txt
       63  2023-06-12 23:28   f4.txt
       51  2023-06-15 06:21   f5.txt
---------                     -------
      279                     5 files

$ zgrep w.rd f?.txt.gz
f2.txt.gz:linha 2, word 2
f3.txt.gz:linha 3, w0rd 3
f5.txt.gz:linha 2, w.rd 2
f5.txt.gz:linha 3, w0rd 3

$ zipgrep w.rd f.zip
f2.txt:linha 2, word 2
f3.txt:linha 3, w0rd 3
f5.txt:linha 2, w.rd 2
f5.txt:linha 3, w0rd 3
```

A diferença entre um e outro está relacionada com o modo como cada formato armazena o arquivo: o formato **LZ77** armazena/compacta apenas um arquivo por *archive*, enquanto
o formato **ZIP** armazena vários. Isso nos deixa num beco sem saída quando temos vários arquivos armazenados em um *archive* **tar.gz**:

```shell-session
$ ls -1 *.tar.gz
t1.tar.gz
t2.tar.gz
t3.tar.gz

$ zgrep w.rd t1.tar.gz
grep: t1.tar.gz: arquivo binário coincide com o padrão
```

Para compactar vários arquivos com o `gzip`, mas em um único *archive*, primeiro encapsulamos todos em um arquivo **tar** (*tape archive*) e depois compactamos esse arquivo
com o `gzip`.

Mas como pesquisar o conteúdo dos arquivos nesse caso, sem ter que explicitamente descompactá-lo, mantendo um comportamento equivalente ao `zgrep` e ao `zipgrep`?



---



O comando `tar` tem um parâmetro pouco conhecido mas que lhe adiciona um poder significativo: `--to-command`.

Com ele podemos indicar uma linha de comando que processará a saída do `tar` modificando o resultado bruto original. No caso, o que queremos é que os arquivos compactados sejam
analisados pelo `grep` à procura de linhas que casem com uma expressão regular e, se encontradas ocorrências, que sejam mostrados o nome do arquivo e o conteúdo dessa linha.

O comando `grep`, por sua vez, tem o parâmetro `-H` (`--with-filename`) que prefixa com o nome do arquivo cada linha que casar com a expressão de pesquisa e que vai ao
encontro de uma das nossas especificações.

Mas como os dados que serão processados pelo `grep` são passado do `tar` para ele pela entrada padrão e não por um arquivo propriamente dito, para relacionar as linhas
casadas ao arquivo que está em análise utilizaremos o parâmetro `--label` do `grep`. Esse parâmetro prefixa cada linha casada pelo `grep` com um rótulo fornecido como valor.
A esperteza consiste em alimentar o `--label` com o nome do arquivo que está chegando pela entrada padrão. Isso fará a associação \<nome do arquivo\>—\<conteúdo da linha\>.

Vamos exemplificar o comportamento geral desses dois parâmetros do `grep`:

```shell-session
$ grep w.rd f2.txt
linha 2, word 2

$ grep -H w.rd f2.txt
f2.txt:linha 2, word 2

$ grep w.rd < f2.txt
linha 2, word 2

$ grep -H w.rd < f2.txt
(entrada padrão):linha 2, word 2

$ grep --label="um label qualquer" -H w.rd < f2.txt
um label qualquer:linha 2, word 2

$ grep --label=f2.txt -H w.rd < f2.txt
f2.txt:linha 2, word 2
```

O parâmetro `--to-command` fornece um conjunto muito útil de variáveis internas que disponibilizam diversas informações sobre os arquivos armazenados e o *archive* que
os contém. Elas podem ser usadas com o comando desejado para modificar o resultado do que seria a extração do conteúdo do **tar.gz** processado por esse comando. A variável
`$TAR_FILENAME`, por exemplo, supre o nome do arquivo em processamento pelo `tar` e pode ser colocada como valor do parâmetro `--label` para fazer as vezes de "um label
qualquer" ao prefixar as linhas casadas pelo `grep` com seu parâmetro `-H`.

Então, juntando tudo:

```shell-session
$ tar ztf t2.tar.gz
f1.txt
f3.txt
f5.txt

$ tar zxf t2.tar.gz \
  --to-command='grep --label=$TAR_FILENAME -H w.rd; true'
f3.txt:linha 3, w0rd 3
f5.txt:linha 2, w.rd 2
f5.txt:linha 3, w0rd 3
```

O que aconteceu aí?

## 1º) o shell

O `tar`, ao executar um comando externo via opção `--to-command`, invoca o *shell* `/bin/sh` e executa o comando para cada arquivo do *archive*. Podemos executar qualquer
comando:

```shell-session
$ tar zxf t2.tar.gz --to-command='echo $TAR_FILENAME'
f1.txt
f3.txt
f5.txt
```

`/bin/sh` não passa de um nome genérico e costuma ser um *link* simbólico que aponta para algum *shell* específico, normalmente `ash`, `dash` ou `bash`. Por isso, o
comportamento do *shell* durante a execução do comando seguirá a sintaxe do *shell* alvo deste *link*. No Ubuntu, por exemplo, `/bin/sh` aponta por padrão para o `dash`.
Uma das características do `dash` é que o comando `echo` não precisa do parâmetro `-e` para ecoar uma nova linha (`\n`), ao contrário do `bash`:

```shell-session
$ dash -c 'echo "a\nb"'
a
b

$ dash -c 'echo -e "a\nb"'
-e a
b

$ bash -c 'echo "a\nb"'
a\nb

$ bash -c 'echo -e "a\nb"'
a
b
```

Modificando o alvo do *link*, obviamente teremos um comportamento coerente com o novo *shell*:

```shell-session
$ ls -l /bin/sh
lrwxrwxrwx 1 root root 4 jun 13 22:58 /bin/sh -> dash

$ tar xf t2.tar --to-command='echo "a\nb\n"'
a
b

a
b

$ sudo ln -fs bash /bin/sh
$ ls -l /bin/sh
lrwxrwxrwx 1 root root 4 jun 13 23:37 /bin/sh -> bash

$ tar xf t2.tar --to-command='echo "a\nb\n"'
a\nb\n
a\nb\n

$ tar xf t2.tar --to-command='echo -e "a\nb\n"'
a
b

a
b

$ sudo ln -fs dash /bin/sh
```

2º) o `true`

A menos que o `tar` receba a opção `--ignore-command-error`, o comando da opção `--to-command` **PRECISA finalizar com *status* 0**, caso contrário o `tar` vai "gritar". Para
garantir o status 0, a técnica mais tradicional é finalizar o comando com um `true`. Como `true`é um comando "longo" se comparado com seu *alias* `:` e como esse *alias* existe
tanto no `bash` quanto no `dash`, por uma questão de concisão podemos adotá-lo. Se acontecer de por acaso usarmos algum *shell* que **não tenha** o comando `:` ou que ele não
seja equivalente ao `true`, então deverá ser usado especificamente `true` **OU** qualquer outro comando que **garantidamente** finalize com *status* 0:

```shell-session
$ tar xf t2.tar --to-command='true'

$ tar xf t2.tar --to-command='false'
tar: 2930675: Child returned status 1
tar: 2930676: Child returned status 1
tar: 2930677: Child returned status 1
tar: Saindo com status de falha em razão de erros anteriores

$ tar xf t2.tar --to-command='false' --ignore-command-error

$ tar xf t2.tar --to-command='false; :'

$ tar xf t2.tar --to-command='grep --label=$TAR_ARCHIVE:$TAR_FILENAME -H w.rd'
tar: 2930728: Child returned status 1
t2.tar:f3.txt:linha 3, w0rd 3
t2.tar:f5.txt:linha 2, w.rd 2
t2.tar:f5.txt:linha 3, w0rd 3
tar: Saindo com status de falha em razão de erros anteriores

$ tar xf t2.tar --to-command='grep --label=$TAR_ARCHIVE:$TAR_FILENAME -H w.rd' --ignore-command-error
$ tar xf t2.tar --to-command='grep --label=$TAR_ARCHIVE:$TAR_FILENAME -H w.rd; :'
$ tar xf t2.tar --to-command='grep --label=$TAR_ARCHIVE:$TAR_FILENAME -H w.rd; true'
$ tar xf t2.tar --to-command='grep --label=$TAR_ARCHIVE:$TAR_FILENAME -H w.rd; exit 0'
$ tar xf t2.tar --to-command='grep --label=$TAR_ARCHIVE:$TAR_FILENAME -H w.rd; return 0'
$ tar xf t2.tar --to-command='grep --label=$TAR_ARCHIVE:$TAR_FILENAME -H w.rd; echo -n'
t2.tar:f3.txt:linha 3, w0rd 3
t2.tar:f5.txt:linha 2, w.rd 2
t2.tar:f5.txt:linha 3, w0rd 3

$ tar xf t2.tar --to-command='grep --label=$TAR_ARCHIVE:$TAR_FILENAME -H w.rd; echo FIM'
FIM
t2.tar:f3.txt:linha 3, w0rd 3
FIM
t2.tar:f5.txt:linha 2, w.rd 2
t2.tar:f5.txt:linha 3, w0rd 3
FIM
```

Vejam que a *string* `FIM` foi ecoada 3 vezes no último exemplo porque o comando foi executado para cada um dos 3 arquivos do *archive*. Em apenas dois deles a expressão
regular `w.rd` casou com alguma linha, mas para todos o `echo` foi executado.

Então, a menos que se tenha uma boa razão para permitir que o `tar` finalize a execução do comando com um status diferente de 0, mais simples que emendar um `true` (ou algo
que o valha) ao final do comando, é utilizar a opção `--ignore-command-error`. Para não precisarmos ficar arrastando um parâmetro desse tamanho para cá e para lá, outra medida
interessante é utilizar a variável de ambiente `TAR_OPTIONS` do `tar`:

```shell-session
$ tar xf t2.tar --to-command='grep --label=$TAR_ARCHIVE:$TAR_FILENAME -H w.rd'
tar: 2931703: Child returned status 1
t2.tar:f3.txt:linha 3, w0rd 3
t2.tar:f5.txt:linha 2, w.rd 2
t2.tar:f5.txt:linha 3, w0rd 3
tar: Saindo com status de falha em razão de erros anteriores

$ tar xf t2.tar --to-command='grep --label=$TAR_ARCHIVE:$TAR_FILENAME -H w.rd' --ignore-command-error
t2.tar:f3.txt:linha 3, w0rd 3
t2.tar:f5.txt:linha 2, w.rd 2
t2.tar:f5.txt:linha 3, w0rd 3

$ export TAR_OPTIONS=--ignore-command-error
$ tar xf t2.tar --to-command='grep --label=$TAR_ARCHIVE:$TAR_FILENAME -H w.rd'
t2.tar:f3.txt:linha 3, w0rd 3
t2.tar:f5.txt:linha 2, w.rd 2
t2.tar:f5.txt:linha 3, w0rd 3
```


----



Enfim, isso já parece legal e atinge o objetivo, mas pode ficar melhor.

E se tivermos vários *archives* **tar.gz** e quisermos inspecionar todos, listando o conteúdo casado em associação com o nome do *archive* que o contém, mas apenas dos
que possuírem algum arquivo casando com a expressão; como proceder?

Até poderíamos fazer um *loop* iterando o comando acima entre os *archives* com o padrão `*.tar.gz`, claro. Mas o shell é mais sobre os *loops* implícitos de seus comandos
malandros que sobre os *loops* explícitos característicos das linguagens tradicionais.

Nesse espírito, vamos deixar o `for`, o `while` e o `until` de lado e usar o poderoso `xargs`.

```shell-session
$ ls -1 *.tar.gz
t1.tar.gz
t2.tar.gz
t3.tar.gz

$ export TAR_OPTIONS=--ignore-command-error

$ ls *.tar.gz |
  xargs -I+ tar zxf + \
  --to-command='grep --label=$TAR_FILENAME -H w.rd'
f2.txt:linha 2, word 2
f3.txt:linha 3, w0rd 3
f3.txt:linha 3, w0rd 3
f5.txt:linha 2, w.rd 2
f5.txt:linha 3, w0rd 3
```

OK, devolveu 5 ocorrências distribuídas em 3 arquivos de 3 *archives*. Falta saber que *archives* possuem os arquivos com ocorrências.

Para explicitar isso vamos usar outra variável interna: `$TAR_ARCHIVE`. Ela armazena o nome do *archive* de onde estão sendo processados os arquivos (com o `grep`, no caso).
Inclusive, se não adotarmos a opção que caracteriza especificamente o tipo de compactação usada, por default será adotada a opção `-a` (`--auto-compress`) do `tar`, tornando
possível inspecionar de uma única vez *archives* compactados com os vários tipos de compressão suportadas pelo comando.

Outra possibilidade interessante é usar as inúmeras opções do `grep` para modificar o resultado devolvido:

```shell-session
$ ls -1 *.tar*
t1.tar.bz2
t1.tar.gz
t2.tar
t2.tar.gz
t2.tar.xz
t3.tar.gz
t3.tar.Z

$ export TAR_OPTIONS=--ignore-command-error

$ ls *.tar* |
  xargs -I+ tar xf + \
            --to-command='
              grep \
              --color=always \
              --label=$TAR_ARCHIVE:$TAR_FILENAME \
              -HP "w\K.(?=rd)"
            '
```
![Saída 1](saida1.png)

Se, ao invés de emendarmos `$TAR_ARCHIVE` e `$TAR_FILENAME` como label no `grep`, armazenarmos o resultado correspondido pelo `grep` para depois utilizá-lo, podemos criar uma
saída um pouco mais sofisticada:

```shell-session
$ export TAR_OPTIONS=--ignore-command-error tmp=/dev/shm/archive
$ ls *.tar* |
  xargs -I+ tar xf + \
            --to-command='
              if match=$(
                   grep                  \
                   --color=always        \
                   --label=$TAR_FILENAME \
                   -HP "w\K.(?=rd)"
                 )
              then
                arc=$TAR_ARCHIVE
                grep -qx $arc $tmp &&
                lbl= ||
                {
                  echo $arc > $tmp
                  lbl="[\e[1;33m$arc\e[0m]\n"
                }
                echo "$lbl$match"
              fi
            '
```
![Saída 2](saida2.png)

O comando externo executado pelo `tar` começou a ficar meio complexo. Agora, se `/bin/sh` apontar para o `bash`, poderemos usufruir de facilidades que simplificarão a
composição desse comando. Uma delas é a possibilidade de usar funções previamente definidas e exportadas para o uso no `tar`. Infelizmente o `dash` não exporta funções,
seria necessário usar um *script* armazenado em em um arquivo.


```shell-session
$ sudo ln -fs bash /bin/sh

$ grepintar()
  {
    tmp=/dev/shm/archive
    if match=$(
         grep           \
         --color=always \
         --label=$$2    \
         -HP "w\K.(?=rd)"
       )
    then
      if grep -qx $1 $tmp
      then
        lbl=
      else
        echo $1 > $tmp
        lbl="[\e[1;33m$1\e[0m]\n"
      fi
      echo -e "$lbl$match"
    fi
  }

$ export TAR_OPTIONS=--ignore-command-error

$ ls *.tar* | xargs -I+ tar xf + --to-command='grepintar $TAR_ARCHIVE $TAR_FILENAME'
/bin/sh: linha 1: grepintar: comando não encontrado [18 vezes]

$ export -f grepintar
$ ls *.tar* | xargs -I+ tar xf + --to-command='grepintar $TAR_ARCHIVE $TAR_FILENAME'
[resultado idêntico à saída 2]
```

E já que voltamos a falar da troca do *shell* usado pelo `tar`, sim, mesmo um *shell* alienígena como o [PowerShell](http://github.com/PowerShell/PowerShell) pode ser usado:

```shell-session
$ sudo ln -fs /usr/bin/pwsh /bin/sh
$ ls -l /bin/sh
lrwxrwxrwx 1 root root 13 jun 14 01:32 /bin/sh -> /usr/bin/pwsh

$ tar xf t2.tar --to-command='sls -noemphasis -path $env:TAR_FILENAME w.rd'

f3.txt:3:linha 3, w0rd 3

$ tar xf t2.tar --to-command='sls -noemphasis -path $env:TAR_FILENAME w.rd | % { $_.filename+":"+$_.line }'
f3.txt:linha 3, w0rd 3

$ ls *.tar* | xargs -I@ tar xf @ --to-command='sls -noemphasis -path $env:TAR_FILENAME w.rd | % { $env:TAR_ARCHIVE + ":" + $_.filename + ":" + $_.line }'
t1.tar.bz2:f2.txt:linha 2, word 2
t1.tar.gz:f2.txt:linha 2, word 2
t2.tar:f3.txt:linha 3, w0rd 3
t2.tar.gz:f3.txt:linha 3, w0rd 3
t2.tar.xz:f3.txt:linha 3, w0rd 3
```

Detalhe, nesse último exemplo, para a necessidade de adotar no `xargs` uma *string* de substituição que não esteja sendo usada no comando do powershell, como por exemplo `@`.



## A lista de variáveis

A lista de variáveis disponibilizadas pelo parâmetro `--to-command` do `tar` é bem completa. Provê informações relacionadas ao comando `tar` em si, ao *archive* aberto e ao
arquivo armazenado que está sendo processado:

- ` TAR_VERSION         ` número da versão do GNU `tar`
<!-- -->
- ` TAR_ARCHIVE         ` nome do *archive* **tar** que está sendo processado
- ` TAR_FORMAT          ` formato do *archive* sendo processado
- ` TAR_BLOCKING_FACTOR ` número de blocos de 512 bytes em um registro
- ` TAR_VOLUME          ` número ordinal do volume que o `tar` está processando se estiver lendo um *archive* de vários volumes
- ` TAR_SUBCOMMAND      ` opção curta precedida por um traço descrevendo a operação que o `tar` está executando
<!-- -->

- ` TAR_FILENAME        ` nome do arquivo
- ` TAR_REALNAME        ` nome do arquivo conforme armazenado no *archive*
- ` TAR_FILETYPE        ` uma única letra significando o tipo do arquivo
- ` TAR_MODE            ` modo de arquivo em octal
- ` TAR_SIZE            ` tamanho do arquivo
- ` TAR_ATIME           ` hora do último acesso, número decimal representando a quantidade de segundos do *Unix Timestamp*
- ` TAR_MTIME           ` hora da última modificação
- ` TAR_CTIME           ` hora da última mudança de status
- ` TAR_UNAME           ` nome do dono do arquivo
- ` TAR_GNAME           ` nome do grupo de proprietários do arquivo
- ` TAR_UID             ` UID do proprietário do arquivo
- ` TAR_GID             ` GID do proprietário do arquivo



O comando fornecido ao parâmetro `--to-command` será executado para cada arquivo armazenado no *archive*. O `grep` é apenas a aplicação mais óbvia, talvez. Poderíamos dar um
`cat`:

```shell-session
$ tar xf t2.tar --to-command='echo Conteúdo de $TAR_FILENAME:; cat; echo'
Conteúdo de f1.txt:
linha 1, palavra 1
linha 2, palavra 2
linha 3, palavra 3

Conteúdo de f3.txt:
linha 1, palavra 1
linha 2, palavra 2
linha 3, w0rd 3
```

Portanto, uma forma de inspecionar o valor de alguma(s) das variáveis internas do `tar` (`TAR_...`) é simplesmente dar um `echo` nela(s):

```shell-session
$ tar xf t2.tar --to-command='echo "$TAR_VERSION\n$TAR_FILENAME\n$TAR_SIZE"; echo'
1.34
f1.txt
57

1.34
f3.txt
54
```

Para inspecionar TODAS as variáveis, podemos fazer algo mais simples:

```shell-session
$ tar xf t2.tar --to-command='set | grep ^TAR_; echo'
TAR_ARCHIVE='t2.tar'
TAR_ATIME='1686706888.594238697'
TAR_BLOCKING_FACTOR='20'
TAR_CTIME='1686706888.594238697'
TAR_FILENAME='f1.txt'
TAR_FILETYPE='f'
TAR_FORMAT='gnu'
TAR_GID='10101'
TAR_GNAME='arkanon'
TAR_MODE='0664'
TAR_MTIME='1686623249'
TAR_REALNAME='f1.txt'
TAR_SIZE='57'
TAR_UID='10101'
TAR_UNAME='arkanon'
TAR_VERSION='1.34'
TAR_VOLUME='1'

TAR_ARCHIVE='t2.tar'
TAR_ATIME='1686706888.594238697'
TAR_BLOCKING_FACTOR='20'
TAR_CTIME='1686706888.594238697'
TAR_FILENAME='f3.txt'
TAR_FILETYPE='f'
TAR_FORMAT='gnu'
TAR_GID='10101'
TAR_GNAME='arkanon'
TAR_MODE='0664'
TAR_MTIME='1686623307'
TAR_REALNAME='f3.txt'
TAR_SIZE='54'
TAR_UID='10101'
TAR_UNAME='arkanon'
TAR_VERSION='1.34'
TAR_VOLUME='1'
```



# Comandos usados nesse artigo:

- [%      ](http://learn.microsoft.com/powershell/module/microsoft.powershell.core/foreach-object) (`pwsh`: *alias* do comando `foreach-object`)
- [:      ](http://gnu.org/software/bash/manual/bash.html#index-_003a) (`bash`: *alias* do comando `true`)
- [ash    ](http://man.page/ash)
- [bash   ](http://man.page/bash)
- [cat    ](http://man.page/cat)
- [dash   ](http://man7.org/linux/man-pages/man1/dash.1.html)
- [echo   ](http://gnu.org/software/bash/manual/bash.html#index-echo) (`bash`)
- [export ](http://gnu.org/software/bash/manual/bash.html#index-export) (`bash`)
- [exit   ](http://gnu.org/software/bash/manual/bash.html#index-exit) (`bash`)
- [false  ](http://ss64.com/bash/false.html) (`bash`)
- [grep   ](http://man.page/grep)
- [if     ](http://gnu.org/software/bash/manual/bash.html#index-if) (`bash`)
- [ln     ](http://man.page/ln)
- [ls     ](http://man.page/ls)
- [pwsh   ](http://learn.microsoft.com/powershell/scripting/learn/ps101/01-getting-started)
- [return ](http://gnu.org/software/bash/manual/bash.html#index-return) (`bash`)
- [set    ](http://gnu.org/software/bash/manual/bash.html#index-set) (`bash`)
- [sls    ](http://learn.microsoft.com/powershell/module/microsoft.powershell.utility/select-string) (`pwsh`: *alias* do comando `select-string`)
- [sudo   ](http://man.page/sudo)
- [tar    ](http://man.page/tar)
- [true   ](http://ss64.com/bash/true.html) (`bash`)
- [xargs  ](http://man.page/xargs)
- [zgrep  ](http://man.page/zgrep)
- [zipgrep](http://man.page/zipgrep)



# TODO

- [x] apresentar um exemplo usando powershell
- [x] exemplificar como inspecionar variáveis internas do `tar`
- [x] explicar o motivo do `echo` ecoar `\n` sem o parâmetro `-e`
- [x] explicar porque `--to-command` termina com `:`
- [x] linkar a lista de comandos aos seus man's
- [x] listar outras variáveis internas
- [x] mencionar que funciona também com outras compactações do `tar`
- [x] mencionar que outros parâmetros do `grep`podem ser usados, em especial o `-E`, `-P`, `-i`, `-v` e `--color=always`



☐
